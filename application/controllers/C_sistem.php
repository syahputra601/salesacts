<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_sistem extends CI_Controller {
	function __construct(){
    parent:: __construct();
		$this->load->model('M_rencana');

		if($this->session->userdata('status') != "login"){
			redirect(base_url());
		}
  }

	public function index()
	{
		$data['point']= $this->M_rencana->tampilpoint();
		$data['join']= $this->M_rencana->tampilrealisasi2();
		$data['ultah']= $this->M_rencana->tampilultah()->result_array();
		$data['ksg']= $this->M_rencana->tampilksg();
		$data['cust']= $this->M_rencana->tampilcust()->result();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		$data['sumbercust']= $this->M_rencana->tampilsumbercust()->result();
		$data['hslaktiv']= $this->M_rencana->tampilhslaktiv()->result();
		$data['content_page']= 'v_reminder';
    	$this->load->view('index', $data);
	}


	// Rencana//
	function rencana()
	{
		$data['point']= $this->M_rencana->tampilpoint();
		$data['aktiv']= $this->M_rencana->tampilaktiv()->result();
		$data['cust']= $this->M_rencana->tampilcust()->result();
		$data['rencana']= $this->M_rencana->tampilrencana();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		$data['content_page']= 'v_rencana';
		$this->load->view('index', $data);
	}
	function inputrencana()
	{	
		// die();
		$IDtipemotor = $this->input->post('tipe');
		//START ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		$cekDataActiveMotor = $this->M_rencana->cekActiveMotor($IDtipemotor)->result();
		// print_r($cekDataActiveMotor[0]->count);
		// die();
		$UrlRefreshInput = base_url().'C_sistem/realisasi';
		if($cekDataActiveMotor[0]->count == 0){//jika status hasil count 0 berarti tidak active
			// print_r("tidak active motor");
			echo "<script>alert('Gagal! Tipe Motor Yang Dipilih Saat Ini Tidak Active!');</script>";
			redirect($UrlRefreshInput,'refresh');
		}
		//END ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		if ($this->input->post('customer')=='Nocust')
		{
			$idcust = 2;
			$idtipemotor = 0;
			$idwarna = 0;
		} elseif ($this->input->post('customer')=='Lama')
		{
			$idcust = $this->input->post('cust');
			$idtipemotor = $this->input->post('tipe');
			$idwarna = $this->input->post('warna');
			$tahunmotor = $this->input->post('tahun');
		}else {
			$nama = $this->input->post('nama');
			$telp = $this->input->post('telp');
			$x = str_split($telp);
		  if($x[0] == 6 and $x[1] == 2){
		    $x[0] = '';
		    $x[1] = 0;
		  }
		  $telp = implode("",$x);
			$alamat = $this->input->post('almt');
			$idtipemotor = $this->input->post('tipe');
			$idwarna = $this->input->post('warna');
			$tahunmotor = $this->input->post('tahun');
			$idcst = $this->input->post('idcst');

			$data = array(
				'id_login' => $this->session->userdata('id'),
				'nama' => $nama,
				'hp' => $telp,
				'alamat' => $alamat
			);
			if($idcst == ''){
			$idcust =	$this->M_rencana->inputcustbaru($data);
			}else{
			$idcust = $idcst;
		}
		}

		date_default_timezone_set('Asia/Jakarta');
		// $tanggal = date('Y-m-d H:i:s');
		$time = time();
		$source = $this->input->post('sumbercust');
		$source1 = $this->M_rencana->ambilnamasource($source);
    	$hslaktiv = $this->input->post('hslaktiv');
		if($hslaktiv == 5){
		$spk = 'T'; }else{
		$spk = 'F';
		}
		$ket = $this->input->post('ket');

		$x = array(
			'id_customer' => $idcust,
			'id_sumbercust' => $source,
			'id_login' => $this->session->userdata('id')
		);
		$id = $this->M_rencana->addrencana($x);
		$ro = $this->input->post('cekro');

		if($hslaktiv != 1){
		$tanggal = date('Y-m-d H:i:s', $time);

		$data = array(
			'id_rencana' => $id,
      		'tgl_rencana' => $tanggal,
			'id_tipemotor' => $idtipemotor,
			'id_warnamotor' => $idwarna,
			'tahun_motor' => $tahunmotor,
			'id_hslaktiv' => 1,
			'id_aktiv' => 0,
			'ro' => $ro,
			'status' => 1,
			'spk' => 'F'
    );
		$idrendet = $this->M_rencana->addrencanadet($data);
		$time = $time+1;
		}
		$tanggal = date('Y-m-d H:i:s', $time);

		$data = array(
			'id_rencana' => $id,
      		'tgl_rencana' => $tanggal,
			'id_tipemotor' => $idtipemotor,
			'id_warnamotor' => $idwarna,
			'tahun_motor' => $tahunmotor,
			'id_hslaktiv' => $hslaktiv,
			'id_aktiv' => 0,
			'ket_rencana' => $ket,
			'ro' => $ro,
			'status' => 1,
			'spk' => $spk
    );
		$idrendet = $this->M_rencana->addrencanadet($data);

		if($hslaktiv == 5){
			// Customer
				$nama = $this->input->post('namacust');
				$tempatlahir = $this->input->post('tempatlahir');
				$tgllahir = $this->input->post('tanggallahir');
				$identitas = $this->input->post('identitas');
				$jk = $this->input->post('jk');
				$alamat = $this->input->post('alamat');
				$kota = $this->input->post('kota');
				$kodepos = $this->input->post('kodepos');
				$hp = $this->input->post('hp');
				$idbranch = $this->M_rencana->ambilidbranch();

				$data = array(
					'nama' => strtoupper($nama),
					'tmp_lahir' => strtoupper($tempatlahir),
					'identitas' => 'KTP~'.$identitas,
					'tgl_lahir' => date('Y-m-d', strtotime($tgllahir)),
					'jenis_kelamin' => $jk,
					'alamat' => strtoupper($alamat),
					'kota' => strtoupper($kota),
					'kode_pos' => $kodepos,
					'hp' => $hp,
					'id_branch' => $idbranch,
					'nama_cusgrup' => 'Perorangan'
				);
				$this->M_rencana->updatecust($data, $idcust);
				// End Customer

			// PJSO
		$noso = $this->M_rencana->tampilinv();
		date_default_timezone_set('Asia/Jakarta');
		$datetime = date('Y-m-d H:i:s');
		$date = mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y'));
		$offroad = $this->input->post('offroad');
		$onroad = $this->input->post('ontheroad');
		$bbn = $this->input->post('bbn2');
		if($bbn == 0){
		if($offroad != $onroad){
			$offroad = $onroad;
		}}
		$sub = $offroad / 1.1;
		$ppn = $sub * 10 / 100;
		$ket = $this->input->post('ket');
		$idcomp = $this->M_rencana->ambilidcomp();
		$payment = $this->input->post('payment');
		$nomanual = $this->input->post('nomanual');
		$subsidi = $this->input->post('subsididp');

		$data = array(
			'no_so' => $noso,
			'kode_transaksi' => 'SI',
			'date_so' => $date,
			'id_customer' => $idcust,
			'alamat' => strtoupper($alamat),
			'id_employee' => $this->session->userdata('idemp'),
			'date_kirim' => $date,
			'kode_currency' => 'IDR',
			'kurs' => 1,
			'sub_total' => $sub,
			'ppnpersen' => 10,
			'ppn' => $ppn,
			'total' => $onroad,
			'included' => 'true',
			'approve' => 'true',
			'pembuat' => $this->session->userdata('nama'),
			'catatan' => $ket,
			'closed' => 'false',
			'date_created' => $datetime,
			'date_approved' => $datetime,
			'approved_by' => $this->session->userdata('nama'),
			'id_company' => $idcomp,
			'indent' => 'true',
			'payment' => $payment,
			'no_so_manual' => strtoupper($nomanual),
			'approve_cancel' => 'false',
			'approve_bayar_cancel' => 'false',
			'faktur_req' => 'false',
			'sell_type' => $source1,
			'subsidi_dp' => $subsidi
		);

		$idso = $this->M_rencana->inputpjso($data);
			// End PJSO

			// Barang Jual
			$tipe = $this->input->post('tipe');
			$namamtr = $this->M_rencana->ambilnamamtr($tipe);
			$yesno = $this->input->post('yesno');
			$idwarna = $this->input->post('warna');
			$warnamtr = $this->M_rencana->ambilwarnamtr($idwarna);

			$data = array(
				'nama_barang' => $namamtr,
				'cqty_sisa' => 1,
				'cqty' => 1,
				'satuan' => 'UNIT',
				'harga_satuan' => $offroad,
				'total_so' => $onroad,
				'id_so' => $idso,
				'id_inventori' => $tipe,
				'qty_so' => 1,
				'qty_price' => 1,
				'conv_qty_inv' => 1,
				'conv_qty_price' => 1,
				'conv_harga_satuan' => $offroad,
				'price_1' => $offroad,
				'price_2' => $bbn,
				'is_price_2' => $yesno,
				'remark' => $warnamtr,
				'is_ksg' => 'false',
				'is_free' => 'false'
			);

			$this->M_rencana->inputbarangjual($data);
			// End Barang Jual

			// Sadv
			$nosadv = $this->M_rencana->ambilnodasv();
			$dp = $this->input->post('dp');
			$persensadv = $dp / $onroad * 100;
			date_default_timezone_set('Asia/Jakarta');
			$datetime = date('Y-m-d H:i:s');
			$paymethod = $this->input->post('paymethod');

			$data = array(
				'kode_transaksi' => 'UM',
				'no_sadv' => $nosadv,
				'date_sadv' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
				'id_customer' => $idcust,
				'id_employee' => $this->session->userdata('idemp'),
				'catatan' => $ket,
				'kode_currency' => 'IDR',
				'kurs' => 1,
				'date_pajak' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
				'sub_total' => $onroad,
				'included' => 'false',
				'total_sadv' => $dp,
				'persen_sadv' => number_format($persensadv,2),
				'total' => $dp,
				'created_by' => $this->session->userdata('nama'),
				'date_created' => $datetime,
				'approved' => 'true',
				'date_approved' => $datetime,
				'approved_by' => $this->session->userdata('nama'),
				'id_company' => $idcomp,
				'jalur' => $paymethod,
				'approve_cancel' => 'false'
			);
			$idsadv = $this->M_rencana->inputsadv($data);

			// End Sadv

			// Sadvdet
			//
			$data = array(
				'id_sadv' => $idsadv,
				'id_so' => $idso,
				'no_so' => $noso,
				'description' => 'Pembayaran Down Payment Untuk'.' '.$noso,
				'total_so' => $onroad
			);
			$this->M_rencana->inputsadvdet($data);

			// End Sadvdet
		}else{$idso = NULL;}

		$data = array(
			'idso' => $idso
		);
		$this->M_rencana->updaterencana($data,$idrendet);
    	redirect('C_sistem/realisasi');
	}

	function updateinq()
	{
		date_default_timezone_set('Asia/Jakarta');
		$tanggal = date('Y-m-d H:i:s');
		$idcust = $this->input->post('idcust');
		$source = $this->input->post('sumbercust');
		$source1 = $this->M_rencana->ambilnamasource($source);
		$hslaktiv = $this->input->post('hslaktiv');
		$idtipemotor = $this->input->post('tipe');
		$idwarna = $this->input->post('warna');
		$tahunmotor = $this->input->post('tahun');
		$id = $this->input->post('idren');
		$idrendet = $this->input->post('idrendet');
		//START ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		$cekDataActiveMotor = $this->M_rencana->cekActiveMotor($idtipemotor)->result();
		// print_r($cekDataActiveMotor[0]->count);
		// die();
		$UrlRefreshUpdate = base_url().'C_sistem/pageupdateinq/'.@$idrendet;
		if($cekDataActiveMotor[0]->count == 0){//jika status hasil count 0 berarti tidak active
			// print_r("tidak active motor");
			echo "<script>alert('Gagal! Tipe Motor Yang Dipilih Saat Ini Tidak Active!');</script>";
			redirect($UrlRefreshUpdate,'refresh');
		}elseif($cekDataActiveMotor[0]->count == 1 || $cekDataActiveMotor[0]->count >= 1){//jika status hasil count 1 berarti active
			$this->M_rencana->deletereminder($id);
			if($hslaktiv == 5){
			$spk = 'T'; }else{
			$spk = 'F';
			}
			$ket = $this->input->post('ket');

			$data = array(
				'id_rencana' => $id,
				'tgl_rencana' => $tanggal,
				'id_tipemotor' => $idtipemotor,
				'id_warnamotor' => $idwarna,
				'tahun_motor' => $tahunmotor,
				'id_hslaktiv' => $hslaktiv,
				'id_aktiv' => 0,
				'ket_rencana' => $ket,
				'status' => 1,
				'spk' => $spk
			);
			$idrendet = $this->M_rencana->addrencanadet($data);

			$data = array(
				'id_customer' => $idcust
			);
			$this->M_rencana->updateheader($id, $data);

			if($hslaktiv == 5){
				// Customer
					$nama = $this->input->post('namacust');
					$tempatlahir = $this->input->post('tempatlahir');
					$tgllahir = $this->input->post('tanggallahir');
					$identitas = $this->input->post('identitas');
					$jk = $this->input->post('jk');
					$alamat = $this->input->post('alamat');
					$kota = $this->input->post('kota');
					$kodepos = $this->input->post('kodepos');
					$hp = $this->input->post('hp');
					$idbranch = $this->M_rencana->ambilidbranch();

					$data = array(
						'nama' => strtoupper($nama),
						'tmp_lahir' => strtoupper($tempatlahir),
						'identitas' => 'KTP~'.$identitas,
						'tgl_lahir' => date('Y-m-d', strtotime($tgllahir)),
						'jenis_kelamin' => $jk,
						'alamat' => strtoupper($alamat),
						'kota' => strtoupper($kota),
						'kode_pos' => $kodepos,
						'hp' => $hp,
						'id_branch' => $idbranch,
						'nama_cusgrup' => 'Perorangan'
					);
					$this->M_rencana->updatecust($data, $idcust);
					// End Customer

			// PJSO
			$noso = $this->M_rencana->tampilinv();
			date_default_timezone_set('Asia/Jakarta');
			$datetime = date('Y-m-d H:i:s');
			$date = mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y'));
			$offroad = $this->input->post('offroad');
			$onroad = $this->input->post('ontheroad');
			$bbn = $this->input->post('bbn2');
			if($bbn == 0){
			if($offroad != $onroad){
				$offroad = $onroad;
			}}
			$sub = $offroad / 1.1;
			$ppn = $sub * 10 / 100;
			$ket = $this->input->post('ket');
			$idcomp = $this->M_rencana->ambilidcomp();
			$payment = $this->input->post('payment');
			$nomanual = $this->input->post('nomanual');
			$subsidi = $this->input->post('subsididp');

			$data = array(
				'no_so' => $noso,
				'kode_transaksi' => 'SI',
				'date_so' => $date,
				'id_customer' => $idcust,
				'alamat' => strtoupper($alamat),
				'id_employee' => $this->session->userdata('idemp'),
				'date_kirim' => $date,
				'kode_currency' => 'IDR',
				'kurs' => 1,
				'sub_total' => $sub,
				'ppnpersen' => 10,
				'ppn' => $ppn,
				'total' => $onroad,
				'included' => 'true',
				'approve' => 'true',
				'pembuat' => $this->session->userdata('nama'),
				'catatan' => $ket,
				'closed' => 'false',
				'date_created' => $datetime,
				'date_approved' => $datetime,
				'approved_by' => $this->session->userdata('nama'),
				'id_company' => $idcomp,
				'indent' => 'true',
				'payment' => $payment,
				'no_so_manual' => strtoupper($nomanual),
				'approve_cancel' => 'false',
				'approve_bayar_cancel' => 'false',
				'faktur_req' => 'false',
				'sell_type' => $source1,
				'subsidi_dp' => $subsidi
			);

			$idso = $this->M_rencana->inputpjso($data);
			// End PJSO

				// Barang Jual
				$tipe = $this->input->post('tipe');
				$namamtr = $this->M_rencana->ambilnamamtr($tipe);
				$yesno = $this->input->post('yesno');
				$idwarna = $this->input->post('warna');
				$warnamtr = $this->M_rencana->ambilwarnamtr($idwarna);

				$data = array(
					'nama_barang' => $namamtr,
					'cqty_sisa' => 1,
					'cqty' => 1,
					'satuan' => 'UNIT',
					'harga_satuan' => $offroad,
					'total_so' => $onroad,
					'id_so' => $idso,
					'id_inventori' => $tipe,
					'qty_so' => 1,
					'qty_price' => 1,
					'conv_qty_inv' => 1,
					'conv_qty_price' => 1,
					'conv_harga_satuan' => $offroad,
					'price_1' => $offroad,
					'price_2' => $bbn,
					'is_price_2' => $yesno,
					'remark' => $warnamtr,
					'is_ksg' => 'false',
					'is_free' => 'false'
				);

				$this->M_rencana->inputbarangjual($data);
				// End Barang Jual

				// Sadv
				$nosadv = $this->M_rencana->ambilnodasv();
				$dp = $this->input->post('dp');
				$persensadv = $dp / $onroad * 100;
				date_default_timezone_set('Asia/Jakarta');
				$datetime = date('Y-m-d H:i:s');
				$paymethod = $this->input->post('paymethod');

				$data = array(
					'kode_transaksi' => 'UM',
					'no_sadv' => $nosadv,
					'date_sadv' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
					'id_customer' => $idcust,
					'id_employee' => $this->session->userdata('idemp'),
					'catatan' => $ket,
					'kode_currency' => 'IDR',
					'kurs' => 1,
					'date_pajak' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
					'sub_total' => $onroad,
					'included' => 'false',
					'total_sadv' => $dp,
					'persen_sadv' => number_format($persensadv,2),
					'total' => $dp,
					'created_by' => $this->session->userdata('nama'),
					'date_created' => $datetime,
					'approved' => 'true',
					'date_approved' => $datetime,
					'approved_by' => $this->session->userdata('nama'),
					'id_company' => $idcomp,
					'jalur' => $paymethod,
					'approve_cancel' => 'false'
				);
				$idsadv = $this->M_rencana->inputsadv($data);
				// End Sadv

				// Sadvdet
				//
				$data = array(
					'id_sadv' => $idsadv,
					'id_so' => $idso,
					'no_so' => $noso,
					'description' => 'Pembayaran Down Payment Untuk'.' '.$noso,
					'total_so' => $onroad
				);
				$this->M_rencana->inputsadvdet($data);
				// End Sadvdet
			}else{$idso = NULL;}

			$data = array(
				'idso' => $idso
			);
			$this->M_rencana->updaterencana($data,$idrendet);
			redirect('C_sistem/realisasi');
		}else{//jika status action tidak berjalan
			// print_r("action tidak jalan");
			echo "<script>alert('Gagal! Action Check Tipe Motor Tidak Berjalan.');</script>";
			redirect($UrlRefreshUpdate,'refresh');
		}
		// die();
		//END ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
	}

	function deleterencana($id)
	{
		$this->M_rencana->hapusrencana($id);
		redirect('C_sistem/rencana');
	}

	function editrencana($id)
	{
		$jam = $this->input->post('jam');
		$aktiv = $this->input->post('aktiv');
		$ket = $this->input->post('ket');

		$data = array(
			'jam_rencana' => $jam,
			'id_aktiv' => $aktiv,
			'ket_rencana' => $ket
		);

		$this->M_rencana->updaterencana($data, $id);
		redirect('C_sistem/rencana?ur=1');
	}

	// Reminder //
	function reminder()
	{
		$data['point']= $this->M_rencana->tampilpoint();
		$data['join']= $this->M_rencana->tampilrealisasi2();
		$data['ultah']= $this->M_rencana->tampilultah()->result_array();
		$data['ksg']= $this->M_rencana->tampilksg();
		// var_dump($data['ksg']);
		$data['cust']= $this->M_rencana->tampilcust()->result();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		$data['sumbercust']= $this->M_rencana->tampilsumbercust()->result();
		$data['hslaktiv']= $this->M_rencana->tampilhslaktiv()->result();
		$data['content_page']= 'v_reminder';
		$this->load->view('index', $data);
	}
	function inputreminder()
	{
		date_default_timezone_set('Asia/Jakarta');
		$idren = $this->input->post('idren');
		$idaktiv = $this->input->post('aktiv');
		$tgl = $this->input->post('tglreminder');
		$idcust = $this->input->post('idcust');
		$idsbrcust = $this->input->post('idsbrcust');
		$idhsl = $this->input->post('idhsl');
		$idtipe = $this->input->post('idtipe');
		$idwarna = $this->input->post('idwarna');
		$tahun = $this->input->post('tahun');

		$data = array(
			'id_rencana' => $idren,
			// 'tgl_rencana' => $tgl,
			'tgl_rencana' => date('Y-m-d', strtotime($tgl)),
			'id_hslaktiv' => $idhsl,
			'id_aktiv' => $idaktiv,
			'id_tipemotor' => $idtipe,
			'id_warnamotor' => $idwarna,
			'tahun_motor' => $tahun,
			'status' => 0
		);
		$this->M_rencana->addreminder($data);
		redirect('C_sistem/realisasi');
	}

	function addreal($id)
	{
		$data['r'] = $this->M_rencana->tampilr($id);
		$tes = $this->M_rencana->selectmotor($id)->row();
		// print_r($tes);
		// die();
		$id2=$tes->id_tipemotor;
		$id3=$tes->id_warnamotor;
		$id4=$tes->id_hslaktiv;
		$getMtrDet = $this->M_rencana->getTampilmotor($id2)->result_array();
		$data['mtr_det'] = $getMtrDet;
		$data['idrendet'] = $id;
		$data['src'] = $this->M_rencana->getsrc($id4)->row();
		$data['src2'] = $this->M_rencana->tampilhslaktiv()->result();
		$data['tes'] = $this->M_rencana->datamotor($id2)->row();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['wrn'] = $this->M_rencana->datawarna($id3)->row();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		// $data['harga'] = $this->M_rencana->tampilharga()->result();
		$data['offroad']= $this->M_rencana->tampiloffroad($id2)->row();
		$data['content_page']= 'v_addreal';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index_page', $data);
	}

	function getwarna(){
		$id=$this->input->post('id');
		$data=$this->M_rencana->get_warna($id)->result();
		echo json_encode($data);
	}

	function gethp(){
		$nohp=$this->input->post('id');
		$d=$this->M_rencana->get_hp($nohp)->row();
		$id = $d->id_login;
		$data = $this->M_rencana->get_sls($id)->row();
		echo json_encode($data);
	}

	function getktp(){
		$ktp=$this->input->post('id');
		$data=$this->M_rencana->get_ktp($ktp)->result();
		echo json_encode($data);
	}

	function getsales(){
		$idsales = $this->input->post('id');
		$data=$this->M_rencana->get_sales($idsales)->result();
		echo json_encode($data);
	}

	function getharga(){
		$id=$this->input->post('id');
		$data=$this->M_rencana->get_harga($id)->result();
		echo json_encode($data);
	}

	function cekhpold(){
		$x = $this->input->post('id');
		$cust = explode(';',$x);
		$idcust = $cust[0];
		$hpbaru = $cust[1];
		$data=$this->M_rencana->cek_hpold($idcust,$hpbaru)->result();
		echo json_encode($data);
	}

	function inputreal()
	{
		$idtipemotor = $this->input->post('tipe');
		$idrendet = $this->input->post('id');
		//START ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		$cekDataActiveMotor = $this->M_rencana->cekActiveMotor($idtipemotor)->result();
		$UrlRefreshUpdate = base_url().'C_sistem/addreal/'.@$idrendet;
		if($cekDataActiveMotor[0]->count == 0){//jika status hasil count 0 berarti tidak active
			// print_r("tidak active motor");
			echo "<script>alert('Gagal! Tipe Motor Yang Dipilih Saat Ini Tidak Active!');</script>";
			redirect($UrlRefreshUpdate,'refresh');
		}
		//END ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		if($this->input->post('hasil') != 1){
			$id = $this->input->post('idren');
			$row = $this->M_rencana->cekleads($id)->row();
			if($row == NULL){
				date_default_timezone_set('Asia/Jakarta');
				$time = time();
				$tanggal = date('Y-m-d H:i:s', $time);

				$data = array(
					'id_rencana' => $this->input->post('idren'),
		      		'tgl_rencana' => $tanggal,
					'id_tipemotor' => $this->input->post('tipe'),
					'id_warnamotor' => $this->input->post('warna'),
					'tahun_motor' => $this->input->post('tahun'),
					'id_hslaktiv' => 1,
					'id_aktiv' => 0,
					'ro' => 0,
					'status' => 1,
					'spk' => 'F'
		    );
				$idrendet = $this->M_rencana->addrencanadet($data);
			}
		}

		if ($this->input->post('hasil')=='5')
		{
			$idcust = $this->input->post('nama');
			$id = $this->input->post('idren');

			$data = array(
				'id_customer' => $idcust
			);
			$this->M_rencana->updateheader($id, $data);

		// Customer
			$nama = $this->input->post('namacust');
			$tempatlahir = $this->input->post('tempatlahir');
			$tgllahir = $this->input->post('tanggallahir');
			$identitas = $this->input->post('identitas');
			$jk = $this->input->post('jk');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kodepos = $this->input->post('kodepos');
			$hp = $this->input->post('hp');
			$idbranch = $this->M_rencana->ambilidbranch();

			$data = array(
				'nama' => strtoupper($nama),
				'tmp_lahir' => strtoupper($tempatlahir),
				'identitas' => 'KTP~'.$identitas,
				'tgl_lahir' => date('Y-m-d', strtotime($tgllahir)),
				'jenis_kelamin' => $jk,
				'alamat' => strtoupper($alamat),
				'kota' => strtoupper($kota),
				'kode_pos' => $kodepos,
				'hp' => $hp,
				'id_branch' => $idbranch,
				'nama_cusgrup' => 'Perorangan'
			);
			$this->M_rencana->updatecust($data, $idcust);
			// End Customer

			// PJSO
		$noso = $this->M_rencana->tampilinv();
		date_default_timezone_set('Asia/Jakarta');
		$datetime = date('Y-m-d H:i:s');
		$date = mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y'));
		$offroad = $this->input->post('offroad');
		$onroad = $this->input->post('ontheroad');
		$bbn = $this->input->post('bbn2');
		if($bbn == 0){
		if($offroad != $onroad){
			$offroad = $onroad;
		}}
		$sub = $offroad / 1.1;
		$ppn = $sub * 10 / 100;
		$ket = $this->input->post('ket');
		$idcomp = $this->M_rencana->ambilidcomp();
		$payment = $this->input->post('payment');
		$nomanual = $this->input->post('nomanual');
		$source = $this->input->post('source1');
		$subsidi = $this->input->post('subsididp');

		$data = array(
			'no_so' => $noso,
			'kode_transaksi' => 'SI',
			'date_so' => $date,
			'id_customer' => $idcust,
			'alamat' => strtoupper($alamat),
			'id_employee' => $this->session->userdata('idemp'),
			'date_kirim' => $date,
			'kode_currency' => 'IDR',
			'kurs' => 1,
			'sub_total' => $sub,
			'ppnpersen' => 10,
			'ppn' => $ppn,
			'total' => $onroad,
			'included' => 'true',
			'approve' => 'true',
			'pembuat' => $this->session->userdata('nama'),
			'catatan' => $ket,
			'closed' => 'false',
			'date_created' => $datetime,
			'date_approved' => $datetime,
			'approved_by' => $this->session->userdata('nama'),
			'id_company' => $idcomp,
			'indent' => 'true',
			'payment' => $payment,
			'no_so_manual' => strtoupper($nomanual),
			'approve_cancel' => 'false',
			'approve_bayar_cancel' => 'false',
			'faktur_req' => 'false',
			'sell_type' => $source,
			'subsidi_dp' => $subsidi
		);

		$idso = $this->M_rencana->inputpjso($data);
			// End PJSO

			// Barang Jual
			$tipe = $this->input->post('tipe');
			$namamtr = $this->M_rencana->ambilnamamtr($tipe);
			$yesno = $this->input->post('yesno');
			$idwarna = $this->input->post('warna');
			$warnamtr = $this->M_rencana->ambilwarnamtr($idwarna);

			$data = array(
				'nama_barang' => $namamtr,
				'cqty_sisa' => 1,
				'cqty' => 1,
				'satuan' => 'UNIT',
				'harga_satuan' => $offroad,
				'total_so' => $onroad,
				'id_so' => $idso,
				'id_inventori' => $tipe,
				'qty_so' => 1,
				'qty_price' => 1,
				'conv_qty_inv' => 1,
				'conv_qty_price' => 1,
				'conv_harga_satuan' => $offroad,
				'price_1' => $offroad,
				'price_2' => $bbn,
				'is_price_2' => $yesno,
				'remark' => $warnamtr,
				'is_ksg' => 'false',
				'is_free' => 'false'
			);

			$this->M_rencana->inputbarangjual($data);
			// End Barang Jual

			// Sadv
			$nosadv = $this->M_rencana->ambilnodasv();
			$dp = $this->input->post('dp');
			$persensadv = $dp / $onroad * 100;
			date_default_timezone_set('Asia/Jakarta');
			$datetime = date('Y-m-d H:i:s');
			$paymethod = $this->input->post('paymethod');

			$data = array(
				'kode_transaksi' => 'UM',
				'no_sadv' => $nosadv,
				'date_sadv' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
				'id_customer' => $idcust,
				'id_employee' => $this->session->userdata('idemp'),
				'catatan' => $ket,
				'kode_currency' => 'IDR',
				'kurs' => 1,
				'date_pajak' => mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')),
				'sub_total' => $onroad,
				'included' => 'false',
				'total_sadv' => $dp,
				'persen_sadv' => number_format($persensadv,2),
				'total' => $dp,
				'created_by' => $this->session->userdata('nama'),
				'date_created' => $datetime,
				'approved' => 'true',
				'date_approved' => $datetime,
				'approved_by' => $this->session->userdata('nama'),
				'id_company' => $idcomp,
				'jalur' => $paymethod,
				'approve_cancel' => 'false'
			);
			$idsadv = $this->M_rencana->inputsadv($data);


			// End Sadv

			// Sadvdet
			//
			$data = array(
				'id_sadv' => $idsadv,
				'id_so' => $idso,
				'no_so' => $noso,
				'description' => 'Pembayaran Down Payment Untuk'.' '.$noso,
				'total_so' => $onroad
			);
			$this->M_rencana->inputsadvdet($data);

			// End Sadvdet

		}else{$idso = NULL;}

		$id = $this->input->post('id');
		$tgl = $this->input->post('tgl');
		$nama = $this->input->post('nama');
		$source = $this->input->post('source');
		$hasil = $this->input->post('hasil');
		if($hasil != 5){
		$spk = 'F'; }else{
		$spk = 'T';
		}
		$tipe = $this->input->post('tipe');
		$warna = $this->input->post('warna');
		$tahun = $this->input->post('tahun');
		$ket = $this->input->post('ket');

		date_default_timezone_set('Asia/Jakarta');
		$a = time();
		$time = $a+1;
		$data = array(
			'tgl_rencana' => date('Y-m-d H:i:s', $time),
			'id_hslaktiv' => $hasil,
			'id_tipemotor' => $tipe,
			'id_warnamotor' => $warna,
			'tahun_motor' => $tahun,
			'ket_rencana' => $ket,
			'status' => 1,
			'spk' => $spk,
			'idso' => $idso
		);
		$this->M_rencana->updatereminder($id, $data);
		redirect('C_sistem/realisasi');
	}


// Realisasi//
	function realisasi()
	{
		$data['point']= $this->M_rencana->tampilpoint();
		$data['join']= $this->M_rencana->tampilrealisasi();
		$data['cust']= $this->M_rencana->tampilcust()->result();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		$data['sumbercust']= $this->M_rencana->tampilsumbercust()->result();
		$data['hslaktiv2']= $this->M_rencana->tampilhslaktivbaru()->result();
		$data['aktiv'] = $this->M_rencana->tampilaktiv()->result();
		$data['mtr'] = $this->M_rencana->tampilmotor()->result_array();
		$data['status'] = $this->M_rencana->tampilhslaktiv()->result();
		$data['content_page']= 'v_realisasi';
		$this->load->view('index', $data);
	}

	function detailrealisasi($id)
	{
		$data['detail'] = $this->M_rencana->detailreal($id);
		$data['aktiv'] = $this->M_rencana->tampilaktiv()->result();
		$idso = $this->M_rencana->selectidso($id);
		if($idso != NULL){
		$data['detso'] = $this->M_rencana->tampildetso($idso)->row();
		$data['detbarang'] = $this->M_rencana->tampildetbarang($idso)->row();
		$idsadv = $this->M_rencana->selectidsadv($idso);
		$data['sadv'] = $this->M_rencana->tampilsadv($idsadv)->row();
		}
		$tes = $this->M_rencana->selectmotor($id)->row();
		$id2=$tes->id_tipemotor;
		$warna = $tes->id_warnamotor;
		if($id2 != 0){
		$data['motor'] = $this->M_rencana->tesmotor2($id2)->row();
		}else{
		$data['motor'] = $this->M_rencana->tesmtr()->row();
		}
		if($warna != 0){
		$data['warna']= $this->M_rencana->teswarna($warna)->row();
		}else{
		$data['warna']= $this->M_rencana->teswrn()->row();
		}
		$data['mtr'] = $this->M_rencana->tampilmotor()->result_array();
		$data['status'] = $this->M_rencana->tampilhslaktiv()->result();
		$data['content_page']= 'v_detailrealisasi';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index', $data);
	}

	function pageupdateinq($idrendet){
		$data['data']=$this->M_rencana->getdata($idrendet)->row();
		$id = $this->M_rencana->getidren($idrendet)->row();
		$idren = $id->id_rencana;
		$data['join']=$this->M_rencana->getdataren($idren)->row();
		$tes = $this->M_rencana->selectmotor($idrendet)->row();
		$id4=$tes->id_hslaktiv;
		$id2=$tes->id_tipemotor;
		$warna = $tes->id_warnamotor;
		$data['src'] = $this->M_rencana->getsrc($id4)->row();
		if($id2 != 0){
		$motor = $this->M_rencana->tesmotor_new($id2)->row();
		$data['motor'] = $this->M_rencana->tesmotor_new($id2)->row();
		}else{
		$motor = $this->M_rencana->tesmtr()->row();
		$data['motor'] = $this->M_rencana->tesmtr()->row();
		}
		if($warna != 0){
		$data['warna']= $this->M_rencana->teswarna($warna)->row();
		}else{
		$data['warna']= $this->M_rencana->teswrn()->row();
		}
		$idMotor = @$motor->id_inventori;
		$getMtrDet = $this->M_rencana->getTampilmotor($idMotor)->result_array();
		$data['mtr_det'] = $getMtrDet;
		$data['idrendet'] = $idrendet;
		$data['mtr'] = $this->M_rencana->tampilmotor()->result_array();
		$data['status'] = $this->M_rencana->tampilhslaktiv()->result();
		$data['content_page']= 'v_updateinq';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index_page', $data);
	}

	function filter()
	{
		$filter = $this->input->post('filter');
		$data['join']=$this->M_rencana->filterdata($filter);
		$data['content_page']= 'v_realisasi';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index', $data);
	}

	function pageinputreal($id)
	{
		$data['sumbercust']= $this->M_rencana->tampilsumbercust()->result();
		$data['hslaktiv']= $this->M_rencana->tampilhslaktiv()->result();
		$data['join']= $this->M_rencana->tampilreal($id);
		$data['inv']= $this->M_rencana->tampilinv();
		$tes = $this->M_rencana->selectmotor($id)->row();
		$id2=$tes->id_tipemotor;
		$id3=$tes->id_warnamotor;
		$data['tes'] = $this->M_rencana->datamotor($id2)->row();
		$data['motor'] = $this->M_rencana->tampilmotor()->result_array();
		$data['wrn'] = $this->M_rencana->datawarna($id3)->row();
		$data['warna']= $this->M_rencana->tampilwarna()->result();
		$data['offroad']= $this->M_rencana->tampiloffroad($id2)->row();
		$data['content_page']= 'v_addrealisasi';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index', $data);
	}

	function pageinputdo($idrendet)
	{
		$data['data']= $this->M_rencana->datado($idrendet)->row();
		$tes = $this->M_rencana->selectmotor($idrendet)->row();
		$id2=$tes->id_tipemotor;
		$warna = $tes->id_warnamotor;
		$getMtrDet = $this->M_rencana->getTampilmotor($id2)->result_array();
		$data['mtr_det'] = $getMtrDet;
		$data['idrendet'] = $idrendet;
		$data['motor'] = $this->M_rencana->tesmotor($id2)->row();
		$data['warna']= $this->M_rencana->teswarna($warna)->row();
		$data['mtr'] = $this->M_rencana->tampilmotor()->result_array();
		$data['content_page']= 'v_inputdo';
		$data['point']= $this->M_rencana->tampilpoint();
		$this->load->view('index_page', $data);
	}

	function inputdo()
	{
		$idren = $this->input->post('idren');
		$idaktiv = $this->input->post('idaktiv');
		$idtipe= $this->input->post('idtipe');
		$idwarna = $this->input->post('idwarna');
		$tahun = $this->input->post('tahun');
		$idso = $this->input->post('idso');
		$nodo = $this->input->post('nodo');
		$idrendet = $this->input->post('idrendet');
		date_default_timezone_set('Asia/Jakarta');
		$datetime = date('Y-m-d H:i:s');
		//START ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		$cekDataActiveMotor = $this->M_rencana->cekActiveMotor($idtipe)->result();
		// print_r($cekDataActiveMotor[0]->count);
		// die();
		$UrlRefreshUpdate = base_url().'C_sistem/pageinputdo/'.@$idrendet;
		if($cekDataActiveMotor[0]->count == 0){//jika status hasil count 0 berarti tidak active
			// print_r("tidak active motor");
			echo "<script>alert('Gagal! Tipe Motor Yang Dipilih Saat Ini Tidak Active!');</script>";
			redirect($UrlRefreshUpdate,'refresh');
		}
		//END ACTION FOR CHECKING DATA TIPE MOTOR ACTIVE/NOT
		$id = $idren;
		$this->M_rencana->deletereminder($id);

		$data = array(
			'id_rencana' => $idren,
			'tgl_rencana' => $datetime,
			'id_hslaktiv' => 7,
			'id_aktiv' => $idaktiv,
			'id_tipemotor' => $idtipe,
			'id_warnamotor' => $idwarna,
			'tahun_motor' => $tahun,
			'status' => 1,
			'spk' => 'T',
			'idso' => $idso,
			'no_do' => $nodo
		);
		$idrendet = $this->M_rencana->input_do($data);

		$skrg = time();
	  $dulu = strtotime('2019-12-30');
	  $hasil = $skrg - $dulu;
	  $hasilhari = floor($hasil / (60 * 60 * 24));
	  // var_dump($hasilhari);die;

		$grupksg = $this->M_rencana->cekgrupksg($idtipe)->row();
		$grup = $grupksg->kode_turunan_inv;
		$ksg = $this->M_rencana->datagrupksg($grup)->row();
		//
		// $data = array(
		// 	'id_rencana_det' => $idrendet,
		// );
		redirect('C_sistem/realisasi');
	}

	function changebatal()
	{

		$id = $this->input->post('idrendet');
		$ket = $this->input->post('ket');
		$row = $this->M_rencana->ambilrow($id)->row();

		date_default_timezone_set('Asia/Jakarta');
		$datetime = date('Y-m-d H:i:s');

		$data = array(
			'id_rencana' => $row->id_rencana,
			'tgl_rencana' => $datetime,
			'id_hslaktiv' => 6,
			'id_aktiv' => $row->id_aktiv,
			'ket_rencana' => $ket,
			'id_tipemotor' => $row->id_tipemotor,
			'id_warnamotor' => $row->id_warnamotor,
			'tahun_motor' => $row->tahun_motor,
			'status' => 1,
			'spk' => 'T',
			'idso' => $row->idso
		);
		$this->M_rencana->input_batal($data);
		redirect('C_sistem/realisasi');
	}

	function inputrealisasi()
	{
		if ($this->input->post('hslaktiv')=='1')
		{

		}elseif ($this->input->post('hslaktiv')=='2')
		{

		}
		else {
			// Customer
			$tempatlahir = $this->input->post('tempatlahir');
			$tgllahir = $this->input->post('tanggallahir');
			$identitas = $this->input->post('identitas');
			$jk = $this->input->post('jk');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kodepos = $this->input->post('kodepos');
			$telp = $this->input->post('telp');
			$hp = $this->input->post('hp');
			$id = $this->input->post('idcust');
			$idbranch = $this->M_rencana->ambilidbranch();
			
			$data = array(
				'tmp_lahir' => $tempatlahir,
				'identitas' => $identitas,
				'tgl_lahir' => date('Y-m-d', strtotime($tgllahir)),
				'jenis_kelamin' => $jk,
				'alamat' => $alamat,
				'kota' => $kota,
				'kode_pos' => $kodepos,
				'telepon_1' => $telp,
				'hp' => $hp,
				'id_branch' => $idbranch,
				'nama_cusgrup' => 'Perorangan'
			);
			$this->M_rencana->updatecust($data, $id);
			// End Customer


			// Rencana
			$idrencana = $this->input->post('idrencana');
			$idmotor = $this->input->post('tipe');
			$idwarna = $this->input->post('warna');
			if($this->input->post('yesno') == 'N'){
				$bbn = 0;
			}else{
			$bbn = $this->input->post('bbn');
			}

			$data = array(
				'id_tipemotor' => $idmotor,
				'id_warnamotor' => $idwarna
			);
			$this->M_rencana->updatemotor($data, $idrencana);
		}
		// End Rencana

		// Realisasi
		$jamawal = $this->input->post('jamawal');
		$jamakhir = $this->input->post('jamakhir');
		$hslaktiv = $this->input->post('hslaktiv');
		$sumbercust = $this->input->post('sumbercust');
		$ket = $this->input->post('ket');
		$uang = $this->input->post('dp');
		$idrencana = $this->input->post('idrencana');

		$status = array('status' => 0);

		$data = array(
			'jamawal' => $jamawal,
			'jamakhir' => $jamakhir,
			'id_hslaktiv' => $hslaktiv,
			'id_sumbercust' => $sumbercust,
			'keterangan' => $ket,
			'uangmuka' => $uang,
			'id_rencana' => $idrencana,
			'id_login' => $this->session->userdata('id')
		);
		$this->M_rencana->addstatus($status, $idrencana);
		$this->M_rencana->addrealisasi($data);
		// End Realisasi

		redirect('C_sistem/realisasi');
	}

	function konsumen()
	{
		$data['point']= $this->M_rencana->tampilpoint();
		$data['cust'] = $this->M_rencana->tampilcust()->result_array();
		$data['aktiv'] = $this->M_rencana->tampilaktiv()->result();
		$data['sumbercust']= $this->M_rencana->tampilsumbercust()->result();
		$data['content_page']= 'v_konsumen';
    $this->load->view('index', $data);
	}

	function konsaddrem()
	{
		$idcust = $this->input->post('idcust');
		$tgl = $this->input->post('tglreminder');
		$aktiv = $this->input->post('aktiv');
		$source = $this->input->post('source');

		$x = array(
			'id_customer' => $idcust,
			'id_sumbercust' => $source,
			'id_login' => $this->session->userdata('id')
		);
		$id = $this->M_rencana->addrencana($x);

		$data = array(
			'id_rencana' => $id,
			// 'tgl_rencana' => $tgl,//version old
			'tgl_rencana' => date('Y-m-d', strtotime($tgl)),
			'id_hslaktiv' => 0,
			'id_aktiv' => $aktiv,
			'status' => 0
		);
		$this->M_rencana->addreminder($data);
		redirect('C_sistem/reminder');
	}

	function trailtelp(){
		$id=$this->input->post('id');
		$row = $this->M_rencana->ambilrow($id)->row();
		date_default_timezone_set('Asia/Jakarta');

		$x = array(
			'id_login' => $this->session->userdata('id'),
			'id_rencana_det' => $row->id_rencana_det,
			'tanggal' => date('Y-m-d H:i:s'),
			'ket' => 'Klik tombol Phone'
		);
		$data = $this->M_rencana->addtrailtelp($x);
		echo json_encode($data);
	}

	function trailwa(){
		$id=$this->input->post('id');
		$row = $this->M_rencana->ambilrow($id)->row();
		date_default_timezone_set('Asia/Jakarta');

		$x = array(
			'id_login' => $this->session->userdata('id'),
			'id_rencana_det' => $row->id_rencana_det,
			'tanggal' => date('Y-m-d H:i:s'),
			'ket' => 'Klik tombol WhatsApp'
		);
		$data = $this->M_rencana->addtrailwa($x);
		echo json_encode($data);
	}

	//START CREATED BY AJI FIRMAN SYAHPUTRA
	public function get_select_customer(){//FOR PAGE INQUIRY - NEW CUSTOMER LAMA
		// Search term
		$searchTerm = $this->input->post('searchTerm');
		// Get users
		$response = $this->M_rencana->getSelectCustomer($searchTerm);
		echo json_encode($response);
	}
	public function get_select_motor(){
		// Search term
		$searchTerm = $this->input->post('searchTerm');
		// Get users
		$response = $this->M_rencana->getSelectMotor($searchTerm);
		echo json_encode($response);
	}
	//START FOR TESTING DATA ANY/NOT ANY IN THE DB2 WITH FUNCTION MANUAL
	public function show_data_motor_db2(){//56607//58999 -> active	//56607 -> no active
		$motor = "58999";//56607 //59035 //57769
		$var = $this->M_rencana->getShowDataMtrDb2($motor);
		print_r($var);
	}
	//END FOR TESTING DATA ANY/NOT ANY IN THE DB2 WITH FUNCTION MANUAL
	
	//END CREATED BY AJI FIRMAN SYAHPUTRA

}
