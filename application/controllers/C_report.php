<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_report extends CI_Controller {
	function __construct(){
    parent:: __construct();
		$this->load->model('M_report');

		if($this->session->userdata('status') != "login"){
			redirect(base_url());
		}
  }
	function index()
	{
		$data['join']= $this->M_report->tampildo()->result();
    $data['content_page']= 'v_listdo';
	$data['point']= $this->M_report->tampilpoint();
    $this->load->view('index', $data);
	}

  function tampildo()
	{
    	$data['point']= $this->M_report->tampilpoint();
	$data['join']= $this->M_report->tampildo()->result();
    $data['content_page']= 'v_listdo';
    $this->load->view('index', $data);
  }

	function filterdo()
	{
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$date = $tahun.'-'.$bulan;
		// $data = array($tahun, $bulan);


		$data['join']= $this->M_report->filter_do($date)->result();
		$data['bulan']= $bulan;
		$data['tahun']= $tahun;
		$data['content_page']= 'v_filterdo';
		$data['point']= $this->M_report->tampilpoint();
    $this->load->view('index', $data);
	}
	function filterdo2($a)
	{
		$date = $a;
		$a = explode("-",$date);
		$bulan = $a[1];
		$tahun = $a[0];
		$data['join']= $this->M_report->filter_do($date)->result();
		$data['bulan']= $bulan;
		$data['tahun']= $tahun;
		$data['content_page']= 'v_filterdo';
		$data['point']= $this->M_report->tampilpoint();
		$this->load->view('index', $data);
	}

}
