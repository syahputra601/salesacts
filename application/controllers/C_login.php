<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {
	function __construct(){
    parent:: __construct();
		$this->load->model('M_login');
		$this->CI =& get_instance();
  }

	public function index()
	{
		$this->load->view('v_login');
	}

	function aksilogin()
	{
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');
		$cek = $this->M_login->cek_login($user,$pass)->num_rows();
		$u = $this->M_login->cek_data($user,$pass)->row();
		$id = $u->iduser;
		$a = $this->M_login->ambilidemp($id)->row();
		if($a->id_employee != NULL){
		$idemp = $a->id_employee;
		}else{
		$idemp = 0;
		}
		$idcomp = $u->id_company;
		if($cek > 0){


			$datasession = array(
				'nama' => $user,
				'id' => $id,
				'idcomp' => $idcomp,
				'idemp' => $idemp,
				'status' => "login"
			);

			$this->session->set_userdata($datasession);
			redirect(base_url('C_sistem'));

		}else{
			// echo "Username / password salah !";
			$this->CI->session->set_flashdata('sukses','Oops... Username/password salah');
			redirect(base_url('C_login'));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('C_login'));
	}

}
