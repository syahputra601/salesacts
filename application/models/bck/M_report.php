<?php

class M_report extends CI_Model{
# Point SALAC
function tampilpoint(){
	$idusr = $this->session->userdata('id');
	$poin = 0;
	$jawa = 2;
	$grd = '';
	
	$db2 = $this->load->database('db2', TRUE);
	$db2->select('b.nama,c.id_parent');
	$db2->from('employee as a');
	$db2->join('stratum as b','a.kode_stratum = b.kode');
	$db2->join('company as c','a.id_company = c.id_company');
	$db2->where('a.id_user', $idusr);

	$q = $db2->get()->result();
	foreach($q as $r){
		$grd = $r->nama;
		if($r->id_parent == 2) $jawa = 3;
	}

	if($grd){
		$this->db->from('tb_rencana');
		$this->db->where('id_login',$idusr);
		$q = $this->db->get()->result();
		foreach($q as $r) $datrcn[] = $r->id_rencana;
		
		if(isset($datrcn) and !empty($datrcn)){
			$tdo = $j = $tpoin = 0;
			$this->db->from('tb_target');
			$this->db->where('grade',$grd);
			$q = $this->db->get()->result();
			foreach($q as $r){
				if($jawa == 2) $tdo = $r->njawa;
				else $tdo = $r->jawa;
			}	
			$tgl = date('Y-m-d');
			if($tgl < '2021-03-01') $tgl = '2021-03-01';
			if($tgl > '2021-12-31') $tgl = '2021-12-31';
			$ftgl = explode('-',$tgl);    
			$n = (int)$ftgl[1];		

			for($i=3;$i<=$n;$i++){
				$tgl1 = date('Y-m-d', mktime(0,0,0,$i,1,$ftgl[0]));
				$tgl2 = date('Y-m-d', mktime(23,59,59,($i+1),0,$ftgl[0]));
				$nlead = $nleadgtg = $tlead = $ndo = $nfol = $npal = $nakur = 0;
				$nodo = array();

				$this->db->select('hari');
				$this->db->from('tb_hari');
				$this->db->where('bln',$i);
				$this->db->where('thn',(int)$ftgl[0]);
				$q = $this->db->get()->result();
				foreach($q as $r) $tlead = $r->hari * $jawa;

				foreach($datrcn as $t){
					$this->db->select('id_hslaktiv');
					$this->db->from('tb_rencana_det');
					$this->db->where('id_rencana',$t);
					$this->db->where('status',1);
					$this->db->where('tgl_rencana <',$tgl1);
					$this->db->order_by('tgl_rencana', 'DESC');
					$this->db->limit(1);
					$q = $this->db->get()->result();
					if($q){
						foreach($q as $r){
							if(!in_array($r->id_hslaktiv,array(6,7,8))){
								$nleadgtg++;
								$this->db->select('id_hslaktiv,no_do');
								$this->db->from('tb_rencana_det');
								$this->db->where('id_rencana',$t);
								$this->db->where('status',1);
								$this->db->where('tgl_rencana between "'.$tgl1.'" and "'.$tgl2.'"');
								$this->db->order_by('tgl_rencana', 'DESC');
								$this->db->limit(1);
								$q2 = $this->db->get()->result();
								if($q2){
									foreach($q2 as $r2){
										if($r2->id_hslaktiv == 7){
											$ndo++;
											$nodo[] = "'".$r2->no_do."'";
										}
										if($r2->id_hslaktiv == 8) $npal++;
									}	
								}
							}
						}
					}
					else{
						$this->db->select('id_rencana');
						$this->db->from('tb_rencana_det');
						$this->db->where('id_rencana',$t);
						$this->db->where('status',1);
						$this->db->where('tgl_rencana between "'.$tgl1.'" and "'.$tgl2.'"');
						$this->db->order_by('tgl_rencana', 'ASC');
						$this->db->limit(1);
						$q2 = $this->db->get()->result();
						if($q2){
							foreach($q2 as $r2)	$nlead++;
							$this->db->select('id_hslaktiv,no_do');
							$this->db->from('tb_rencana_det');
							$this->db->where('id_rencana',$t);
							$this->db->where('status',1);
							$this->db->where('tgl_rencana between "'.$tgl1.'" and "'.$tgl2.'"');
							$this->db->order_by('tgl_rencana', 'DESC');
							$this->db->limit(1);
							$q3 = $this->db->get()->result();
							if($q3){
								foreach($q3 as $r3){
									if($r3->id_hslaktiv == 7){
										$ndo++;
										$nodo[] = "'".$r3->no_do."'";
									}
									if($r3->id_hslaktiv == 8) $npal++;
								}	
							}
						}
					}			
				}

				$this->db->select('count(*) as nfol');
				$this->db->from('tb_rencana_det');
				$this->db->where_in('id_rencana',$datrcn);
				$this->db->where('status',1);
				$this->db->where('tgl_rencana between "'.$tgl1.'" and "'.$tgl2.'"');
				$q = $this->db->get()->result();
				foreach($q as $r) $nfol = $r->nfol - $nlead;

				if($nodo){
					$db2->select('a.no_so');
					$db2->from('pj_so as a');
					$db2->join('employee as b','a.id_employee = b.id_employee');
					$db2->where('a.kode_transaksi','SO-U');
					$db2->where('b.id_user',$idusr);
					$db2->where_in('a.no_so',$nodo);
					$q = $db2->get()->result();
					foreach($q as $r) $nakur++;
				}

				$mlead = ($nlead - $npal) * 250 / $tlead;
				$mfol = ($nfol / (($nleadgtg + $nlead) * 2)) * 250;
				$makur = 0;
				if($ndo == $nakur and $ndo > 0) $makur = 200;
				$mplead = ($ndo / ($nleadgtg + $nlead)) * 250;
				$mdo = ($ndo / $tdo) * 50;
				$mpoin = $mlead + $mfol + $makur + $mplead + $mdo;
				$mpal = 0;
				if($npal > 0) $mpal = $mpoin / 2;
				$tpoin += $mpoin - $mpal;
				$j++;			
			}
			$poin = round($tpoin / $j);
		}
	}
	return $poin;
}

  function tampildo()
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

  function filter_do($date)
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->like('tb_rencana_det.tgl_rencana', $date);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

}
