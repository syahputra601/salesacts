<?php

class M_report extends CI_Model{
# Point SALAC
function tampilpoint(){
	$idusr = $this->session->userdata('id');
	$poin = $j = $tpoin = 0;
	
	$tgl = date('Y-m-d');
    if($tgl > '2021-12-31') $tgl = '2021-12-31';
    $ftgl = explode('-',$tgl);    
    $n = (int)$ftgl[1];
    $thn = (int)$ftgl[0];

    for($i=3;$i<=$n;$i++){
		$this->db->from('tb_crondetpointfn');
        $this->db->where('bln',$i);
        $this->db->where('thn',$thn);
        $this->db->where('id_sales',$idusr);
        $q = $this->db->get()->result();
        if($q){
            foreach($q as $r){				
                $mpoin = $r->pleads + $r->pfu + $r->pdo + $r->pakurdat + $r->leadstodo;
                $tpoin += ($r->leadspal > 0) ? ($mpoin / 2) : $mpoin;
                $j++;				
			}
		}		
    }
    if($j > 0) $poin = round($tpoin / $j);
		
	return $poin;
}

  function tampildo()
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

  function filter_do($date)
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->like('tb_rencana_det.tgl_rencana', $date);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

}
