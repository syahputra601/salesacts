<?php
class M_rencana extends CI_Model{
# Point SALAC
function tampilpoint(){
	$idusr = $this->session->userdata('id');
	$poin = $j = $tpoin = 0;
	
	$tgl = date('Y-m-d');
    if($tgl > '2021-12-31') $tgl = '2021-12-31';
    $ftgl = explode('-',$tgl);    
    $n = (int)$ftgl[1];
    $thn = (int)$ftgl[0];

    for($i=3;$i<=$n;$i++){
		$this->db->from('tb_crondetpointfn');
        $this->db->where('bln',$i);
        $this->db->where('thn',$thn);
        $this->db->where('id_sales',$idusr);
        $q = $this->db->get()->result();
        if($q){
            foreach($q as $r){				
                $mpoin = $r->pleads + $r->pfu + $r->pdo + $r->pakurdat + $r->leadstodo;
                $tpoin += ($r->leadspal > 0) ? ($mpoin / 2) : $mpoin;
                $j++;				
			}
		}		
    }
    if($j > 0) $poin = round($tpoin / $j);
		
	return $poin;
}

// Rencana//
function tampilrencana()
{
  $this->db->from('tb_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  $this->db->where('status !=', 1);
  return $this->db->get()->result_array();
}
function tampilaktiv()
{
  $this->db->where('id_aktiv !=', 0);
  return $this->db->get('tb_aktivitas');
}
function tampilcust()
{
  $this->db->where('id_login', $this->session->userdata('id'));
  // $this->db->where('id_customer !=', 2);
  return $this->db->get('tb_customer');
}
function tampilhslaktiv()
{
  $this->db->where('id_hslaktiv !=', 0);
  $this->db->where('id_hslaktiv !=', 7);
  return $this->db->get('tb_hslaktivitas');
}
function tampilhslaktivbaru()
{
  $this->db->where('id_hslaktiv !=', 0);
  $this->db->where('id_hslaktiv !=', 6);
  $this->db->where('id_hslaktiv !=', 7);
  return $this->db->get('tb_hslaktivitas');
}
function tampilsumbercust()
{
  $this->db->where('id_sumbercust !=', 0);
  return $this->db->get('tb_sumbercust');
}
function tampilmotor()
{
  $db2 = $this->load->database('db2', TRUE);
  $invgrup = array('13', '14', '15');
  $db2->where_in('id_invgrup', $invgrup);
  $db2->where('active', TRUE);
  $db2->order_by('nama', 'ASC');
  return $db2->get('inventori');
}
function tampilwarna()
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  return $db2->get();
}
function tampildetso($idso)
{
  $this->db->where('id_so', $idso);
  return $this->db->get('tb_pjso');
}
function tampildetbarang($idso)
{
  $this->db->where('id_so', $idso);
  return $this->db->get('tb_barangjual');
}
function tampilsadv($idsadv)
{
  $this->db->where('id_sadv', $idsadv);
  return $this->db->get('tb_sadv');
}
function get_harga($id)
{
  $idcomp = $this->session->userdata('idcomp');
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id);
  $db2->where("'{".$idcomp."}' && id_company");
  return $db2->get('price_sell');
}
function get_warna($id){
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  $db2->where('id_inventori', $id);
  return $db2->get();
}
function get_hp($nohp){
  $this->db->where('hp', $nohp);
  return $this->db->get('tb_customer');
}
function get_sls($id)
{
  $db2 = $this->load->database('db2', TRUE);
  $grup = array('5', '16', '19', '22', '24', '49', '62', '65');
  $db2->where('iduser', $id);
  return $db2->get('erpuser');

}
function get_ktp($ktp){
  $this->db->where('identitas', 'KTP~'.$ktp);
  return $this->db->get('tb_customer');
}
function get_sales($idsales){
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('iduser', $idsales);
  $db2->where('active', TRUE);
  return $db2->get('erpuser');
}
function cek_hpold($idcust,$hpbaru){
  $this->db->where('hp', $hpbaru);
  $this->db->where('id_customer !=', $idcust);
  return $this->db->get('tb_customer');
}
function tampilwarna2($id2)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  $db2->where('id_inventori', $id2);
  return $db2->get();
}
function addrencana($x)
{
  $this->db->insert('tb_rencana',$x);
  return $this->db->insert_id();
}
function input_do($data)
{
  $this->db->insert('tb_rencana_det',$data);
  return $this->db->insert_id();
}
function addrencanadet($data)
{
  $this->db->insert('tb_rencana_det',$data);
  return $this->db->insert_id();
}
function inputcustbaru($data)
{
  $this->db->insert('tb_customer', $data);
  return $this->db->insert_id();
}
function updaterencana($data, $idrendet)
{
  $this->db->where('id_rencana_det', $idrendet);
  $this->db->update('tb_rencana_det', $data);
}
function updateheader($id, $data)
{
  $this->db->where('id_rencana', $id);
  $this->db->update('tb_rencana', $data);
}
function updateid($x,$idph)
{
  $this->db->where('id_rencana', $idph);
  $this->db->update('tb_rencana', $x);
}
function hapusrencana($id)
{
  $this->db->where('id_rencana', $id);
  $this->db->delete('tb_rencana');
}



// Realisasi//
function tampilrealisasi()
{
  $this->db->from('tb_rencana');
  // $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  // $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
  // $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  // $this->db->where('tb_rencana_det.status', 1);
  // $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
  // $this->db->group_by('tb_rencana_det.id_rencana');
  $a = $this->db->get()->result_array();
  $data = [];
  $i = 0;

  foreach ($a as $e) {
    $this->db->from('tb_rencana_det');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('id_rencana', $e['id_rencana']);
    $this->db->where('status', 1);
    $this->db->order_by('tgl_rencana', 'DESC');
    $this->db->limit(1);
    $b = $this->db->get()->result_array();

    foreach ($b as $f){
      if($f['id_hslaktiv']!= 6 and $f['id_hslaktiv']!= 7){
      $data[$i][0]= $e['id_rencana'];
      $data[$i][1]= $e['nama'];
      $data[$i][2]= $e['nama_sumbercust'];
      $data[$i][3]= $f['tgl_rencana'];
      $data[$i][4]= $f['nama_aktiv'];
      $data[$i][5]= $f['nama_hslaktiv'];
      $data[$i][6]= $f['ket_rencana'];
      $data[$i][7]= $f['status'];
      $data[$i][8]= $f['id_rencana_det'];
      $data[$i][9]= $e['id_customer'];
      $data[$i][10]= $e['id_sumbercust'];
      $data[$i][11]= $f['id_hslaktiv'];
      $data[$i][12]= $f['id_tipemotor'];
      $data[$i][13]= $f['id_warnamotor'];
      $data[$i][14]= $f['tahun_motor'];
      $data[$i][15]= $e['alamat'];
      $data[$i][16]= $e['hp'];
      $data[$i][17]= $f['id_aktiv'];
      $data[$i][18]= $f['idso'];
      }
    }
    $i++;
  }
  function cmp_obj($a, $b)
    {
        if ($a[3] == $b[3]) {
            return 0;
        }
        return ($a[3] < $b[3]) ? +1 : -1;
    }
    usort($data, "cmp_obj");
  return $data;
}

function tampilrealisasi2()
{
  $this->db->from('tb_rencana_det');
  $this->db->join('tb_rencana','tb_rencana.id_rencana=tb_rencana_det.id_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
  $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  $this->db->where('status !=', 1);
  $this->db->where('tgl_rencana', date('Y-m-d'));
  return $this->db->get()->result_array();
}

function getdata($idrendet)
{
  $this->db->where('id_rencana_det', $idrendet);
  return $this->db->get('tb_rencana_det');
}

function datado($idrendet)
{
  $this->db->where('id_rencana_det', $idrendet);
  return $this->db->get('tb_rencana_det');
}

function getdataren($idren)
{
  $this->db->from('tb_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  $this->db->where('id_rencana', $idren);
  return $this->db->get();
}

function tampilinv()
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('iduser', $this->session->userdata('id'));
  $i = $db2->get('erpuser')->row();
  $idc = $i->id_company;

  $db2->where('id_company', $idc);
  $a = $db2->get('company')->row();
  $kd = $a->kode_company;

  $db2->select('no_so');
  $db2->order_by('no_so','DESC');
  $db2->where('id_company', $idc);
  $db2->where('kode_transaksi', 'SI');
  $db2->limit(1);
  $query = $db2->get('pj_so');
        if($query->num_rows() <> 0){
        $data = $query->row();
        $no = $data->no_so;
        $e = explode("/", $no);
        $k = $e[4];
        $kode = intval($k) + 1;
        }else{
            $kode = 1;
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "SI/".$kd."/".date('y/m')."/".$kodemax;
        return $kodejadi;
}
function tampilultah()
{
  $tgl=date('-m-d');
  $this->db->like('tgl_lahir', $tgl);
  $this->db->where('id_login', $this->session->userdata('id'));
  return $this->db->get('tb_customer');
}
function tampilksg()
{
  $this->db->from('tb_rencana_det');
  $this->db->join('tb_rencana','tb_rencana.id_rencana=tb_rencana_det.id_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->where('id_hslaktiv', 7);
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  $data = $this->db->get()->result_array();
  $ksg = [];
  $c = 0;
  foreach($data as $d){
    $db2 = $this->load->database('db2', TRUE);
    $db2->select(['nama','kode_turunan_inv']);
    $db2->where('id_inventori', $d['id_tipemotor']);
    $grupmotor = $db2->get('inventori')->result_array();
    foreach($grupmotor as $g){
      $this->db->where('id_grupksg', $g['kode_turunan_inv']);
      $grupksg = $this->db->get('tb_grupksg')->result_array();
      foreach($grupksg as $gk){
        for($i=1;$i<6;$i++){
          $day = $gk['ksg'.$i];
          if($day != 0){
            $ex = explode(" ", $d['tgl_rencana']);
            $tgl = $ex[0];
            $skrg = time();
            $ksgdate = strtotime(+$day.'day',strtotime($tgl));
            $hasil = $skrg - $ksgdate;
            $selisih = floor($hasil / (60 * 60 * 24));
            if($selisih >= -10 and $selisih <= 0){
              $ksg[$c]['idrendet']= $d['id_rencana_det'];
              $ksg[$c]['nama']= $d['nama'];
              $ksg[$c]['tanggal']= date("d-m-Y",$ksgdate);
              $ksg[$c]['tahun']= $d['no_do'];
              $ksg[$c]['hp']= $d['hp'];
              $ksg[$c]['motor']= $g['nama'];
              $ksg[$c]['ksg']= 'KSG '.$i;
              $c++;
            }
          }
        }
      }
    }
  }

  // var_dump($ksg);


    // foreach($data as $d){
    //   echo $d['id_tipemotor'].'<br>';
    //   $db2 = $this->load->database('db2', TRUE);
    //   $db2->select('kode_turunan_inv');
    //   $db2->where('id_inventori', $d['id_tipemotor']);
    //   $grupmotor = $db2->get('inventori')->result_array();
    //     if($grupmotor){
    //     foreach($grupmotor as $g){
    //       // echo $g->kode_turunan_inv.'<br>';
    //       $this->db->where('id_grupksg', $g['kode_turunan_inv']);
    //       $grupksg = $this->db->get('tb_grupksg')->result_array();
    //           foreach($grupksg as $gk){
    //             for($i=1;$i<7;$i++){
    //               $day = $gk['ksg'.$i];
    //               if($day != 0){
    //                 $ex = explode(" ", $d['tgl_rencana']);
    //                 $tgl = $ex[0];
    //                 $skrg = time();
    //                 $ksgdate = strtotime(+$day.'day',strtotime($tgl));
    //                 $hasil = $skrg - $ksgdate;
    //                 $selisih = floor($hasil / (60 * 60 * 24));
    //                 $ksg = [];
    //                 $i = 0;
    //                 // $ksgdate = date("Y-m-d",$a);
    //                 // var_dump($selisih);die;
    //                 if($selisih >= -10 and $selisih <= 0){
    //                   $ksg[$i]['nama']= $d['nama'];
    //                   $ksg[$i]['tahun']= $d['no_do'];
    //                   $ksg[$i]['warna']= $d['id_warnamotor'];
    //                   $i++;
    //                 }
    //                 // return $ksg;
    //               }
    //             }
    //           }
    //           // $skrg = time();
    //           // $ex = explode(" ", $d->tgl_rencana);
    //           // $tgl = $ex[0];
    //           // $dulu = strtotime($tgl);
    //           // $hasil = $skrg - $dulu;
    //           // $hasilhari = floor($hasil / (60 * 60 * 24));
    //           // var_dump($grupksg);die;
    //           // if($hasilhari == $gruphari->ksg1){
    //           //   return $data;
    //           // }
    //     }
    //   }
    // }
    // return $ksg;
    // var_dump($ksg);die;
      // $this->db->where('id_grupksg', $grupksg->kode_turunan_inv);
      // $gruphari = $this->db->get('tb_grupksg')->row();



  // var_dump($hasilhari);die;
  // return $this->db->get('tb_ksg');
  return $ksg;
}
function ambilnodasv()
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('iduser', $this->session->userdata('id'));
  $i = $db2->get('erpuser')->row();
  $idc = $i->id_company;

  $db2->where('id_company', $idc);
  $a = $db2->get('company')->row();
  $kd = $a->kode_company;

  $db2->select('no_sadv');
  $db2->order_by('no_sadv','DESC');
  $db2->where('id_company', $idc);
  $db2->where('kode_transaksi', 'UM');
  $db2->limit(1);
  $query = $db2->get('pj_sadv');
        if($query->num_rows() <> 0){
        $data = $query->row();
        $no = $data->no_sadv;
        $e = explode("/", $no);
        $k = $e[4];
        $kode = intval($k) + 1;
        }else{
            $kode = 1;
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "UM/".$kd."/".date('y/m')."/".$kodemax;
        return $kodejadi;
}
function ambilidbranch()
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('iduser', $this->session->userdata('id'));
  $row = $db2->get('erpuser')->row();
  return $row->id_branches;
}
function ambilidcomp()
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('iduser', $this->session->userdata('id'));
  $row = $db2->get('erpuser')->row();
  return $row->id_company;
}
function inputpjso($data)
{
  $this->db->insert('tb_pjso', $data);
  return $this->db->insert_id();
}
function inputbarangjual($data)
{
  $this->db->insert('tb_barangjual', $data);
}
function inputsadv($data)
{
  $this->db->insert('tb_sadv', $data);
  return $this->db->insert_id();
}
function inputsadvdet($data)
{
  $this->db->insert('tb_sadvdet', $data);
}
function filterdata($filter)
{
  $tgl=date('d-m-Y');
  $this->db->from('tb_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana.id_hslaktiv');
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  $this->db->where('status', $filter);
  if($filter != 'Realisasi'){
  $this->db->where('tgl_rencana', $tgl);}
  return $this->db->get()->result_array();
}
function datamotor($id2)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id2);
  $db2->where('active', TRUE);
  return $db2->get('inventori');
}
function ambilnamamtr($tipe)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $tipe);
  $db2->where('active', TRUE);
  $row = $db2->get('inventori')->row();
  return $row->nama;
}
function ambilnamasource($source)
{
  $this->db->where('id_sumbercust', $source);
  $row = $this->db->get('tb_sumbercust')->row();
  return $row->nama_sumbercust;
}
function datawarna($id3)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  $db2->where('id_invattropt', $id3);
  return $db2->get();
}
function ambilwarnamtr($idwarna)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  $db2->where('id_invattropt', $idwarna);
  $row = $db2->get()->row();
  return $row->val;
}
function tampiloffroad($id2)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id2);
  return $db2->get('price_sell');
}
function tesmotor($id2)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id2);
  $db2->where('active', TRUE);
  return $db2->get('inventori');
}
function tesmotor2($id2)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id2);
  return $db2->get('inventori');
}
function tesmtr()
{
  $this->db->select('nama');
  return $this->db->get('tb_motor');
}
function teswarna($warna)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->from('inv_attr_opt');
  $db2->join('inv_has_attr','inv_has_attr.id_invhasattr=inv_attr_opt.id_invhasattr');
  $db2->where('id_invattr', 1);
  $db2->where('id_invattropt', $warna);
  return $db2->get();
}
function teswrn()
{
  $this->db->select('val');
  return $this->db->get('tb_motor');
}
function detailreal($id)
{
  $this->db->from('tb_rencana_det');
  $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
  $this->db->join('tb_rencana','tb_rencana.id_rencana=tb_rencana_det.id_rencana');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->where('id_rencana_det', $id);
  return $this->db->get()->row();
}
function tampilreal($id)
{
  $tgl=date('d-m-Y');
  $this->db->from('tb_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana.id_aktiv');
  $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
  $this->db->where('status !=', 1);
  $this->db->where('tgl_rencana', $tgl);
  $this->db->where('id_rencana', $id);
  return $this->db->get()->row();
}
function selectmotor($id)
{
  $this->db->where('id_rencana_det', $id);
  return $this->db->get('tb_rencana_det');
}
function getidren($idrendet)
{
  $this->db->where('id_rencana_det', $idrendet);
  return $this->db->get('tb_rencana_det');
}
function selectidso($id)
{
  $this->db->where('id_rencana_det', $id);
  $row = $this->db->get('tb_rencana_det')->row();
  return $row->idso;
}
function selectidsadv($idso)
{
  $this->db->where('id_so', $idso);
  $row = $this->db->get('tb_sadvdet')->row();
  return $row->id_sadv;
}
function addrealisasi($data)
{
  $this->db->insert('tb_realisasi', $data);
}
function addstatus($status, $idrencana)
{
  $this->db->where('id_rencana', $idrencana);
  $this->db->update('tb_rencana', $status);
}
function updatecust($data, $idcust)
{
  $this->db->where('id_customer', $idcust);
  $this->db->update('tb_customer', $data);
}
function updatemotor($data, $idrencana)
{
  $this->db->where('id_rencana', $idrencana);
  $this->db->update('tb_rencana', $data);
}
function adddp($dp, $idrencana)
{
  $this->db->where('id_rencana', $idrencana);
  $this->db->update('tb_rencana', $dp);
}

function addreminder($data)
{
  $this->db->insert('tb_rencana_det', $data);
}

function deletereminder($id)
{
  $this->db->where('id_rencana', $id);
  $this->db->where('status', 0);
  $this->db->delete('tb_rencana_det');
}

function tampilr($id)
{
  $this->db->from('tb_rencana_det');
  $this->db->join('tb_rencana','tb_rencana.id_rencana=tb_rencana_det.id_rencana');
  $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
  $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
  $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
  $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
  $this->db->where('id_rencana_det', $id);
  return $this->db->get()->row();
}

function ambilrow($id)
{
  $this->db->where('id_rencana_det', $id);
  return $this->db->get('tb_rencana_det');
}

function getsrc($id4)
{
  $this->db->where('id_hslaktiv', $id4);
  return $this->db->get('tb_hslaktivitas');
}

function updatereminder($id, $data)
{
  $this->db->where('id_rencana_det', $id);
  return $this->db->update('tb_rencana_det', $data);
}

function input_batal($data)
{
  $this->db->insert('tb_rencana_det', $data);
}

function cekgrupksg($idtipe)
{
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $idtipe);
  return $db2->get('inventori');
}

function datagrupksg($grup)
{
  $this->db->where('id_grupksg', $grup);
  return $this->db->get('tb_grupksg');
}

function addtrailtelp($x)
{
  $this->db->insert('tb_audittrail', $x);
}

function addtrailwa($x)
{
  $this->db->insert('tb_audittrail', $x);
}

function cekleads($id)
{
  $this->db->where('id_rencana', $id);
  $this->db->where('id_hslaktiv', 1);
  return $this->db->get('tb_rencana_det');
}

//START CREATE BY AJI FIRMAN S
function getSelectCustomer($searchTerm=""){//FOR CONTROLLER PAGE INQUERY - CUSTOMER LAMA
	$this->db->where('id_login', $this->session->userdata('id'));
  $this->db->group_start();
	$this->db->where("nama like '%".$searchTerm."%' ");
	$this->db->or_where("identitas like '%".$searchTerm."%' ");
  $this->db->group_end();
	$this->db->order_by('nama', 'ASC');
	$this->db->limit(10);
	$users = $this->db->get('tb_customer')->result_array();
	$data = array();
	foreach($users as $user){
			$nama_identitas = $user['nama']." | ".$user['identitas'];
			$data[] = array("id"=>$user['id_customer'], "text"=>$nama_identitas);//$user['name']
	}
	return $data;
}
function getSelectMotor($searchTerm=""){
	$db2 = $this->load->database('db2', TRUE);
  $invgrup = array('13', '14', '15');
	$db2->where('active', TRUE);//version old
  $db2->where_in('id_invgrup', $invgrup);
  $db2->group_start();
	$db2->where("kode_original_inv ilike '%".$searchTerm."%' ");
	$db2->or_where("nama ilike '%".$searchTerm."%' ");
  $db2->group_end();
	$db2->order_by('nama', 'ASC');
	// $db2->limit(50);
	$users = $db2->get('inventori')->result_array();
	$data = array();
	foreach($users as $user){
			$kode_nama = $user['kode_original_inv']." | ".$user['nama'];
			$data[] = array("id"=>$user['id_inventori'], "text"=>$kode_nama);//$user['name']
	}
	return $data;
}
function tesmotor_new($id2){
  $db2 = $this->load->database('db2', TRUE);
  $db2->where('id_inventori', $id2);
  // $db2->where('active', 't');//not used
  return $db2->get('inventori');
}
function getTampilmotor($idMotor=''){
  $db2 = $this->load->database('db2', TRUE);
  $invgrup = array('13', '14', '15');
	$db2->where_in('id_invgrup', $invgrup);
  $db2->where('id_inventori', $idMotor);
  // $db2->where('active', 't');//not used
  $db2->order_by('nama', 'ASC');
  return $db2->get('inventori');
}
function cekActiveMotor($motor=''){
	$db2 = $this->load->database('db2', TRUE);
	$db2->select('count(id_inventori)');
	$db2->where('id_inventori', $motor);
  $db2->where('active', 't');//not used
  return $db2->get('inventori');
	// echo $this->db->last_query();
	// die();
	// return $sql;
}
function getShowDataMtrDb2($searchTerm=""){
	$db2 = $this->load->database('db2', TRUE);
  $invgrup = array('13', '14', '15');
	$db2->where('id_inventori', $searchTerm);
	// $db2->where('active', 't');
	$db2->order_by('nama', 'ASC');
	// $db2->limit(50);
	$inventori = $db2->get('inventori')->result_array();
	// return $inventori;
	print_r($inventori);
	die();
}
//END CREATE BY AJI FIRMAN

}
