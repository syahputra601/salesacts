<?php

class M_katalog extends CI_Model{
# Point SALAC
function tampilpoint(){
	$idusr = $this->session->userdata('id');
	$poin = $j = $tpoin = 0;
	
	$tgl = date('Y-m-d');
    if($tgl > '2021-12-31') $tgl = '2021-12-31';
    $ftgl = explode('-',$tgl);    
    $n = (int)$ftgl[1];
    $thn = (int)$ftgl[0];

    for($i=3;$i<=$n;$i++){
		$this->db->from('tb_crondetpointfn');
        $this->db->where('bln',$i);
        $this->db->where('thn',$thn);
        $this->db->where('id_sales',$idusr);
        $q = $this->db->get()->result();
        if($q){
            foreach($q as $r){				
                $mpoin = $r->pleads + $r->pfu + $r->pdo + $r->pakurdat + $r->leadstodo;
                $tpoin += ($r->leadspal > 0) ? ($mpoin / 2) : $mpoin;
                $j++;				
			}
		}		
    }
    if($j > 0) $poin = round($tpoin / $j);
		
	return $poin;
}

  function tampilkatalog($id)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->select('id_motor, nama_motor');
    $db3->where('id_category', $id);
    $a = $db3->get('motorcycle')->result_array();
    $j = 0;

    foreach($a as $i){
      $db3->select('gambar_color');
      $db3->where('id_motor',$i['id_motor']);
      $db3->where('default_color', 'Y');
      $b[$j][0] = $i['id_motor'];
      $b[$j][1] = $i['nama_motor'];
      $b[$j][2] = $db3->get('motcol')->row();
      $j++;
    }
    return $b;
  }

  function tampilgbrmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->from('motorcycle');
    $db3->join('motcol','motcol.id_motor=motorcycle.id_motor');
    $db3->join('master_color','master_color.id_master=motcol.id_icon');
    $db3->where('motorcycle.id_motor', $idmotor);
    return $db3->get();
  }

  function tampildatamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motorcycle');
  }

  function tampilwarnamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->select('id_icon');
    $db3->where('id_motor', $idmotor);
    $a = $db3->get('motcol')->result();

    foreach($a as $i){
      $db3->where('id_master', $i->id_icon);
      $b[] = $db3->get('master_color')->result();
    }
    return $b;
  }

  function tampilhargamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motfitur');
  }

  function tampilfiturmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motfitur_list');
  }

  function tampilbrosurmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('fiturmotor');
  }

}
