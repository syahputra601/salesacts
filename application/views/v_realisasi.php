 <!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Inquiry</h1>
  </div>
  <hr><br>

  <style>
  @media screen and (min-width: 768px) {
  #filt {
    margin-right: 85%;
  }
  #tbhrencana{
    margin-right: 83%;
    width:15%;
  }
  #btnreminder{
    margin-top: 5px;
  }
  }

  @media screen and (max-width: 768px) {
    #tbhrencana{
      width:92%;
    }

    #btnupdate{
      margin-top: 5px;
      margin-bottom: 5px;
    }

    #table{
      margin-left: -25px;
      margin-right: -25px;
    }

    .dataTables_wrapper{
      margin: -15px!important;
    }

    /* table.DataTable td{
      padding : 10px;
    } */
  }
  /* .select2-container .select2-container--default .select2-container--open{
    top: 440px;
  }
  .select-dropdown {
    position: static;
  }
  .select-dropdown .select-dropdown--above {
        margin-top: 336px;
  } */
  </style>

  <div class="card" id="table">
  <div class="card-header">
    Daftar Inquiry
  </div>
  <div class="card-body">
    <center><button type="button" class="btn btn-danger" id="tbhrencana" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-fw fa-plus-circle"></i>
      Tambah Inquiry
    </button></center><br>
    <!-- <form action="<?php echo base_url(); ?>C_sistem/filter" method="post">
      <div class="col-md-2" id="filt">
      <center><select class="form-control" name="filter" style="width:100%;">
        <option value="Realisasi">Realisasi</option>
        <option value="">Belum Realisasi</option>
      </select><br>
    <button type="submit" class="btn btn-info" name="button" style="width:100%;">Filter</button></center></div><br>
    </form> -->

    <!-- Modal -->
		<!-- tabindex="-1" -->
    <div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Inquiry</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="">
            <div class="row">
              <div class="col-md-10">
              <!-- <?php echo base_url(); ?>C_sistem/inputrencana -->
              <!-- onSubmit="validasi_modal_addInquiry()" -->
              <!-- id="form_save_inputRealisasi" name="form_save_inputRealisasi" -->
            <form class="" action="" method="post">
              <label>Customer</label>
              <select id="cust" class="form-control" name="customer" required>
                <option value="">Pilih Customer</option>
                <option value="Lama">Lama</option>
                <option value="Baru">Baru</option>
              </select>
              <div id="select">
                <label>Customer Lama</label>
                <select class="form-control" name="cust" id="cust_lama" style="width: 100%;">
									<option value="">Pilih Customer Lama</option>
                </select>
              </div>
              <div id="box" class="card" style="background-color:gainsboro;">
                <div class="col-md-10">
                  <label><i>Nama</i></label>
                  <input id="nm" type="text" class="form-control" name="nama" required>
                  <label><i>HP</i></label>
                  <input id="nohp" type="number" class="form-control" name="telp" required>
                  <label><i>Alamat</i></label>
                  <textarea class="form-control" name="almt" id="almt" rows="2" cols="80"></textarea>
                  <br>
                </div>
              </div>
            <div id="box2" class="">
              <br>
              <input id="cekro" type="checkbox"  value="1"> Repeat Order<br><br>
              <input id="hidero" type="hidden" name="cekro" value="1">
              <label>Tipe Motor</label>
              <select id="tipe" class="form-control" name="tipe" required style="width: 100%;">
                <option value="">Pilih Tipe Motor</option>
              </select>
            <label>Warna Motor</label>
            <select id="warna" class="form-control" name="warna" required>
            </select>
            <label>Tahun Motor</label>
            <input type="number" class="form-control" name="tahun" id="tahun" value="" required="">
            </div>
            <label>Source</label>
            <select class="form-control" name="sumbercust" id="sumbercust" required>
              <option value="">Pilih Source</option>
              <?php foreach($sumbercust as $a){ ?>
              <option value="<?php echo $a->id_sumbercust?>"><?php echo $a->nama_sumbercust ?></option>
            <?php } ?>
            </select>
            <label>Status Customer</label>
            <select id="kategori" class="form-control" name="hslaktiv">
              <?php foreach($hslaktiv2 as $i){ ?>
              <option value="<?php echo $i->id_hslaktiv?>"><?php echo $i->nama_hslaktiv?></option>
              <?php } ?>
            </select>
              <div id="spk" class="col-md-12" style="background-color:gainsboro;">
                <div class="">
                  <input type="hidden" id="idlogin" name="" value="<?php echo $this->session->userdata('id'); ?>">
            <label><i>Manual Number</i></label>
            <input type="text" id="nomanual" class="form-control" name="nomanual" value="" style="text-transform:uppercase;">
            <label><i>Nama Customer</i></label>
            <input type="text" class="form-control" name="namacust" id="namacust">
            <label><i>Tempat Lahir</i></label>
            <input type="text" class="form-control" name="tempatlahir" id="tempatlahir" value="">
            <label><i>Tanggal Lahir</i></label>
            <input type="text" class="form-control" name="tanggallahir" id="date" onkeypress="return hanyaAngka(event)" maxlength="10">
            <label><i>Identitas</i></label>
            <input type="number" id="ktp" class="form-control" name="identitas" value="">
            <input type="hidden" id="idcst" name="idcst" value="">
            <label><i>Jenis Kelamin</i></label>
            <select class="form-control" name="jk" id="jk">
              <option value="Pria">Pria</option>
              <option value="Wanita">Wanita</option>
            </select>
            <label><i>Alamat</i></label>
            <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
            <label><i>Kota</i></label>
            <input type="text" class="form-control" name="kota" id="kota" value="">
            <label><i>Kode Pos</i></label>
            <input type="number" class="form-control" name="kodepos" id="kodepos" value="">
            <label><i>HP</i></label>
            <input type="number" class="form-control" name="hp" id="hp" value="">
          <label><i>Payment Model</i></label>
          <select class="form-control" name="payment" id="payment">
            <option value="Credit">Credit</option>
            <option value="Cash">Cash</option>
          </select>
          <div class="" hidden>
          <label><i>Offroad Price</i></label>
          <input id="ofr" onkeyup="sum();" type="number" class="form-control" name="offroad" value="" readonly>
          </div>
          <!-- <label><i>BBN Price</i></label> -->
          <div class="row" hidden>
          <div class="col-md-4">
            <select id="yes" class="form-control" name="yesno">
              <option value="Y">Yes</option>
              <option value="N">No</option>
            </select>
          </div>
          <div class="col-md-8" id="bbn1">
            <input id="a" onkeyup="sum();" type="text" class="form-control" name="" value="0" readonly>
            <input id="b" onkeyup="sum();" type="text" class="form-control" name="" value="">
            <input id="valbbn2" onkeyup="sum();" type="text" class="form-control" name="bbn2" value="0">
          </div>
          </div>
          <label>Jenis Penjualan</label>
            <select id="jenis2" class="form-control" name="">
              <option value="Y">On The Road</option>
              <option value="N">Off The Road</option>
            </select>
          <label><i>Harga Unit</i></label>
          <input id="otr" type="text" class="form-control" name="ontheroad" value="" readonly>
          <!-- <label><i>Subsidi DP</i></label> -->
          <input type="hidden" class="form-control" name="subsididp" value="0">
          <label><i>Payment Method</i></label>
            <select class="form-control" name="paymethod" id="paymethod">
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              <option value="Cheque">Cheque</option>
              <option value="Giro">Giro</option>
            </select>
            <label><i>Uang Muka</i></label>
            <input type="text" name="dp" id="dp" class="form-control">
          <br>
              </div>
            </div>
            <label>Keterangan</label>
            <textarea name="ket" id="ket" class="form-control" rows="2"></textarea>
          </div>
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</button>
            <!-- <button type="submit" id="btn_saves_inputRealisasi" name="btn_save_inputRealisasi" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpans</button> -->
            <!-- onclick="return confirm('Apa anda yakin untuk membuat inquiry ini?')" -->
            <button type="button" id="btns_saves_inputRealisasi" name="btns_saves_inputRealisasi" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </div></form>

    <table width="100%" class="table table-striped table-bordered table-hover" id="dttable">
      <thead>
          <tr style="text-align: center;">
              <th>No</th>
              <th>Tanggal</th>
              <th>Nama Customer</th>
              <th>Source</th>
              <th>Aktivitas</th>
              <th>Status Customer</th>
              <th>Keterangan</th>
              <th></th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1;
        foreach ($join as $u) {
        ?>
        <tr style="text-align: center;">
          <td style="padding:5px;"><?php echo $no; ?></td>
          <td style="padding:5px;"><?php echo date("d-m-Y", strtotime($u[3])); ?></td>
          <td style="padding:5px;"><?php echo $u[1] ?></td>
          <td style="padding:5px;"><?php echo $u[2] ?></td>
          <td style="padding:5px;"><?php echo $u[4] ?></td>
          <td style="padding:5px;"><?php echo $u[5] ?></td>
          <td style="padding:5px;"><?php echo $u[6] ?></td>
          <td style="padding:5px;">
            <a href="<?php echo base_url(); ?>C_sistem/detailrealisasi/<?php echo $u[8] ?>" id="btndetail" class="btn btn-danger" style="width:100%"><i class="fas fa-fw fa-eye"></i> Detail</a>
            <?php if($u[11] != 5 and $u[11] != 6) {?>
              <a href="<?php echo base_url(); ?>C_sistem/pageupdateinq/<?php echo $u[8] ?>" id="btnupdate" class="btn btn-primary" style="width:100%; margin-top:5px;"><i class="fas fa-fw fa-edit"></i> Update</a>
            <button class="btn btn-success" id="btnreminder" data-toggle="modal" data-target="#reminderModal/<?php echo $u[0] ?>" style="width:100%"><i class="fas fa-fw fa-bullhorn"></i> Reminder</button>
            <?php }else{ ?>
              <a href="<?php echo base_url(); ?>C_sistem/pageinputdo/<?php echo $u[8] ?>" class="btn btn-info" style="width:100%; margin-top:5px;"><i class="fas fa-fw fa-motorcycle"></i> DO</a>
              <button class="btn btn-secondary" id="btnreminder" data-toggle="modal" data-target="#batalModal/<?php echo $u[8] ?>" style="width:100%; margin-top:5px;"><i class="fas fa-fw fa-close"></i> Batal</button>
              <!-- <a href="<?php echo base_url(); ?>C_sistem/changebatal/<?php echo $u[8] ?>" class="btn btn-secondary" style="width:100%; margin-top:5px;"><i class="fas fa-fw fa-close"></i> Batal</a> -->
              <!-- <button class="btn btn-info" data-toggle="modal" data-target="#DOModal/<?php echo $u[0] ?>">DO</button> -->
            <?php } ?>
          </td>
        </tr>

        <!-- Modal Reminder -->
        <div class="modal fade" id="batalModal/<?php echo $u[8] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Batal Inquiry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8">
                <form class="" action="<?php echo base_url(); ?>C_sistem/changebatal" method="post">
                  <label>Keterangan</label>
                  <textarea name="ket" class="form-control" rows="3" cols="80"></textarea>
                  <input type="hidden" class="form-control" name="idrendet" value="<?php echo $u[8] ?>">
              </div>
              </div>
            </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</button>
                <button type="submit" onclick="return confirm('Apa anda yakin untuk membatalkan inquiry ini?')" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpan</button>
              </div>
            </div>
            </div>
          </div></form>

        <!-- Modal Reminder -->
        <div class="modal fade" id="reminderModal/<?php echo $u[0] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Reminder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8">
                <form class="" action="<?php echo base_url(); ?>C_sistem/inputreminder" method="post">
                  <label>Tanggal Reminder</label>
                  <?php $tgl = date('Y-m-d');?>
                  <input type="text" min="<?php echo $tgl ?>" class="form-control" name="tglreminder" id="tglreminder" onkeypress="return hanyaAngka(event)" maxlength="10" required>
                  <label>Aktivitas</label>
                  <select class="form-control" name="aktiv" required>
                    <option value="">Pilih Aktivitas</option>
                    <?php foreach($aktiv as $a){ ?>
                      <option value="<?php echo $a->id_aktiv ?>"><?php echo $a->nama_aktiv ?></option>
                    <?php } ?>
                  </select>
                  <input type="hidden" class="form-control" name="idren" value="<?php echo $u[0] ?>">
                  <input type="hidden" class="form-control" name="idcust" value="<?php echo $u[9] ?>">
                  <input type="hidden" class="form-control" name="idsbrcust" value="<?php echo $u[10] ?>">
                  <input type="hidden" class="form-control" name="idhsl" value="<?php echo $u[11] ?>">
                  <input type="hidden" class="form-control" name="idtipe" value="<?php echo $u[12] ?>">
                  <input type="hidden" class="form-control" name="idwarna" value="<?php echo $u[13] ?>">
                  <input type="hidden" class="form-control" name="tahun" value="<?php echo $u[14] ?>">
              </div>
              </div>
            </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</button>
                <button type="submit" onclick="return confirm('Apa anda yakin untuk membuat reminder ini?')" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpan</button>
              </div>
            </div>
            </div>
          </div></form>


      <?php $no++; } ?>
      </tbody>
    </table>


  </div>
</div>

</div><br>
<!-- /.container-fluid -->
