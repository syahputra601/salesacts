<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Inquiry</h1>
  </div>
  <hr><br>

  <style>
  @media screen and (max-width: 768px) {
    #btnreminder{
      width:100%;
    }
    #btnupdate{
      margin-top: 5px;
      width:100%;
    }
  }
  </style>

  <div class="card">
  <div class="card-header">
    Detail Inquiry
  </div>
  <div class="card-body">
    <?php if($detail->id_hslaktiv == '7') {?>
    <a href="<?php echo base_url(); ?>C_report/filterdo2/<?php echo date("Y-m", strtotime($detail->tgl_rencana)); ?>" class="btn btn-secondary"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</a><br><br>
    <?php } else{?>
    <a href="<?php echo base_url(); ?>C_sistem/realisasi" class="btn btn-secondary"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</a><br><br>
    <?php } ?>

    <div class="col-md-8">
    <form class="" action="" method="">
    <label>Tanggal</label>
    <input type="text" class="form-control" name="" value="<?php echo date("d-m-Y", strtotime($detail->tgl_rencana)); ?>" readonly>
    <label>Nama Customer</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->nama ?>" readonly>
    <?php if($detail->id_hslaktiv == '5' or $detail->id_hslaktiv == '7') {?>
    <label>Tempat Lahir</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->tmp_lahir ?>" readonly>
    <label>Tanggal Lahir</label>
    <input type="text" class="form-control" name="" value="<?php echo date("d-m-Y", strtotime($detail->tgl_lahir)) ?>" readonly>
    <label>Identitas</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->identitas ?>" readonly>
    <label>Jenis Kelamin</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->jenis_kelamin ?>" readonly>
    <label>Alamat</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->alamat ?>" readonly>
    <label>Kota</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->kota ?>" readonly>
    <label>Kode Pos</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->kode_pos ?>" readonly>
    <?php } ?>
    <label>No Telp</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->hp ?>" readonly>
    <label>Tipe Motor</label>
    <input type="text" id="nama" class="form-control" name="" value="<?php echo $motor->nama ?>" readonly>
    <label>Warna Motor</label>
    <input type="text" class="form-control" name="" value="<?php echo $warna->val ?>" readonly>
    <label>Tahun Motor</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->tahun_motor ?>" readonly>
    <label>Source</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->nama_sumbercust ?>" readonly>
    <?php if($detail->id_hslaktiv == '5' or $detail->id_hslaktiv == '7') {?>
    <label>Payment Model</label>
    <input type="text" class="form-control" name="" value="<?php echo $detso->payment ?>" readonly>
    <label>Offroad Price</label>
    <input type="text" class="form-control" name="" value="<?php echo number_format($detbarang->harga_satuan) ?>" readonly>
    <label>BBN Price</label>
    <input type="text" class="form-control" name="" value="<?php echo number_format($detbarang->price_2) ?>" readonly>
    <label>On The Road</label>
    <input type="text" class="form-control" name="" value="<?php echo number_format($detbarang->total_so) ?>" readonly>
    <label>Payment Method</label>
    <input type="text" class="form-control" name="" value="<?php echo $sadv->jalur ?>" readonly>
    <label>Uang Muka</label>
    <input type="text" class="form-control" name="" value="<?php echo number_format($sadv->total_sadv) ?>" readonly>
    <?php } ?>
    <label>Status Customer</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->nama_hslaktiv ?>" readonly>
    <?php if($detail->id_hslaktiv == '7') {?>
    <label>No DO</label>
    <input type="text" class="form-control" value="<?php echo $detail->no_do ?>" readonly>
    <?php } ?>
    <label>Keterangan</label>
    <input type="text" class="form-control" name="" value="<?php echo $detail->ket_rencana ?>" readonly>
    </form>
    </div><br>
    <!-- <?php if($detail->id_hslaktiv != '5') {?>
    <button class="btn btn-danger" id="btnreminder" data-toggle="modal" data-target="#reminderModal">Reminder</button>
    <button class="btn btn-primary" id="btnupdat" data-toggle="modal" data-target="#updateModal">Update</button>
    <?php } ?> -->

  </div>
</div>

</div>
<br>
<!-- /.container-fluid -->

<!-- Modal -->
<!-- <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Inquery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
        <form class="" action="<?php echo base_url(); ?>C_sistem/updateinq" method="post">
          <input type="hidden" class="form-control" name="idcust" value="<?php echo $detail->id_customer ?>">
          <input type="hidden" class="form-control" name="sumbercust" value="<?php echo $detail->id_sumbercust ?>">
          <input type="hidden" class="form-control" name="idren" value="<?php echo $detail->id_rencana ?>">
          <label>Nama Customer</label>
          <input type="text" class="form-control" name="nama" value="<?php echo $detail->nama ?>" readonly>
          <label>Source</label>
          <input type="text" class="form-control" name="" value="<?php echo $detail->nama_sumbercust ?>" readonly>
          <label>Tipe Motor</label>
          <select id="tipe4" class="form-control" name="tipe">
              <?php foreach($mtr as $a){ ?>
              <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
            <?php } ?>
          </select>
          <label>Warna Motor</label>
          <select id="warna4" class="form-control" name="warna">
            <option value=""></option>
          </select>
          <label>Tahun Motor</label>
          <input type="text" class="form-control" name="tahun" value="<?php echo $detail->tahun_motor ?>">
          <label>Status Customer</label>
          <select id="status4" class="form-control" name="hslaktiv">
            <option value="<?php echo $detail->id_hslaktiv ?>"><?php echo $detail->nama_hslaktiv ?></option>
            <?php foreach($status as $s){ ?>
              <?php if($s->id_hslaktiv != $detail->id_hslaktiv) {?>
            <option value="<?php echo $s->id_hslaktiv ?>"><?php echo $s->nama_hslaktiv ?></option>
            <?php } } ?>
          </select> -->

          <!-- SPK -->
          <!-- <div class="">
            <div id="spk2" class="col-md-12" style="background-color:gainsboro;">
              <div class="">
          <label><i>Manual Number</i></label>
          <input type="text" class="form-control" name="nomanual" value="">
          <label><i>Tempat Lahir</i></label>
          <input type="text" class="form-control" name="tempatlahir" value="">
          <label><i>Tanggal Lahir</i></label>
            <input type="date" class="form-control" name="tanggallahir">
          <label><i>Identitas</i></label>
          <input type="text" class="form-control" name="identitas" value="">
          <label><i>Jenis Kelamin</i></label>
          <select class="form-control" name="jk">
            <option value="Pria">Pria</option>
            <option value="Wanita">Wanita</option>
          </select>
          <label><i>Alamat</i></label>
          <textarea class="form-control" name="alamat" rows="2"><?php echo $detail->alamat ?></textarea>
          <label><i>Kota</i></label>
          <input type="text" class="form-control" name="kota" value="">
          <label><i>Kode Pos</i></label>
          <input type="text" class="form-control" name="kodepos" value="">
          <label><i>HP</i></label>
          <input type="text" class="form-control" name="hp" value="<?php echo $detail->hp ?>">
          <label><i>Payment Model</i></label>
          <select class="form-control" name="payment">
            <option value="Credit">Credit</option>
            <option value="Cash">Cash</option>
          </select>
          <label><i>Offroad Price</i></label>
          <input id="ofr" onkeyup="sum();" type="text" class="form-control" name="offroad" value="" readonly>
          <label><i>BBN Price</i></label>
          <div class="row">
          <div class="col-md-4">
            <select id="yes" class="form-control" name="yesno">
              <option value="Y">Yes</option>
              <option value="N">No</option>
            </select>
          </div>
          <div class="col-md-8" id="bbn1">
            <input id="a" onkeyup="sum();" type="text" class="form-control" name="bbn" value="0" readonly>
          </div>
          <div class="col-md-8" id="bbn2">
            <input id="b" onkeyup="sum();" type="text" class="form-control" name="bbn2" value="">
          </div>
          </div>
          <label><i>On The Road</i></label>
          <input id="otr" type="text" class="form-control" name="ontheroad" value="" readonly>
          <label><i>Subsidi DP</i></label>
          <input type="text" class="form-control" name="subsididp" value="0">
          <label><i>Payment Method</i></label>
            <select class="form-control" name="paymethod">
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              <option value="Cheque">Cheque</option>
              <option value="Giro">Giro</option>
            </select>
            <label><i>Uang Muka</i></label>
            <input type="text" name="dp" class="form-control">
          <br>
              </div>
            </div>
          </div> -->
          <!-- SPK -->

          <!-- <label>Keterangan</label>
          <input type="text" class="form-control" name="ket" value="<?php echo $detail->ket_rencana ?>">
      </div>
      </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
    </div>
  </div></form>

<!-- Modal -->
<!-- <div class="modal fade" id="reminderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Reminder</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
        <form class="" action="<?php echo base_url(); ?>C_sistem/inputreminder" method="post">
          <label>Tanggal Reminder</label>
          <?php $tgl = date('Y-m-d');?>
          <input type="date" min="<?php echo $tgl ?>" class="form-control" name="tglreminder">
          <label>Aktivitas</label>
          <select class="form-control" name="aktiv" required>
            <option value="">Pilih Aktivitas</option>
            <?php foreach($aktiv as $a){ ?>
              <option value="<?php echo $a->id_aktiv ?>"><?php echo $a->nama_aktiv ?></option>
            <?php } ?>
          </select>
          <input type="hidden" class="form-control" name="idren" value="<?php echo $detail->id_rencana ?>">
          <input type="hidden" class="form-control" name="idcust" value="<?php echo $detail->id_customer ?>">
          <input type="hidden" class="form-control" name="idsbrcust" value="<?php echo $detail->id_sumbercust ?>">
          <input type="hidden" class="form-control" name="idhsl" value="<?php echo $detail->id_hslaktiv ?>">
          <input type="hidden" class="form-control" name="idtipe" value="<?php echo $detail->id_tipemotor ?>">
          <input type="hidden" class="form-control" name="idwarna" value="<?php echo $detail->id_warnamotor ?>">
          <input type="hidden" class="form-control" name="tahun" value="<?php echo $detail->tahun_motor ?>">
      </div>
      </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-danger">Tambah</button>
      </div>
    </div>
    </div>
  </div></form><br> -->
