<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Customer</h1>
  </div>
  <hr><br>

  <style>
  @media screen and (min-width: 768px) {
  #filt {
    margin-right: 85%;
  }
  #tbhrencana{
    margin-right: 83%;
    width:14%;
  }
  }

  @media screen and (max-width: 768px) {
    #tbhrencana{
      width:92%;
    }
    #tablecust{
      margin-left: -25px;
      margin-right: -25px;
    }
  }
  </style>

  <div class="card" id="tablecust">
  <div class="card-header">
    Data Customer
  </div>
  <div class="card-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
      <thead>
          <tr style="text-align: center;">
              <th>No</th>
              <th>Nama</th>
              <th>No HP</th>
              <th>Alamat</th>
              <th></th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1;
        foreach ($cust as $u) {
        ?>

        <?php
        $z = $u['hp'];
        $x = str_split($z);
        $x[0]='62';
        $nohp = implode("",$x);  ?>

        <tr style="text-align: center;">
          <td style="padding:30px;"><?php echo $no++; ?></td>
          <td style="padding:30px;"><?php echo $u['nama'] ?></td>
          <td style="padding:30px;"><?php echo $u['hp'] ?></td>
          <td style="padding:30px;"><?php echo $u['alamat'] ?></td>
          <td style="padding:30px;">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal/<?php echo $u['id_customer'] ?>" style="width:100%"><i class="fas fa-fw fa-bullhorn"></i> Reminder</button>
            <a href="tel:+<?php echo $nohp ?>" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #b3ccff; margin-top:5px;"><img src="<?php echo base_url(); ?>assets/img/telp.png" style="width:25px;"> Phone</a>
            <a href="https://wa.me/<?php echo $nohp ?>?text=" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #ccffcc; margin-top:5px"><img src="<?php echo base_url(); ?>assets/img/wa.png" style="width:25px;"> WhatsApp</a>
          </td>
        </tr>
          <?php } ?>
      </tbody>
    </table>

    <!-- Modal -->
    <?php
    foreach ($cust as $u) {
    ?>
    <div class="modal fade" id="exampleModal/<?php echo $u['id_customer'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Input Reminder</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-8">
            <form class="" action="<?php echo base_url(); ?>C_sistem/konsaddrem" method="post">
              <label>Tanggal Reminder</label>
              <?php $tgl = date('Y-m-d');?>
              <input type="text" min="<?php echo $tgl ?>" class="form-control" name="tglreminder" id="date" onkeypress="return hanyaAngka(event)" maxlength="10" required>
              <label>Aktivitas</label>
              <select class="form-control" name="aktiv" required>
                <option value="">Pilih Aktivitas</option>
                <?php foreach($aktiv as $a){ ?>
                  <option value="<?php echo $a->id_aktiv ?>"><?php echo $a->nama_aktiv ?></option>
                <?php } ?>
              </select>
              <label>Source</label>
              <select class="form-control" name="source" required>
                <option value="">Pilih Source</option>
                <?php foreach($sumbercust as $e){ ?>
                  <option value="<?php echo $e->id_sumbercust ?>"><?php echo $e->nama_sumbercust ?></option>
                <?php } ?>
              </select>
              <input type="hidden" name="idcust" value="<?php echo $u['id_customer'] ?>">
              </div>
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</button>
            <button type="submit" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpan</button>
          </div>
        </div>
        </div>
        </div></form><?php } ?>

  </div>
</div>

</div><br>
<!-- /.container-fluid -->
