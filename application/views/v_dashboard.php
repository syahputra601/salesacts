<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  </div>
  <hr><br>

      <!-- Content Row -->
      <div class="row">

      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
            <a href="<?php echo base_url(); ?>C_sistem/rencana"><button type="button" name="button" class="btn btn-primary" style="width:100%; height:100px;">Rencana</button></a>
      </div>

      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
            <a href="<?php echo base_url(); ?>C_sistem/realisasi"><button type="button" name="button" class="btn btn-success" style="width:100%; height:100px;">Realisasi</button></a>
      </div>

      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
            <a href="#"><button type="button" name="button" class="btn btn-info" style="width:100%; height:100px;">Report</button></a>
      </div>


  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
