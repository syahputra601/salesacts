<!-- Begin Page Content -->
<div class="container-fluid">

 <!-- Page Heading -->
 <div class="d-sm-flex align-items-center justify-content-between mb-4">
   <h1 class="h3 mb-0 text-gray-800">List DO</h1>
 </div>
 <hr><br>

 <style>
 @media screen and (min-width: 768px) {
   #tbhrencana{
     margin-right: 83%;
     width:15%;
   }
   #bulan{
     margin-right: 83%;
     width: 15%;
   }
   #tahun{
     margin-bottom: 10px;
     margin-right: 83%;
     width: 15%;
   }
   #label1{
     margin-right: 83%;
   }
   #label2{
     margin-right: 83%;
   }
 }

 @media screen and (max-width: 768px) {
   #tbhrencana{
     width:100%;
     margin-top: 20px;
   }
   #tglawal{
     width: 92%;
   }
   #tglakhir{
     width: 92%;
   }
   #table{
     margin-left: -25px;
     margin-right: -25px;
   }

 }
 </style>

 <div class="card" id="table">
 <div class="card-header">
   Daftar DO
 </div>
 <div class="card-body">
   <center>
     <form class="" action="<?php echo base_url(); ?>C_report/filterdo" method="post">
       <label id="label1">Bulan</label>
       <?php $m = date('m'); ?>
       <select id="bulan" class="form-control" name="bulan">
         <option value="01" <?php if($m == '01') echo 'selected'; ?>>Januari</option>
         <option value="02" <?php if($m == '02') echo 'selected'; ?>>Februari</option>
         <option value="03" <?php if($m == '03') echo 'selected'; ?>>Maret</option>
         <option value="04" <?php if($m == '04') echo 'selected'; ?>>April</option>
         <option value="05" <?php if($m == '05') echo 'selected'; ?>>Mei</option>
         <option value="06" <?php if($m == '06') echo 'selected'; ?>>Juni</option>
         <option value="07" <?php if($m == '07') echo 'selected'; ?>>Juli</option>
         <option value="08" <?php if($m == '08') echo 'selected'; ?>>Agustus</option>
         <option value="09" <?php if($m == '09') echo 'selected'; ?>>September</option>
         <option value="10" <?php if($m == '10') echo 'selected'; ?>>Oktober</option>
         <option value="11" <?php if($m == '11') echo 'selected'; ?>>November</option>
         <option value="12" <?php if($m == '12') echo 'selected'; ?>>Desember</option>
       </select>
       <label id="label2">Tahun</label>
       <select id="tahun" class="form-control" name="tahun">
         <?php $y = date('Y'); ?>
         <?php for ($i = 2019; $i <= $y; $i++) { ?>
           <option value="<?php echo $i ?>" <?php if($y == $i) echo 'selected'; ?>><?php echo $i ?></option>
         <?php } ?>
       </select>
       <button type="submit" class="btn btn-danger" id="tbhrencana"><i class="fas fa-fw fa-plus-circle"></i>
       Filter
     </button>
     </form>
   </center>


 </div>
</div>

</div><br>
<!-- /.container-fluid -->
