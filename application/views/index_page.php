<!DOCTYPE html>
<html lang="en">

<head>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>System Sales</title>
	<!-- START ADD STYLE BY AJI -->
		<!-- START Select2 CSS -->
	<!-- <link href="<?php echo base_url(); ?>assets/select2/dist/css/select2.min.css?x=<?php echo rand(100,10000); ?>" rel="stylesheet"/> -->
  <link href="<?php echo base_url(); ?>assets/select2/dist/css/select2_page.css?x=<?php echo rand(100,10000); ?>" rel="stylesheet"/>
		<!-- END Select2 CSS -->
	<!-- END ADD STYLE BY AJI -->

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logossm.png">

  <style media="screen">
    .bg-white{
      background-color: #e20b12!important;
    }
    .dtr-details{
      width:100%;
    }
    .text-gray-600{
      color: white!important;
    }
    .sidebar-dark .nav-item .nav-link {
      color: black;
    }
    .sidebar-dark .nav-item .nav-link i {
      color: black;
    }
    .nav-link.collapsed:hover{
      color: red!important;
    }
    .fas:hover{
      color: red!important;
    }
    .sidebar-dark .nav-item .nav-link:active, .sidebar-dark .nav-item .nav-link:focus, .sidebar-dark .nav-item .nav-link:hover {
    color: black;
    }
    .sidebar-dark .nav-item .nav-link:active i, .sidebar-dark .nav-item .nav-link:focus i, .sidebar-dark .nav-item .nav-link:hover i {
    color: black;
    }
    .sidebar-dark .nav-item.active .nav-link i{
      color:red!important;
    }
    .nav-item.active a span{
    color: red;
    }
    .copyright{
      color: white;
    }
    .rounded-circle{
      background-color: #e20b12!important;
    }
    .sidebar-brand-text{
      color: red;
    }
    .sidebar-divider{
      background-color: black!important;
    }
    .fa{
      color:white;
    }
    .listspek table tr td:first-child{
      padding-right: 35px;
    }
    .ftrfix{
      padding: 1rem !important;
      position: fixed;
      bottom: 0;
      width: 100%;
    }
    .headfix{
      position: fixed;
      width: 100%;
      z-index: 1;
      background-color: #e20b12;
    }
    .fotfix{
      bottom: 0;
      position: fixed;
      width: 100%;
      z-index: 1;
      background-color: #e20b12;
    }
    /* @media only screen and (min-width: 900px) {
      .headfix{
        width: 93%;
      }
    } */
    .nav-lk{
      color: white;
    }
    .nav-lk:hover{
      color: black;
    }
    .nav-lk-active{
      color: black;
    }
    .menufix a span{
      display: block;
      font-size: 11px;
    }
  </style>

</head>

<body id="page-top" style="position:relative; padding-bottom:40px;">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>C_sistem">
        <div class="sidebar-brand-icon">
          <img src="<?php echo base_url(); ?>assets/img/logossm.png" alt="" style="width:100%;">
        </div>
        <div class="sidebar-brand-text mx-3">System Sales</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">



      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>C_sistem">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/rencana">
          <i class="fas fa-fw fa-th-list"></i>
          <span>Rencana</span>
        </a>
      </li> -->

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'reminder' or $this->uri->segment('2') == '') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/reminder">
          <i class="fas fa-fw fa-book"></i>
          <span>Reminder</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'realisasi' or $this->uri->segment('2') == 'detailrealisasi') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/realisasi">
          <i class="fas fa-fw fa-list"></i>
          <span>Inquiry</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'konsumen') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/konsumen" >
          <i class="fas fa-fw fa-database"></i>
          <span>Data Customer</span>
        </a>
      </li>

      <li class="nav-item <?php if($this->uri->segment('1') == 'C_katalog') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_katalog/katmotor" >
          <i class="fas fa-fw fa-motorcycle"></i>
          <span>Katalog</span>
        </a>
      </li>

      <li class="nav-item <?php if($this->uri->segment('2') == 'do' or $this->uri->segment('1') == 'C_report' or $this->uri->segment('2') == 'filterdo') echo 'active'; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Report</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">List Report:</h6>
            <a class="collapse-item" href="<?php echo base_url(); ?>C_report/tampildo"><i class="fas fa-fw fa-motorcycle"></i> DO</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_login/logout" >
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Logout</span>
        </a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <!-- <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div> -->

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <nav class="navbar navbar-default navbar-fixed-top headfix">
            <div class="container menufix">
              <a href="#">
                <span style="color:white;"><?php echo strtoupper($this->session->userdata("nama")); ?></span>
                <span style="color:white;">Point : <?php echo $point?></span>
              </a>
              <a href="<?php echo base_url(); ?>C_katalog/katmotor" class="nav-lk text-center <?php if($this->uri->segment('1') == 'C_katalog') echo 'nav-lk-active'; ?>">
                <i class="fas fa-fw fa-motorcycle fa-lg"></i><br>
                <span>Katalog</span>
              </a>
              <a href="<?php echo base_url(); ?>C_sistem/konsumen" class="nav-lk text-center <?php if($this->uri->segment('2') == 'konsumen') echo 'nav-lk-active'; ?>">
                <i class="fas fa-fw fa-database fa-lg"></i><br>
                <span>Data Customer</span>
              </a>
              <a href="<?php echo base_url(); ?>C_login/logout" class="nav-lk text-center">
                <i class="fas fa-fw fa-sign-out-alt fa-lg"></i><br>
                <span>Logout</span>
              </a>
            </div>
        </nav>
        <!-- Topbar -->
        <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow headfix"> -->

          <!-- Sidebar Toggle (Topbar) -->
          <!-- <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button> -->

          <!-- Topbar Navbar -->
          <!-- <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div> -->


            <!-- Nav Item - User Information -->
            <!-- <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-lg-inline text-gray-600 small" id="name">Hello, <?php echo $this->session->userdata("nama"); ?></span>
              </a> -->
              <!-- Dropdown - User Information -->
              <!-- <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo base_url(); ?>C_login/logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div> -->
            <!-- </li> -->

          <!-- </ul>

        </nav> -->
        <!-- End of Topbar -->

						<?php
				        if (isset($content_page)){
				        $this->load->view($content_page);}
				    ?>

      <!-- Footer -->
      <!-- <footer class="sticky-footer bg-white ftrfix">
        <div class="container my-auto">
          <div class="copyright my-auto"> -->
            <!-- <span>Copyright &copy; Sales Activity 2019</span> -->
            <nav class="navbar navbar-default navbar-fixed-bottom fotfix">
              <div class="container menufix">
                <a class="nav-lk text-center <?php if($this->uri->segment('2') == 'reminder' or $this->uri->segment('2') == '') echo 'nav-lk-active'; ?>" href="<?php echo base_url(); ?>C_sistem/reminder">
                  <i class="fas fa-fw fa-book fa-lg"></i><br>
                  <span>Reminder</span>
                </a>
                <a class="nav-lk text-center <?php if($this->uri->segment('2') == 'realisasi' or $this->uri->segment('2') == 'detailrealisasi') echo 'nav-lk-active'; ?>" href="<?php echo base_url(); ?>C_sistem/realisasi">
                  <i class="fas fa-fw fa-list fa-lg"></i><br>
                  <span>Inquiry</span>
                </a>
                <a href="<?php echo base_url(); ?>C_report/tampildo" class="nav-lk text-center <?php if($this->uri->segment('2') == 'do' or $this->uri->segment('1') == 'C_report' or $this->uri->segment('2') == 'filterdo') echo 'nav-lk-active'; ?>">
                  <i class="fas fa-fw fa-wrench fa-lg"></i><br>
                  <span>Report DO</span>
                </a>
              </div>
            </nav>
          <!-- </div>
        </div>
      </footer> -->
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>


  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <!-- <script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script> -->

	<!-- jQuery library -->
	<script src="<?php echo base_url(); ?>assets/jquery-3.3.1.min.js"></script>
	<!-- Select2 JS -->
	<script src="<?php echo base_url(); ?>assets/select2/dist/js/select2.js?x=<?php echo rand(100,10000); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/select2/dist/js/select2-dropdownPosition.js?x=<?php echo rand(100,10000); ?>"></script>

  <!-- Page level custom scripts -->
  <!-- <script src="<?php echo base_url(); ?>assets/js/demo/chart-area-demo.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>assets/js/demo/chart-pie-demo.js"></script> -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

  <script src="https:////cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.chained.min.js"></script>

   <script type="text/javascript">

      function gantimotor(a,b,c,d,e,f,g,h,i,j,k){
         $('#tipe3').val(a);
         $('#tipe3').change();
         $('#warna3').val(b);
         $('input[name=nama]').val(c);
         $('input[name=source]').val(d);
         $('input[name=tahun]').val(e);
         $('#status2').val(f);
         $('textarea[name=alamat]').val(g);
         $('input[name=hp]').val(h);
         $('input[name=idren]').val(i);
         $('input[name=idcust]').val(j);
         $('input[name=idsbrcust]').val(k);

       }

   $('#nohp').change(function(){
     var id=$(this).val();
     if(id.charAt(0) != 0){
       alert("Format No Telp salah!");
       $('#nohp').val('');
     }else if(id.length < 8){
       alert("Format No Telp salah!");
       $('#nohp').val('');
     }else if(id.length > 13){
       alert("Format No Telp salah!");
       $('#nohp').val('');
     }
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/gethp",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if (data != ''){
         alert("Nomor yang anda masukkan sudah terdaftar pada sales "+data.nama+".\nJika ingin menggunakan nomor tersebut silahkan pilih kolom Customer Lama atau hubungi sales bersangkutan.");
         $('#nohp').val('');
         }
       }
     });
     });

   $('#hpupdateinq').change(function(){
     var id2 = $('#idcst').val();
     var hpbaru = $(this).val();
     if(hpbaru.charAt(0) != 0){
       alert("Format No Telp salah!");
       $('#hpupdateinq').val('');
     }else if(hpbaru.length < 8){
       alert("Format No Telp salah!");
       $('#hpupdateinq').val('');
     }else if(hpbaru.length > 13){
       alert("Format No Telp salah!");
       $('#hpupdateinq').val('');
     }
     id = id2+';'+hpbaru;
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/cekhpold",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if(data != ''){
           alert("No HP sudah digunakan oleh Customer Lain!");
           $('#hpupdateinq, #hpupdaterem').val('');
         }
         // alert("ok");
       },
       error: function(data){
         alert("error");
       }
     });
   });

   $('#ktp, #ktp2, #ktp3').change(function(){
     var id=$(this).val();
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/getktp",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if (data != '' && data[0].id_login == $('#idlogin').val()){
         x = confirm('KTP yang anda masukkan sudah terdaftar atas nama '+data[0].nama+ '\nApakah anda akan Replace?');
       }if(data[0].id_login != $('#idlogin').val()){
         var a = data[0].id_login;
         var b = data[0].nama;
         getsales(a,b);
       }
         if(x){
           $('#idcst').val(data[0].id_customer);
           $('input[name=namacust]').val(data[0].nama);
           $('input[name=tempatlahir]').val(data[0].tmp_lahir);
           $('input[name=tanggallahir]').val(data[0].tgl_lahir);
           $('select[name=jk]').val(data[0].jenis_kelamin);
           $('textarea[name=alamat]').val(data[0].alamat);
           $('input[name=kota]').val(data[0].kota);
           $('input[name=kodepos]').val(data[0].kode_pos);
           $('input[name=hp]').val(data[0].hp);
         }else{
           $('#ktp, #ktp2, #ktp3').val('');
           return false;
         }
       }
     });
   });

   function getsales(a,b){
     // console.log(a,b);
     var id = a;
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/getsales",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         alert('KTP yang anda masukkan sudah terdaftar atas nama '+b+' dengan sales ' +data[0].nama+'. \nSilahkan hubungi sales yang bersangkutan.');
         $('#ktp, #ktp2, #ktp3').val('');
       }
     });
   }


   $('#tipe2, #tipe, #tipe3, #tipe5').change(function(){
			var id=$(this).val();
			$.ajax({
				url : "<?php echo base_url();?>C_sistem/getwarna",
				method : "POST",
				data : {id: id},
				async : false,
		    dataType : 'json',
				success: function(data){
					var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<option value="'+data[i].id_invattropt+'">'+data[i].val+'</option>';
		            }
		            $('#warna2, #warna, #warna3, #warna5').html(html);
                // $('#status2').val(1);
                // $('#status2').change();
				}

			});
      $.ajax({
				url : "<?php echo base_url();?>C_sistem/getharga",
				method : "POST",
				data : {id: id},
				async : false,
		    dataType : 'json',
				success: function(data){
          var i;
          for(i=0; i<data.length; i++){
					$('#ofr,#ofr2').val(data[i].price_1);
          $('#b,#b2').val(data[i].price_2);
        }
        sum();
        sum2();

				}
			});
		});
    </script>

  <script>
  $(document).ready(function() {
    $('#datepicker').datepicker({
      format: 'dd-mm-yyyy'
    });
    // <?php if(@$tes->id_inventori != NULL) {?>
    //   $('#tipe2').val(<?php echo $tes->id_inventori ?>);
    //   $('#tipe2').change();
    //   <?php if($wrn->id_invattropt != NULL) { ?>
    //     $('#warna2').val(<?php echo $wrn->id_invattropt ?>);
    // <?php } } ?>

  });
  </script>

  <script>
    // $(document).ready(function(){
    //     <?php if(@$motor->id_inventori != NULL) {?>
    //     $('#tipe3').val(<?php echo $motor->id_inventori ?>);
    //     $('#tipe3').change();
    //     <?php if($warna->id_invattropt != NULL) { ?>
    //       $('#warna3').val(<?php echo $warna->id_invattropt ?>);
    //   <?php } } ?>
    // });
  </script>

  <script>
    // $(document).ready(function(){
    //   <?php if(@$motor->id_inventori != NULL) {?>
    //     $('#tipe5').val(<?php echo $motor->id_inventori ?>);
    //     $('#tipe5').change();
    //     <?php if($warna->id_invattropt != NULL) { ?>
    //       $('#warna5').val(<?php echo $warna->id_invattropt ?>);
    //   <?php } } ?>
    // });
  </script>

  <script>

  function konfirmasi(){
    statusakhir = $('#laststatus2, #lasthasil2').val();
    status = $('#status2, #hasil').val();
    if($('#hpupdateinq, #hpupdaterem, #hpinquiry').val()== ''){
      alert('No HP tidak boleh kosong!');
      return false;
    }
    if(statusakhir > status){
      alert('Maaf Status Customer Salah!');
      return false;
    }else{
      x = confirm('Apakah anda yakin untuk mengupdate Inquiry ini?');
      if(x) return true;
      else return false;
    }
  }

  function sum() {
        var yesno = $('#yes').val();
        var a = document.getElementById('ofr').value;
        var b = document.getElementById('b').value;
        var c = document.getElementById('a').value;
        if(yesno == 'Y'){
          result = parseInt(a) + parseInt(b);
          $('#valbbn2').val(b);
        }
        else{
          result = parseInt(a) + parseInt(c);
          $('#valbbn2').val(c);
        }
        $('#otr').val(result);
  }
  </script>

  <script type="text/javascript">

  function sum2() {
        var yesno = $('#yes2').val();
        var a = document.getElementById('ofr2').value;
        var b = document.getElementById('b2').value;
        var c = document.getElementById('a2').value;
        if(yesno == 'Y'){
          result = parseInt(a) + parseInt(b);
          $('#valbbn').val(b);
        }
        else {
          result = parseInt(a) + parseInt(c);
          $('#valbbn').val(c);
        }
        $('#otr2').val(result);
  }
  </script>

      <script>
      $(document).ready(function() {
      var table = $('#myTable').DataTable( {
          // rowReorder: {
          //     selector: 'td:nth-child(2)'
          // },
          responsive: true,
          "columnDefs": [
            { "visible": false, "targets": 0 }
          ]
      } );
      $('#dttable').DataTable({
        responsive:true
      });

      } );
      </script>

      <script type="text/javascript">
        $(document).ready(function(){
              $('ul li a').click(function(){
                $('li a').removeClass("active");
                $(this).addClass("active");
            });
        });
      </script>
      <script>
        $("#bbn2").show();
        $("#bbn1").hide();
        $("#yes").change(function(){
          if($("#yes").val() == 'N'){
          $("#bbn1").show();
          $("#bbn2").hide();
          }
          else if ($("#yes").val() == 'Y'){
            $("#bbn1").hide();
            $("#bbn2").show();
          }
          sum();
        });
        $("#bbn4").show();
        $("#bbn3").show();
        $("#yes2").change(function(){
          if($("#yes2").val() == 'N'){
          $("#bbn3").show();
          $("#bbn4").hide();
          }
          else if ($("#yes2").val() == 'Y'){
            $("#bbn3").hide();
            $("#bbn4").show();
          }
          sum2();
        });

        $("#jenis").change(function(){
          if($("#jenis").val() == 'N'){
          $("#yes2").val('N');
          $("#yes2").change();
          $("#otr2").val(0);
          $("#otr2").prop('readonly',false);
          }
          else if ($("#jenis").val() == 'Y'){
            $("#yes2").val('Y');
            $("#yes2").change();
            $("#otr2").prop('readonly',true);
          }
        });

        $("#jenis2").change(function(){
          if($("#jenis2").val() == 'N'){
          $("#yes").val('N');
          $("#yes").change();
          $("#otr").val(0);
          $("#otr").prop('readonly',false);
          }
          else if ($("#jenis2").val() == 'Y'){
            $("#yes").val('Y');
            $("#yes").change();
            $("#otr").prop('readonly',true);
          }
        });
      </script>

      <script>
          $(document).ready(function(){
          $("#box").hide();
          $("#box2").hide();
          $("#select").hide();
          $("#cust").change(function(){
           if($("#cust").val() == 'Baru'){
              //Show text box here
              $("#box").show();
              $("#box2").show();
              $("#select").hide();
              $("#cekro").prop('checked', false);
              $("#cekro").prop('disabled', false);
              $("#hidero").val(0);
           }
           else if($("#cust").val() == 'Lama'){
             //Hide text box here
             $("#select").show();
             $("#box").hide();
             $("#box2").show();
             $("#nohp").prop('required',false);
             $("#cekro").prop('checked', true);
             $("#cekro").prop('disabled', true);
             $("#hidero").val(1);
	     $("#nm").prop('required',false);
           }
           else{

             $("#select").hide();
             $("#box2").hide();
             $("#box").hide();

           }
         });
          });

          $(".listfitur").hide();
          $("#btnfitur").click(function(){
            $(".listfitur").toggle();
          });

          $(".listspek").hide();
          $("#btnspek").click(function(){
            $(".listspek").toggle();
          });

          $("#cekro").change(function(){
            if($(this).prop("checked") == true){
            $("#hidero").val(1);
            }
            else if($(this).prop("checked") == false){
            $("#hidero").val(0);
            }
          });
      </script>

      <script>
        $(document).ready(function(){
        $("#nama").change(function(){
          if(("#nama").val() == 0){
            $("#nama").hide();
          }
        });
        });
      </script>


      <script>
          $("#datacust").hide();
          $("#hasil").change(function(){
            if($('#nama-cust').val() != 'Tidak ada customer') {
           if($("#hasil").val() == '5'){
              //Show text box here
              $("#datacust").show();
              $("#nomanual3, #ktp3").prop('required',true);
           }
           else {
             $("#datacust").hide();
             $("#nomanual3, #ktp3").prop('required',false);
           }
         }
            });
      </script>

      <script>
          $("#spk2").hide();
          $("#status2").change(function(){
           if($("#status2").val() == '5'){
              //Show text box here
              $("#spk2").show();
              sum2();
              $("#nomanual2, #ktp2").prop('required',true);
           }
           else {
             $("#spk2").hide();
             $("#nomanual2, #ktp2").prop('required',false);
           }
            });

            function tampilmot(i){
              $(".mot").hide();
              $("#mot"+i).show();
            }
      </script>

      <script>
      $("#spk").hide();
      $("#kategori").change(function(){
       if($("#kategori").val() == '5'){
          //Show text box here
          $("#spk").show();
          $("#nomanual, #ktp").prop('required',true);
       }
       else {
         $("#spk").hide();
         $("#nomanual, #ktp").prop('required',false);
       }
        });
      </script>
      <script>
        function trailtelp(a){
          var id=a;
          $.ajax({
            url : "<?php echo base_url();?>C_sistem/trailtelp",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json'
          });
        }
        function trailwa(a){
          var id=a;
          $.ajax({
            url : "<?php echo base_url();?>C_sistem/trailwa",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json'
          });
        }
      </script>

      <?php if(isset($_GET['ur'])){
        if($_GET['ur']==1){
   ?>
      <script>
        alert('Data Berhasil Diubah!');
      </script>
  <?php } } ?>
<!-- START CREATED AND UPDATED BY AJI FIRMAN -->
<script>
//START FUNCTION HANYA ANGKA
function hanyaAngka(evt) {//START CODING LOCK NUMBER IN TEXTBOX
	var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))

		return false;
	return true;
}//END CODING LOCK NUMBER IN TEXTBOX
//END FUNCTION HANYA ANGKA
	$(document).ready(function(){
		//START FOR FORMAT BOOTSTRAPS
		$('#date, #tglreminder').datepicker({
      format: 'dd-mm-yyyy'
    });
		//END FOR FORMAT BOOTSTRAPS
		//START FOR PAGE INQUIRY - CUSTOMER LAMA
		$("#cust_lama").select2({//VERSION NEW AUTOCOMPLETE
        dropdownParent: $("#exampleModal"),
				delay: 500,
				minimumInputLength: 3,
				allowClear: true,
				ajax: {
					url: '<?php echo base_url(); ?>C_sistem/get_select_customer',//get_select_customer
					type: "post",
					dataType: 'json',
					data: function (params) {
						return {
								searchTerm: params.term // search term
						};
					},
					processResults: function (response) {
						return {
								results: response
						};
					},
					cache: false
				}
		});
		//END FOR PAGE INQUIRY - CUSTOMER LAMA
    // $("#tipe").data("select2").on("results:message", function () {
    //     this.dropdown._positionDropdown();
    // });
    // var select2Instance = $("#tipe").data('select2');
    // select2Instance.on('results:message', function(params){
    //     this.dropdown._resizeDropdown();
    //     this.dropdown._positionDropdown();
    // });
    // $("#tipe").data('select2').on('open', function (e) {
    //     this.results.clear();
    //     this.dropdown._positionDropdown();
    // })
		//START FOR PAGE INQUIRY - TIPE MOTORS
		$("#tipe").select2({//VERSION NEW AUTOCOMPLETE
          dropdownParent: $("#exampleModal"),
          // positionDropdown: $("#exampleModal"),
          dropdownPosition: 'below',
          resizeDropdown: true,
					delay: 500,
					minimumInputLength: 3,
					allowClear: true,
			  	ajax: {
			   		url: '<?php echo base_url(); ?>C_sistem/get_select_motor',//get_select_customer
			   		type: "post",
			   		dataType: 'json',
			   		data: function (params) {
			    		return {
			      			searchTerm: params.term // search term
			    		};
			   		},
			   		processResults: function (response) {
			     		return {
			        		results: response
			     		};
			   		},
			   		cache: false
			  	}
		});
		//END FOR PAGE INQUIRY - TIPE MOTOR
		//START FOR PAGE UPDATE INQUIRY - TIPE MOTOR UPDATE
		$("#tipe3").select2({//VERSION NEW AUTOCOMPLETE
			delay: 500,
			minimumInputLength: 3,
			allowClear: true,
			ajax: {
				url: '<?php echo base_url(); ?>C_sistem/get_select_motor',//get_select_customer
				appendTo: "#exampleModal",//this is the key!!!!
				type: "post",
				dataType: 'json',
				// async : false,
				// delay: 10,
				data: function (params) {
					// alert(params);
					return {
							searchTerm: params.term // search term
					};
				},
				processResults: function (response) {
					// alert(response);
					return {
							results: response
					};
				},
				cache: false
			}
		});
			//START SET SELECT2 FOR TO BE SELECTED
		var $newOption = $("<option selected='selected'></option>").val("<?php echo @$mtr_det[0]['id_inventori'] ?>").text("<?php echo @$mtr_det[0]['kode_original_inv'].' | '.@$mtr_det[0]['nama'] ?>")
		$("#tipe3").append($newOption).trigger('change');
        $('#warna3').val(<?= @$warna->id_invattropt ?>); 
        $('#warna3').change();
			//START SET SELECT2 FOR TO BE SELECTED
		//END FOR PAGE UPDATE INQUIRY - TIPE MOTOR UPDATE
		//START FOR PAGE UPDATE INQUIRY AT PAGE REMINDER - TIPE MOTOR UPDATE
		$("#tipe2").select2({//VERSION NEW AUTOCOMPLETE
            dropdownPosition: 'below',
			delay: 500,
			minimumInputLength: 3,
			allowClear: true,
			ajax: {
				url: '<?php echo base_url(); ?>C_sistem/get_select_motor',//get_select_customer
				// appendTo: "#exampleModal",//this is the key!!!!
				type: "post",
				dataType: 'json',
				// async : false,
				// delay: 10,
				data: function (params) {
					// alert(params);
					return {
							searchTerm: params.term // search term
					};
				},
				processResults: function (response) {
					// alert(response);
					return {
							results: response
					};
				},
				cache: false
			}
		});
			//START SET SELECT2 FOR TO BE SELECTED
		var $newOption = $("<option selected='selected'></option>").val("<?php echo @$mtr_det[0]['id_inventori'] ?>").text("<?php echo @$mtr_det[0]['kode_original_inv'].' | '.@$mtr_det[0]['nama'] ?>")
		$("#tipe2").append($newOption).trigger('change');
        $('#warna2').val(<?= @$wrn->id_invattropt ?>); 
        $('#warna2').change();
			//START SET SELECT2 FOR TO BE SELECTED
		//END FOR PAGE UPDATE INQUIRY AT PAGE REMINDER - TIPE MOTOR UPDATE
		//START FOR PAGE UPDATE DO - TIPE MOTOR UPDATE
		$("#tipe5").select2({//VERSION NEW AUTOCOMPLETE
			delay: 500,
			minimumInputLength: 3,
			allowClear: true,
			ajax: {
				url: '<?php echo base_url(); ?>C_sistem/get_select_motor',//get_select_customer
				// appendTo: "#exampleModal",//this is the key!!!!
				type: "post",
				dataType: 'json',
				// async : false,
				// delay: 10,
				data: function (params) {
					// alert(params);
					return {
							searchTerm: params.term // search term
					};
				},
				processResults: function (response) {
					// alert(response);
					return {
							results: response
					};
				},
				cache: false
			}
		});
			//START SET SELECT2 FOR TO BE SELECTED
		var $newOption = $("<option selected='selected'></option>").val("<?php echo @$mtr_det[0]['id_inventori'] ?>").text("<?php echo @$mtr_det[0]['kode_original_inv'].' | '.@$mtr_det[0]['nama'] ?>")
		$("#tipe5").append($newOption).trigger('change');
        $('#warna5').val(<?= @$warna->id_invattropt ?>); 
        $('#warna5').change();
			//START SET SELECT2 FOR TO BE SELECTED
		//END FOR PAGE UPDATE DO - TIPE MOTOR UPDATE

  //START SUBMIT BTN page inquiry -> Tambah Inquiry
  // $('#btn_saves_inputRealisasi').submit(function(e) {
  //     // e.preventDefault();
  //     alert('Test Alert btn save realisasi');
  //     // save_inputRealisasi();
  // });
  $('#btns_saves_inputRealisasi').click(function(){
    // alert('Test Button ajax!');
    cust_tagID = document.getElementById("cust").value;
    //start bagian cust lama
    cust_lama = document.getElementById("cust_lama").value;
    //end bagin cust lama
    //start bagian cust baru
    nm = document.getElementById("nm").value;
    nohp = document.getElementById("nohp").value;
    almt = document.getElementById("almt").value;
    //end bagian cust baru
    tipe = document.getElementById("tipe").value;
    warna = document.getElementById("warna").value;
    tahun = document.getElementById("tahun").value;
    sumbercust = document.getElementById("sumbercust").value;
    kategori = document.getElementById("kategori").value;
    ket = document.getElementById("ket").value;
    //start bagian SPK
    nomanual = document.getElementById("nomanual").value;
    namacust = document.getElementById("namacust").value;
    tempatlahir = document.getElementById("tempatlahir").value;
    date = document.getElementById("date").value;
    ktp = document.getElementById("ktp").value;
    jk = document.getElementById("jk").value;
    alamat = document.getElementById("alamat").value;
    kota = document.getElementById("kota").value;
    kodepos = document.getElementById("kodepos").value;
    hp = document.getElementById("hp").value;
    payment = document.getElementById("payment").value;
    jenis2 = document.getElementById("jenis2").value;
    otr = document.getElementById("otr").value;
    paymethod = document.getElementById("paymethod").value;
    dp = document.getElementById("dp").value;
    //end bagian SPK
    //start added tag id
    idcst = document.getElementById("idcst").value;
    hidero = document.getElementById("hidero").value;
    yes = document.getElementById("yes").value;
    //end added tag id
    //start penambahan variabel tag name
    customer = document.getElementsByName("customer")[0].value;
    cust_tagName = document.getElementsByName("cust")[0].value;
    nama = document.getElementsByName("nama")[0].value;
    telp = document.getElementsByName("telp")[0].value;
    almt = document.getElementsByName("almt")[0].value;
    //
    hslaktiv = document.getElementsByName("hslaktiv")[0].value;
    cekro = document.getElementsByName("cekro")[0].value;
    identitas = document.getElementsByName("identitas")[0].value;
    offroad = document.getElementsByName("offroad")[0].value;
    ontheroad = document.getElementsByName("ontheroad")[0].value;
    bbn2 = document.getElementsByName("bbn2")[0].value;
    subsididp = document.getElementsByName("subsididp")[0].value;
    yesno = document.getElementsByName("yesno")[0].value;
    //end penambahan variabel tag name
    // alert("test pertama " + customer + "|" + cust);
    if($("#cust").val() == ''){
      alert("Harap Pilih Kembali Customer Dengan Benar!!!");
    }else if($("#cust").val() != '' || cust != ""){
      // $("#cust").change(function(){
        // if($("#cust").val() != 'Lama' || $("#cust").val() != 'Baru'){//jika tidak dipilih
        //   alert("Harap Pilih Kembali Customer Dengan Benar!!!v3");
        // }else 
        if($("#cust").val() == 'Lama' || $("#cust").val() == 'Baru'){
          if($("#cust").val() == 'Lama'){
            if(cust_lama == ""){
              alert("Harap Pilih Kembali Data Customer Lama Dengan Benar!!");
            }
          }else if($("#cust").val() == 'Baru'){
            if(nm == ""){
              alert("Harap Input Kembali Data Nama Customer Dengan Benar!!");
            }else if(nohp == ""){
              alert("Harap Input Kembali Data NO HP Customer Dengan Benar!!");
            }else if(almt == ""){
              alert("Harap Input Kembali Data ALamat Customer Dengan Benar!!");
            }
          }
          // if($("#cust").val() == 'Lama' || $("#cust").val() == 'Baru'){
            if(tipe == ""){
              alert("Harap Pilih Kembali Tipe Motor Dengan Benar!!");
            }else if(warna == ""){
              alert("Harap Pilih Kembali Warna Dengan Benar!!");
            }else if(tahun == ""){
              alert("Harap Input Kembali Tahun Dengan Benar!!");
            }else if(sumbercust == ""){
              alert("Harap Pilih Kembali Source Dengan Benar!!");
            }else if($("#kategori").val() == ''){
              alert("Harap Pilih Kembali Kategori Dengan Benar!!");
            }
            // else if(ket == ""){//field ini hanya optional
            //   alert("Harap Input Kembali Keterangan Dengan Benar!!");
            // }
          // }
        }
      // });
    }
    if(kategori == 5){//Jika pilih SPK/
      if(nomanual == ""){
        alert("Harap Input Kembali NO Manual(SPK) Dengan Benar!!");
      }else if(namacust == ""){
        alert("Harap Input Kembali Nama Customer(SPK) Dengan Benar!!");
      }else if(tempatlahir == ""){
        alert("Harap Input Kembali Tempat Lahir Dengan Benar!!");
      }else if(date == ""){
        alert("Harap Pilih Kembali Tanggal Lahir Dengan Benar!!");
      }else if(ktp == ""){
        alert("Harap Input Kembali Identitas/KTP Dengan Benar!!");
      }else if(jk == ""){
        alert("Harap Pilih Kembali Jenis Kelamin Dengan Benar!!");
      }else if(alamat == ""){
        alert("Harap Input Kembali ALamat Dengan Benar!!");
      }else if(kota == ""){
        alert("Harap Input Kembali Kota Dengan Benar!!");
      }else if(kodepos == ""){
        alert("Harap Input Kembali Kode Pos Dengan Benar!!");
      }else if(hp == ""){
        alert("Harap Input Kembali HP Dengan Benar!!");
      }else if(payment == ""){
        alert("Harap Pilih Kembali Payment Model Dengan Benar!!");
      }else if(jenis2 == ""){
        alert("Harap Pilih Kembali Jenis Penjualan Dengan Benar!!");
      }else if(otr == ""){
        alert("Harap Input Kembali Harga Unit Dengan Benar!!");
      }else if(paymethod == ""){
        alert("Harap Input Kembali Payment Method Dengan Benar!!");
      }else if(dp == ""){
        alert("Harap Input Kembali DP/Uang Muka Dengan Benar!!");
      }
    }
    // alert('Pengecekan Selesai!!');
    //START ACTION SAVE INPUT INQUIRY
    if($("#cust").val() != '' || $("#cust").val() == 'Lama' || $("#cust").val() == 'Baru'){
      if($("#cust").val() == 'Lama'){
        if(cust_lama != '' && tipe != '' && warna != '' && tahun != '' && sumbercust != ''){
          if(kategori == 5){//jika pilih kategori "SPK/DP"
            if(nomanual != '' && namacust != '' && tempatlahir != '' && date != ''
              && ktp != '' && jk != '' && alamat != '' && kota != '' && kodepos != ''
              && hp != '' && payment != '' && jenis2 != '' && otr != '' && paymethod != ''
              && dp != ''){//harus terisi
                let confirmAction = confirm("Apa anda yakin untuk membuat inquiry ini?");
                if (confirmAction) {
                  // alert("Action successfully executed");
                  $.ajax({
                      url: '<?php echo base_url(); ?>C_sistem/inputrencana',
                      type: 'POST',
                      // data: {noinduk:noinduk,nama:nama,alamat:alamat,hobi:hobi},
                      data: {cust_tagID:cust_tagID,cust_lama:cust_lama,nm:nm,nohp:nohp,almt:almt,tipe:tipe,warna:warna,tahun:tahun,
                            sumbercust:sumbercust,kategori:kategori,ket:ket,nomanual:nomanual,namacust:namacust,
                            tempatlahir:tempatlahir,date:date,ktp:ktp,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                            hp:hp,payment:payment,jenis2:jenis2,otr:otr,paymethod:paymethod,dp:dp,customer:customer,
                            cust:cust_tagName,nama:nama,telp:telp,almt:almt,hslaktiv:hslaktiv,hidero:hidero,cekro:cekro,
                            identitas:identitas,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,subsididp:subsididp,
                            yesno:yesno,yes:yes},
                            //patokan batas akhir pnambahan = dp:dp
                      success: function(response){
                        alert('Berhasil Simpan Data Tambah Inquiry!');
                        window.location.reload();
                      }
                  })
                } else {
                  alert("Batal Simpan Tambah Inquiry!");
                }
            }
          }else{//jika tidak pilih kategori selain id = 5 / "SPK?DP"
            let confirmAction = confirm("Apa anda yakin untuk membuat inquiry ini?");
            if (confirmAction) {
              // alert("Action successfully executed");
              $.ajax({
                  url: '<?php echo base_url(); ?>C_sistem/inputrencana',
                  type: 'POST',
                  // data: {noinduk:noinduk,nama:nama,alamat:alamat,hobi:hobi},
                  data: {cust_tagID:cust_tagID,cust_lama:cust_lama,nm:nm,nohp:nohp,almt:almt,tipe:tipe,warna:warna,tahun:tahun,
                        sumbercust:sumbercust,kategori:kategori,ket:ket,nomanual:nomanual,namacust:namacust,
                        tempatlahir:tempatlahir,date:date,ktp:ktp,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                        hp:hp,payment:payment,jenis2:jenis2,otr:otr,paymethod:paymethod,dp:dp,customer:customer,
                        cust:cust_tagName,nama:nama,telp:telp,almt:almt,hslaktiv:hslaktiv,hidero:hidero,cekro:cekro,
                        identitas:identitas,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,subsididp:subsididp,
                        yesno:yesno,yes:yes},
                        //patokan batas akhir pnambahan = dp:dp
                  success: function(response){
                    alert('Berhasil Simpan Data Tambah Inquiry!');
                    window.location.reload();
                  }
              })
            } else {
              alert("Batal Simpan Tambah Inquiry!");
            }
          }
          //
        }
      }else if($("#cust").val() == 'Baru'){
        if(nm != '' && nohp != '' && almt != '' && tipe != '' && warna != '' && tahun != '' && sumbercust != ''){
          if(kategori == 5){//jika pilih kategori "SPK/DP"
            if(nomanual != '' && namacust != '' && tempatlahir != '' && date != ''
              && ktp != '' && jk != '' && alamat != '' && kota != '' && kodepos != ''
              && hp != '' && payment != '' && jenis2 != '' && otr != '' && paymethod != ''
              && dp != ''){//harus terisi
                let confirmAction = confirm("Apa anda yakin untuk membuat inquiry ini?");
                if (confirmAction) {
                  // alert("Action successfully executed");
                  $.ajax({
                      url: '<?php echo base_url(); ?>C_sistem/inputrencana',
                      type: 'POST',
                      // data: {noinduk:noinduk,nama:nama,alamat:alamat,hobi:hobi},
                      data: {cust_tagID:cust_tagID,cust_lama:cust_lama,nm:nm,nohp:nohp,almt:almt,tipe:tipe,warna:warna,tahun:tahun,
                            sumbercust:sumbercust,kategori:kategori,ket:ket,nomanual:nomanual,namacust:namacust,
                            tempatlahir:tempatlahir,date:date,ktp:ktp,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                            hp:hp,payment:payment,jenis2:jenis2,otr:otr,paymethod:paymethod,dp:dp,customer:customer,
                            cust:cust_tagName,nama:nama,telp:telp,almt:almt,hslaktiv:hslaktiv,hidero:hidero,cekro:cekro,
                            identitas:identitas,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,subsididp:subsididp,
                            yesno:yesno,yes:yes},
                            //patokan batas akhir pnambahan = dp:dp
                      success: function(response){
                        alert('Berhasil Simpan Data Tambah Inquiry!');
                        window.location.reload();
                      }
                  })
                } else {
                  alert("Batal Simpan Tambah Inquiry!");
                }
            }
          }else{//jika tidak pilih kategori selain id = 5 / "SPK?DP"
            let confirmAction = confirm("Apa anda yakin untuk membuat inquiry ini?");
            if (confirmAction) {
              // alert("Action successfully executed");
              $.ajax({
                  url: '<?php echo base_url(); ?>C_sistem/inputrencana',
                  type: 'POST',
                  // data: {noinduk:noinduk,nama:nama,alamat:alamat,hobi:hobi},
                  data: {cust_tagID:cust_tagID,cust_lama:cust_lama,nm:nm,nohp:nohp,almt:almt,tipe:tipe,warna:warna,tahun:tahun,
                        sumbercust:sumbercust,kategori:kategori,ket:ket,nomanual:nomanual,namacust:namacust,
                        tempatlahir:tempatlahir,date:date,ktp:ktp,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                        hp:hp,payment:payment,jenis2:jenis2,otr:otr,paymethod:paymethod,dp:dp,customer:customer,
                        cust:cust_tagName,nama:nama,telp:telp,almt:almt,hslaktiv:hslaktiv,hidero:hidero,cekro:cekro,
                        identitas:identitas,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,subsididp:subsididp,
                        yesno:yesno,yes:yes},
                        //patokan batas akhir pnambahan = dp:dp
                  success: function(response){
                    alert('Berhasil Simpan Data Tambah Inquiry!');
                    window.location.reload();
                  }
              })
            } else {
              alert("Batal Simpan Tambah Inquiry!");
            }
          }
          //
        }
      }
    }
    //END ACTION SAVE INPUT INQUIRY
  });
  //END SUBMIT BTN page inquiry -> Tambah Inquiry

  //START SUBMIT BTN PAGE UPDATE INQUIRY -> Update Inquiry
  $('#btns_updates_updateRealisasi').click(function(){
    //START TAG NAME VARIABEL Update Inquiry
    idcust = document.getElementsByName("idcust")[0].value;
    sumbercust = document.getElementsByName("sumbercust")[0].value;
    hslaktiv = document.getElementsByName("hslaktiv")[0].value;
    tipe = document.getElementsByName("tipe")[0].value;
    warna = document.getElementsByName("warna")[0].value;
    tahun = document.getElementsByName("tahun")[0].value;
    idren = document.getElementsByName("idren")[0].value;
    idrendet = document.getElementsByName("idrendet")[0].value;
    ket = document.getElementsByName("ket")[0].value;
    namacust = document.getElementsByName("namacust")[0].value;
    tempatlahir = document.getElementsByName("tempatlahir")[0].value;
    tanggallahir = document.getElementsByName("tanggallahir")[0].value;
    identitas = document.getElementsByName("identitas")[0].value;
    jk = document.getElementsByName("jk")[0].value;
    alamat = document.getElementsByName("alamat")[0].value;
    kota = document.getElementsByName("kota")[0].value;
    kodepos = document.getElementsByName("kodepos")[0].value;
    hp = document.getElementsByName("hp")[0].value;
    offroad = document.getElementsByName("offroad")[0].value;
    ontheroad = document.getElementsByName("ontheroad")[0].value;
    bbn2 = document.getElementsByName("bbn2")[0].value;
    payment = document.getElementsByName("payment")[0].value;
    nomanual = document.getElementsByName("nomanual")[0].value;
    subsididp = document.getElementsByName("subsididp")[0].value;
    yesno = document.getElementsByName("yesno")[0].value;
    dp = document.getElementsByName("dp")[0].value;
    paymethod = document.getElementsByName("paymethod")[0].value;
    jenis = document.getElementsByName("jenis")[0].value;
    //start paramater tambahan
    statusakhir = $('#laststatus2, #lasthasil2').val();
    status = $('#status2, #hasil').val();
    //end parameter tambahan
    //END TAG NAME VARIABEL Update Inquiry
    if($("#tipe3").val() == '' || tipe == ''){
      alert("Harap Pilih Kembali Tipe Motor Dengan Benar!!");
    }else if($("#warna3").val() == '' || warna == ''){
      alert("Harap Pilih Kembali Warna Motor Dengan Benar!!");
    }else if(tahun == ''){
      alert("Harap Pilih Kembali Tahun Motor Dengan Benar!!");
    }
    if(hslaktiv == 5){//Jika pilih SPK/
      if(nomanual == ""){
        alert("Harap Input Kembali NO Manual(SPK) Dengan Benar!!");
      }else if(namacust == ""){
        alert("Harap Input Kembali Nama Customer(SPK) Dengan Benar!!");
      }else if(tempatlahir == ""){
        alert("Harap Input Kembali Tempat Lahir Dengan Benar!!");
      }else if(tanggallahir == ""){
        alert("Harap Pilih Kembali Tanggal Lahir Dengan Benar!!");
      }else if(identitas == ""){
        alert("Harap Input Kembali Identitas/KTP Dengan Benar!!");
      }else if(jk == ""){
        alert("Harap Pilih Kembali Jenis Kelamin Dengan Benar!!");
      }else if(alamat == ""){
        alert("Harap Input Kembali ALamat Dengan Benar!!");
      }else if(kota == ""){
        alert("Harap Input Kembali Kota Dengan Benar!!");
      }else if(kodepos == ""){
        alert("Harap Input Kembali Kode Pos Dengan Benar!!");
      }else if(hp == ""){
        alert("Harap Input Kembali HP Dengan Benar!!");
      }else if(payment == ""){
        alert("Harap Pilih Kembali Payment Model Dengan Benar!!");
      }else if(jenis == ""){
        alert("Harap Pilih Kembali Jenis Penjualan Dengan Benar!!");
      }else if(ontheroad == ""){
        alert("Harap Input Kembali Harga Unit Dengan Benar!!");
      }else if(paymethod == ""){
        alert("Harap Input Kembali Payment Method Dengan Benar!!");
      }else if(dp == ""){
        alert("Harap Input Kembali DP/Uang Muka Dengan Benar!!");
      }
    }
    //START ACTION UPDATE INQUIRY
    if($("#tipe3").val() != '' && $("#warna3").val() != '' && tahun != ''){
      if(statusakhir > status){
        alert('Maaf Status Customer Salah!');
        return false;
      }else{
        if(hslaktiv == 5){//jika kategori dipilih "SPK/DP"
          if(nomanual != '' && namacust != '' && tempatlahir != '' && tanggallahir !== ''
            && identitas != '' && jk != '' && alamat != '' && kota != '' && kodepos != ''
            && hp != '' && payment != '' && jenis != '' && ontheroad != '' && paymethod != ''
            && dp != ''){//harus terisi
              let confirmAction = confirm("Apa anda yakin untuk Merubah Inquiry ini?");
            if (confirmAction) {
              // alert("Action successfully executed");
              $.ajax({
                  url: '<?php echo base_url(); ?>C_sistem/updateinq',
                  type: 'POST',
                  data: {idcust:idcust,sumbercust:sumbercust,hslaktiv:hslaktiv,tipe:tipe,warna:warna,
                        tahun:tahun,idren:idren,idrendet:idrendet,ket:ket,namacust:namacust,tempatlahir:tempatlahir,
                        tanggallahir:tanggallahir,identitas:identitas,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                        hp:hp,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,payment:payment,nomanual:nomanual,
                        subsididp:subsididp,yesno:yesno,dp:dp,paymethod:paymethod},
                  success: function(response){
                    alert('Berhasil Update Data Inquiry!');
                    // window.location.reload();
                    window.location.href="<?php echo base_url(); ?>C_sistem/realisasi";
                  }
              })
            } else {
              alert("Batal Update Data Inquiry!");
            }
          }
        }else{//jika kategori selain kategori = 5
          let confirmAction = confirm("Apa anda yakin untuk Merubah Inquiry ini?");
          if (confirmAction) {
            // alert("Action successfully executed");
            $.ajax({
                url: '<?php echo base_url(); ?>C_sistem/updateinq',
                type: 'POST',
                data: {idcust:idcust,sumbercust:sumbercust,hslaktiv:hslaktiv,tipe:tipe,warna:warna,
                      tahun:tahun,idren:idren,idrendet:idrendet,ket:ket,namacust:namacust,tempatlahir:tempatlahir,
                      tanggallahir:tanggallahir,identitas:identitas,jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,
                      hp:hp,offroad:offroad,ontheroad:ontheroad,bbn2:bbn2,payment:payment,nomanual:nomanual,
                      subsididp:subsididp,yesno:yesno,dp:dp,paymethod:paymethod},
                success: function(response){
                  alert('Berhasil Update Data Inquiry!');
                  // window.location.reload();
                  window.location.href="<?php echo base_url(); ?>C_sistem/realisasi";
                }
            })
          } else {
            alert("Batal Update Data Inquiry!");
          }
        }
      }
      //
    }
    //END ACTION UPDATE INQUIRY
  });
  //END SUBMIT BTN PAGE UPDATE INQUIRY -> Update Inquiry
  
  //START SUBMIT BTN PAGE UPDATE REMINDER -> Update REMINDER
  $('#btns_updates_updateReminder').click(function(){
    //START TAG NAME VARIABEL Update REMINDER
    tipe = document.getElementsByName("tipe")[0].value;
    id = document.getElementsByName("id")[0].value;
    hasil = document.getElementsByName("hasil")[0].value;
    idren = document.getElementsByName("idren")[0].value;
    warna = document.getElementsByName("warna")[0].value;
    tahun = document.getElementsByName("tahun")[0].value;
    nama = document.getElementsByName("nama")[0].value;
    namacust = document.getElementsByName("namacust")[0].value;
    tempatlahir = document.getElementsByName("tempatlahir")[0].value;
    tanggallahir = document.getElementsByName("tanggallahir")[0].value;
    identitas = document.getElementsByName("identitas")[0].value;
    jk = document.getElementsByName("jk")[0].value;
    alamat = document.getElementsByName("alamat")[0].value;
    kota = document.getElementsByName("kota")[0].value;
    kodepos = document.getElementsByName("kodepos")[0].value;
    hp = document.getElementsByName("hp")[0].value;
    offroad = document.getElementsByName("offroad")[0].value;
    ontheroad = document.getElementsByName("ontheroad")[0].value;
    bbn2 = document.getElementsByName("bbn2")[0].value;
    ket = document.getElementsByName("ket")[0].value;
    payment = document.getElementsByName("payment")[0].value;
    jenis2 = document.getElementsByName("jenis2")[0].value;
    nomanual = document.getElementsByName("nomanual")[0].value;
    source1 = document.getElementsByName("source1")[0].value;
    subsididp = document.getElementsByName("subsididp")[0].value;
    yesno = document.getElementsByName("yesno")[0].value;
    dp = document.getElementsByName("dp")[0].value;
    paymethod = document.getElementsByName("paymethod")[0].value;
    tgl = document.getElementsByName("tgl")[0].value;
    source = document.getElementsByName("source")[0].value;
    //END TAG NAME VARIABEL Update REMINDER
    //start paramater tambahan
    statusakhir = $('#laststatus2, #lasthasil2').val();
    status = $('#status2, #hasil').val();
    //end parameter tambahan
    if($("#tipe2").val() == '' || tipe == ''){
      alert("Harap Pilih Kembali Tipe Motor Dengan Benar!!");
    }else if($("#warna3").val() == '' || warna == ''){
      alert("Harap Pilih Kembali Warna Motor Dengan Benar!!");
    }else if(tahun == ''){
      alert("Harap Pilih Kembali Tahun Motor Dengan Benar!!");
    }
    if(hasil == 5){//Jika pilih SPK/
      if(nomanual == ""){
        alert("Harap Input Kembali NO Manual(SPK) Dengan Benar!!");
      }else if(namacust == ""){
        alert("Harap Input Kembali Nama Customer(SPK) Dengan Benar!!");
      }else if(tempatlahir == ""){
        alert("Harap Input Kembali Tempat Lahir Dengan Benar!!");
      }else if(tanggallahir == ""){
        alert("Harap Pilih Kembali Tanggal Lahir Dengan Benar!!");
      }else if(identitas == ""){
        alert("Harap Input Kembali Identitas/KTP Dengan Benar!!");
      }else if(jk == ""){
        alert("Harap Pilih Kembali Jenis Kelamin Dengan Benar!!");
      }else if(alamat == ""){
        alert("Harap Input Kembali ALamat Dengan Benar!!");
      }else if(kota == ""){
        alert("Harap Input Kembali Kota Dengan Benar!!");
      }else if(kodepos == ""){
        alert("Harap Input Kembali Kode Pos Dengan Benar!!");
      }else if(hp == ""){
        alert("Harap Input Kembali HP Dengan Benar!!");
      }else if(payment == ""){
        alert("Harap Pilih Kembali Payment Model Dengan Benar!!");
      }else if(jenis2 == ""){
        alert("Harap Pilih Kembali Jenis Penjualan Dengan Benar!!");
      }else if(ontheroad == ""){
        alert("Harap Input Kembali Harga Unit Dengan Benar!!");
      }else if(paymethod == ""){
        alert("Harap Input Kembali Payment Method Dengan Benar!!");
      }else if(dp == ""){
        alert("Harap Input Kembali DP/Uang Muka Dengan Benar!!");
      }
    }
    //START ACTION UPDATE REMINDER
    if($("#tipe2").val() != '' && $("#warna2").val() != '' && tahun != ''){
      if(statusakhir > status){
        alert('Maaf Status Customer Salah!');
        return false;
      }else{
        if(hasil == 5){//jika kategori dipilih "SPK/DP"
          if(nomanual != '' && namacust != '' && tempatlahir != '' && tanggallahir !== ''
            && identitas != '' && jk != '' && alamat != '' && kota != '' && kodepos != ''
            && hp != '' && payment != '' && jenis2 != '' && ontheroad != '' && paymethod != ''
            && dp != ''){//harus terisi
            let confirmAction = confirm("Apa anda yakin untuk Merubah Inquiry ini?");
            if (confirmAction) {
              // alert("Action successfully executed");
              $.ajax({
                  url: '<?php echo base_url(); ?>C_sistem/inputreal',
                  type: 'POST',
                  data: {tipe:tipe,id:id,hasil:hasil,idren:idren,warna:warna,tahun:tahun,nama:nama,
                        namacust:namacust,tempatlahir:tempatlahir,tanggallahir:tanggallahir,identitas:identitas,
                        jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,hp:hp,offroad:offroad,ontheroad:ontheroad,
                        bbn2:bbn2,ket:ket,payment:payment,nomanual:nomanual,source1:source1,subsididp:subsididp,
                        yesno:yesno,dp:dp,paymethod:paymethod,tgl:tgl,source:source},
                  success: function(response){
                    alert('Berhasil Update Data Inquiry!');
                    // window.location.reload();
                    window.location.href="<?php echo base_url(); ?>C_sistem/reminder";
                  }
              })
            } else {
              alert("Batal Update Data Inquiry!");
            }
          }
        }else{//jika kategori selain kategori = 5
          let confirmAction = confirm("Apa anda yakin untuk Merubah Inquiry ini?");
          if (confirmAction) {
            // alert("Action successfully executed");
            $.ajax({
                url: '<?php echo base_url(); ?>C_sistem/inputreal',
                type: 'POST',
                data: {tipe:tipe,id:id,hasil:hasil,idren:idren,warna:warna,tahun:tahun,nama:nama,
                        namacust:namacust,tempatlahir:tempatlahir,tanggallahir:tanggallahir,identitas:identitas,
                        jk:jk,alamat:alamat,kota:kota,kodepos:kodepos,hp:hp,offroad:offroad,ontheroad:ontheroad,
                        bbn2:bbn2,ket:ket,payment:payment,nomanual:nomanual,source1:source1,subsididp:subsididp,
                        yesno:yesno,dp:dp,paymethod:paymethod,tgl:tgl,source:source},
                success: function(response){
                  alert('Berhasil Update Data Inquiry!');
                  // window.location.reload();
                  window.location.href="<?php echo base_url(); ?>C_sistem/reminder";
                }
            })
          } else {
            alert("Batal Update Data Inquiry!");
          }
        }
      }
      //
    }
    //END ACTION UPDATE REMINDER
  });
  //END SUBMIT BTN PAGE UPDATE REMINDER -> Update REMINDER

	});

</script>
<!-- END CREATED AND UPDATED BY AJI FIRMAN -->
</body>

</html>
