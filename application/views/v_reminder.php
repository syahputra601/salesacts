<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Reminder</h1>
  </div>
  <hr><br>

  <style>
  @media screen and (min-width: 768px) {
  #filt {
    margin-right: 85%;
  }
  #tbhrencana{
    margin-right: 83%;
    width:14%;
  }
  }

  @media screen and (max-width: 768px) {

    #tbhrencana{
      width:92%;
    }
    #tablermd{
      margin-left: -25px;
      margin-right: -25px;
    }

  }
  </style>

  <!-- <?php
  $ksg1 = '';
  $ksg2 = '4';
  $a = '11-01-2019';
  // if($ksg1 != NULL){
  $d = strtotime("+1 month",strtotime($a));
  $e = date("d-m-Y",$d);
  $f = strtotime("-1 days",strtotime($e));
  echo date("Y-m-d",$f);
  // }
  ?> -->

  <div class="card" id="tablermd">
  <div class="card-header">
    Daftar Reminder
  </div>
  <div class="card-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
      <thead>
          <tr style="text-align: center;">
              <th>No</th>
              <th>Tanggal</th>
              <th>Nama Customer</th>
              <th>No Telp</th>
              <th>Source</th>
              <th>Aktivitas</th>
              <th>Status Customer</th>
              <th>Keterangan</th>
              <th></th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1;
        foreach ($join as $u) {
        ?>

        <?php
        $z = $u['hp'];
        $x = str_split($z);
        $x[0]='62';
        $nohp = implode("",$x);  ?>

        <tr style="text-align: center;">
          <td style="padding:30px;"><?php echo $no++; ?></td>
          <td style="padding:30px;"><?php echo date("d-m-Y", strtotime($u['tgl_rencana'])); ?></td>
          <td style="padding:30px;"><?php echo $u['nama'] ?></td>
          <td style="padding:30px;"><?php echo $u['hp'] ?></td>
          <td style="padding:30px;"><?php echo $u['nama_sumbercust'] ?></td>
          <td style="padding:30px;"><?php echo $u['nama_aktiv'] ?></td>
          <td style="padding:30px;"><?php echo $u['nama_hslaktiv'] ?></td>
          <td style="padding:30px;"><?php echo $u['ket_rencana'] ?></td>
          <td style="padding:30px;">
            <a href="<?php echo base_url(); ?>C_sistem/addreal/<?php echo $u['id_rencana_det'] ?>" class="btn btn-danger" style="width:100%"><i class="fas fa-fw fa-edit"></i> Update Inquiry</a>
            <a href="tel:+<?php echo $nohp ?>" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #b3ccff; margin-top:5px;"><img src="<?php echo base_url(); ?>assets/img/telp.png" style="width:25px;"> Phone</a>
            <a href="https://wa.me/<?php echo $nohp ?>?text=" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #ccffcc; margin-top:5px"><img src="<?php echo base_url(); ?>assets/img/wa.png" style="width:25px;"> WhatsApp</a>
          </td>
        </tr>
        <?php } ?>

        <?php foreach($ultah as $a) {?>
          <?php
          $b = $a['hp'];
          $c = str_split($b);
          $c[0]='62';
          $hp = implode("",$c);
          ?>
        <tr style="text-align: center;">
          <td style="padding:30px;"><?php echo $no++; ?></td>
          <td style="padding:30px;"><?php echo date("d-m-Y"); ?></td>
          <td style="padding:30px;"><?php echo $a['nama'] ?></td>
          <td style="padding:30px;"><?php echo $a['hp'] ?></td>
          <td style="padding:30px;"></td>
          <td style="padding:30px;"></td>
          <td style="padding:30px;"></td>
          <td style="padding:30px;">Ulang Tahun</td>
          <td style="padding:30px;">
            <a href="tel:+<?php echo $hp ?>" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #b3ccff;"><img src="<?php echo base_url(); ?>assets/img/telp.png" style="width:25px;"> Phone</a>
            <a href="https://wa.me/<?php echo $hp ?>?text=Kami dari PT. Super Sukses Motor mengucapkan Selamat Ulang Tahun untuk Bapak/Ibu <?php echo $a['nama'] ?>, Semoga panjang umur dan sehat selalu." class="btn btn-success" style="width:100%; background-color:white; border: solid 1px #ccffcc; margin-top:5px; color:#808080;">
              <img src="<?php echo base_url(); ?>assets/img/wa.png" style="width:25px;"> WhatsApp</a>
          </td>
        </tr>
      <?php } ?>

      <?php foreach($ksg as $k) {?>
        <?php
        $b = $k['hp'];
        $c = str_split($b);
        $c[0]='62';
        $hp = implode("",$c);

        $id = $k['idrendet'];
        $tgl = date("d F Y",strtotime($k['tanggal']));
        ?>
      <tr style="text-align: center; color:maroon;">
        <td style="padding:30px;"><?php echo $no++; ?></td>
        <td style="padding:30px;"><?php echo $k['tanggal'] ?></td>
        <td style="padding:30px;"><?php echo $k['nama'] ?> <?php echo '('.$k['ksg'].')' ?></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"><?php echo $k['ksg'] ?> <br> <?php echo $k['motor'] ?></td>
        <td style="padding:30px;">
          <a href="tel:+<?php echo $hp ?>" onclick="trailtelp(<?php echo $id ?>)" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #b3ccff;"><img src="<?php echo base_url(); ?>assets/img/telp.png" style="width:25px;"> Phone</a>
          <a href="https://wa.me/<?php echo $hp ?>?text=Yth. Bapak/Ibu <?php echo $k['nama'] ?>, Segera bawa motor <?php echo $k['motor'] ?> anda untuk dilakukan Servis <?php echo $k['ksg'] ?> yang jatuh pada tanggal <?php echo $tgl ?> ke bengkel PT. Super Sukses Motor terdekat." onclick="trailwa(<?php echo $id ?>)" class="btn btn-success" style="width:100%; background-color:white; border: solid 1px #ccffcc; margin-top:5px; color:#808080;">
            <img src="<?php echo base_url(); ?>assets/img/wa.png" style="width:25px;"> WhatsApp</a>
        </td>
      </tr>
    <?php } ?>
      <!-- <tr style="text-align: center;">
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;"></td>
        <td style="padding:30px;">Servis KSG 1</td>
        <td style="padding:30px;">
          <a href="tel:+<?php echo $hp ?>" class="btn btn-default" style="width:100%; background-color:white; border: solid 1px #b3ccff;"><img src="<?php echo base_url(); ?>assets/img/telp.png" style="width:25px;"> Phone</a>
          <a href="https://wa.me/<?php echo $hp ?>?text=Kami dari PT. Super Sukses Motor mengucapkan Selamat Ulang Tahun untuk Bapak/Ibu <?php echo $a['nama'] ?>, Semoga panjang umur dan sehat selalu." class="btn btn-success" style="width:100%; background-color:white; border: solid 1px #ccffcc; margin-top:5px; color:#808080;">
            <img src="<?php echo base_url(); ?>assets/img/wa.png" style="width:25px;"> WhatsApp</a>
        </td>
      </tr> -->
      </tbody>
    </table>

  </div>
</div>

</div><br>
<!-- /.container-fluid -->
