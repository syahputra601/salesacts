
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Rencana</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
            <?php
            $tgl=date('d-m-Y');
            ?>
        <form class="" action="<?php echo base_url(); ?>C_sistem/inputrencana" method="post">
          <label>Tanggal</label>
          <input type="text" class="form-control" name="tanggal" value="<?php echo $tgl ?>" readonly>
          <label>Jam Aktifitas</label>
          <input type="time" class="form-control" name="jam" value="" required>
          <label>Aktivitas</label>
          <select class="form-control" name="aktiv" required>
            <option value="">Pilih Aktivitas</option>
            <?php foreach($aktiv as $u){ ?>
            <option value="<?php echo $u->id_aktiv ?>"><?php echo $u->jenis_aktiv ?> | <?php echo $u->nama_aktiv ?></option>
          <?php } ?>
          </select>
          <label>Customer</label>
          <select id="cust" class="form-control" name="customer" required>
            <option value="">Pilih Customer</option>
            <option value="Nocust">Tidak ada customer</option>
            <option value="Lama">Lama</option>
            <option value="Baru">Baru</option>
          </select>
          <div id="select">
            <label>Customer Lama</label>
            <select class="form-control" name="cust">
              <?php foreach($cust as $u){ ?>
              <option value="<?php echo $u->id_customer ?>"><?php echo $u->nama ?> | <?php echo $u->tmp_lahir ?> | <?php echo $u->tgl_lahir ?> | <?php echo $u->identitas ?> | <?php echo $u->hp ?> | <?php echo $u->alamat ?></option>
            <?php } ?>
            </select>
          </div>
          <div id="box" class="card" style="background-color:gainsboro;">
            <div class="col-md-10">
          <label><i>Nickname</i></label>
          <input type="text" class="form-control" name="nick">
          <label><i>Nama</i></label>
          <input type="text" class="form-control" name="nama">
          <label><i>HP</i></label>
          <input type="text" class="form-control" name="hp" value="">
          <br>
        </div>
        </div>
        <div id="box2" class="">
          <label>Tipe Motor</label>
          <select id="tipe" class="form-control" name="tipe">
            <?php foreach($motor as $a){ ?>
            <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
          <?php } ?>
          </select>
        <label>Warna Motor</label>
        <select id="warna" class="form-control" name="warna">
          <?php foreach($warna as $i){ ?>
          <option value="<?php echo $i->id_invattropt ?>" class="<?php echo $i->id_inventori; ?>"><?php echo $i->val ?></option>
          <?php } ?>
        </select>
        </div>
          <label>Keterangan</label>
          <textarea name="ket" class="form-control" rows="2"></textarea>
      </div>
      </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Tambah</button>
      </div>
    </div>
  </div>
</div></form>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Rencana</h1>
  </div>
  <hr><br>

  <style>
  @media screen and (min-width: 768px) {
  #tbhrencana {
    margin-right: 85%;
  }
  }
  </style>

<div class="card">
  <div class="card-header">
    Daftar Rencana
  </div>
  <div class="card-body">
    <center><button type="button" class="btn btn-primary" id="tbhrencana" data-toggle="modal" data-target="#exampleModal">
      Tambah Rencana
    </button></center> <br><br>
    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
      <thead>
          <tr style="text-align: center;">
              <th>No</th>
              <th>Tanggal</th>
              <th>Jam</th>
              <th>Aktivitas</th>
              <th>Customer</th>
              <th>Keterangan</th>
              <th>Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1;
        foreach ($rencana as $u) {
        ?>

        <tr style="text-align: center;">
          <td style="padding:30px;"><?php echo $no++; ?></td>
          <td style="padding:30px;"><?php echo $u['tgl_rencana'] ?></td>
          <td style="padding:30px;"><?php echo $u['jam_rencana']?></td>
          <td style="padding:30px;"><?php echo $u['nama_aktiv'] ?></td>
          <td style="padding:30px;"><?php echo $u['nama'] ?></td>
          <td style="padding:30px;"><?php echo $u['ket_rencana'] ?></td>
          <td style="padding:30px;">
            <a href="<?php echo base_url(); ?>C_sistem/deleterencana/<?php echo $u['id_rencana'] ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')">Hapus</a>
          </td>
        </tr>

        <!-- Modal -->
        <div class="modal fade" id="EditModal/<?php echo $u['id_rencana'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Rencana</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-8">
                    <?php
                    $tgl=date('d-m-Y');
                    ?>
                <form class="" action="<?php echo base_url(); ?>C_sistem/editrencana/<?php echo $u['id_rencana'] ?>" method="post">
                  <label>Jam Aktifitas</label>
                  <input type="time" class="form-control" name="jam" value="<?php echo $u['jam_rencana'] ?>" required>
                  <label>Aktivitas</label>
                  <select class="form-control" name="aktiv">
                    <option value="<?php echo $u['id_aktiv'] ?>" selected><?php echo $u['nama_aktiv'] ?></option>
                    <?php foreach($aktiv as $a){
                      if($u['id_aktiv'] != $a->id_aktiv){
                      ?>
                    <option value="<?php echo $a->id_aktiv ?>"><?php echo $a->jenis_aktiv ?> | <?php echo $a->nama_aktiv ?></option>
                  <?php } } ?>
                  </select>
                  <label>Keterangan</label>
                  <textarea name="ket" class="form-control" rows="2"><?php echo $u['ket_rencana'] ?></textarea>
              </div>
              </div>
            </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </div>
          </div>
        </div></form>

      <?php } ?>
      </tbody>
    </table>
    </form>
  </div>
</div>

</div>
<!-- /.container-fluid -->
