<!DOCTYPE html>
<html lang="en">

<head>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>System Sales</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logossm.png">

  <style media="screen">
    .bg-white{
      background-color: #e20b12!important;
    }
    .dtr-details{
      width:100%;
    }
    .text-gray-600{
      color: white!important;
    }
    .sidebar-dark .nav-item .nav-link {
      color: black;
    }
    .sidebar-dark .nav-item .nav-link i {
      color: black;
    }
    .nav-link.collapsed:hover{
      color: red!important;
    }
    .fas:hover{
      color: red!important;
    }
    .sidebar-dark .nav-item .nav-link:active, .sidebar-dark .nav-item .nav-link:focus, .sidebar-dark .nav-item .nav-link:hover {
    color: black;
    }
    .sidebar-dark .nav-item .nav-link:active i, .sidebar-dark .nav-item .nav-link:focus i, .sidebar-dark .nav-item .nav-link:hover i {
    color: black;
    }
    .sidebar-dark .nav-item.active .nav-link i{
      color:red!important;
    }
    .nav-item.active a span{
    color: red;
    }
    .copyright{
      color: white;
    }
    .rounded-circle{
      background-color: #e20b12!important;
    }
    .sidebar-brand-text{
      color: red;
    }
    .sidebar-divider{
      background-color: black!important;
    }
    .fa{
      color:white;
    }
    .listspek table tr td:first-child{
      padding-right: 35px;
    }
  </style>

</head>

<body id="page-top" style="position:relative; padding-bottom:40px;">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>C_sistem">
        <div class="sidebar-brand-icon">
          <img src="<?php echo base_url(); ?>assets/img/logossm.png" alt="" style="width:100%;">
        </div>
        <div class="sidebar-brand-text mx-3">System Sales</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">



      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>C_sistem">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/rencana">
          <i class="fas fa-fw fa-th-list"></i>
          <span>Rencana</span>
        </a>
      </li> -->

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'reminder' or $this->uri->segment('2') == '') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/reminder">
          <i class="fas fa-fw fa-book"></i>
          <span>Reminder</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'realisasi' or $this->uri->segment('2') == 'detailrealisasi') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/realisasi">
          <i class="fas fa-fw fa-list"></i>
          <span>Inquiry</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if($this->uri->segment('2') == 'konsumen') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_sistem/konsumen" >
          <i class="fas fa-fw fa-database"></i>
          <span>Data Customer</span>
        </a>
      </li>

      <li class="nav-item <?php if($this->uri->segment('1') == 'C_katalog') echo 'active'; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_katalog/katmotor" >
          <i class="fas fa-fw fa-motorcycle"></i>
          <span>Katalog</span>
        </a>
      </li>

      <li class="nav-item <?php if($this->uri->segment('2') == 'do' or $this->uri->segment('1') == 'C_report' or $this->uri->segment('2') == 'filterdo') echo 'active'; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Report</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">List Report:</h6>
            <a class="collapse-item" href="<?php echo base_url(); ?>C_report/tampildo"><i class="fas fa-fw fa-motorcycle"></i> DO</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo base_url(); ?>C_login/logout" >
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Logout</span>
        </a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <!-- <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div> -->

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-lg-inline text-gray-600 small" id="name">Hello, <?php echo $this->session->userdata("nama"); ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <!-- <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo base_url(); ?>C_login/logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div> -->
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

						<?php
				        if (isset($content_page)){
				        $this->load->view($content_page);}
				    ?>

      <!-- Footer -->
      <footer class="sticky-footer bg-white" style="position:absolute; padding: 1rem; bottom: 0; width:100%;">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Sales Activity 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url(); ?>assets/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/demo/chart-pie-demo.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

  <script src="https:////cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.chained.min.js"></script>

   <script type="text/javascript">

      function gantimotor(a,b,c,d,e,f,g,h,i,j,k){
         $('#tipe3').val(a);
         $('#tipe3').change();
         $('#warna3').val(b);
         $('input[name=nama]').val(c);
         $('input[name=source]').val(d);
         $('input[name=tahun]').val(e);
         $('#status2').val(f);
         $('textarea[name=alamat]').val(g);
         $('input[name=hp]').val(h);
         $('input[name=idren]').val(i);
         $('input[name=idcust]').val(j);
         $('input[name=idsbrcust]').val(k);

       }

   $('#nohp').change(function(){
     var id=$(this).val();
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/gethp",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if (data != ''){
         alert("Nomor yang anda masukkan sudah terdaftar pada sales "+data.nama+".\nJika ingin menggunakan nomor tersebut silahkan pilih kolom Customer Lama atau hubungi sales bersangkutan.");
         $('#nohp').val('');
         }
         if(id.charAt(0) != 0){
           alert("Format No Telp salah!");
           $('#nohp').val('');
         }else if(id.length < 8){
           alert("Format No Telp salah!");
           $('#nohp').val('');
         }else if(id.length > 13){
           alert("Format No Telp salah!");
           $('#nohp').val('');
         }
       }
     });
     });

   $('#hpupdateinq, #hpupdaterem').change(function(){
     var id2 = $('#idcst').val();
     var hpbaru = $(this).val();
     id = id2+';'+hpbaru;
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/cekhpold",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if(data != ''){
           alert("No HP sudah digunakan oleh Customer Lain!");
           $('#hpupdateinq, #hpupdaterem').val('');
         }
         // alert("ok");
       },
       error: function(data){
         alert("error");
       }
     });
   });

   $('#ktp, #ktp2, #ktp3').change(function(){
     var id=$(this).val();
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/getktp",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         if (data != '' && data[0].id_login == $('#idlogin').val()){
         x = confirm('KTP yang anda masukkan sudah terdaftar atas nama '+data[0].nama+ '\nApakah anda akan Replace?');
       }if(data[0].id_login != $('#idlogin').val()){
         var a = data[0].id_login;
         var b = data[0].nama;
         getsales(a,b);
       }
         if(x){
           $('#idcst').val(data[0].id_customer);
           $('input[name=namacust]').val(data[0].nama);
           $('input[name=tempatlahir]').val(data[0].tmp_lahir);
           $('input[name=tanggallahir]').val(data[0].tgl_lahir);
           $('select[name=jk]').val(data[0].jenis_kelamin);
           $('textarea[name=alamat]').val(data[0].alamat);
           $('input[name=kota]').val(data[0].kota);
           $('input[name=kodepos]').val(data[0].kode_pos);
           $('input[name=hp]').val(data[0].hp);
         }else{
           $('#ktp, #ktp2, #ktp3').val('');
           return false;
         }
       }
     });
   });

   function getsales(a,b){
     // console.log(a,b);
     var id = a;
     $.ajax({
       url : "<?php echo base_url();?>C_sistem/getsales",
       method : "POST",
       data : {id: id},
       async : false,
       dataType : 'json',
       success: function(data){
         alert('KTP yang anda masukkan sudah terdaftar atas nama '+b+' dengan sales ' +data[0].nama+'. \nSilahkan hubungi sales yang bersangkutan.');
         $('#ktp, #ktp2, #ktp3').val('');
       }
     });
   }


   $('#tipe2, #tipe, #tipe3, #tipe5').change(function(){
			var id=$(this).val();
			$.ajax({
				url : "<?php echo base_url();?>C_sistem/getwarna",
				method : "POST",
				data : {id: id},
				async : false,
		    dataType : 'json',
				success: function(data){
					var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<option value="'+data[i].id_invattropt+'">'+data[i].val+'</option>';
		            }
		            $('#warna2, #warna, #warna3, #warna5').html(html);
                // $('#status2').val(1);
                // $('#status2').change();
				}

			});
      $.ajax({
				url : "<?php echo base_url();?>C_sistem/getharga",
				method : "POST",
				data : {id: id},
				async : false,
		    dataType : 'json',
				success: function(data){
          var i;
          for(i=0; i<data.length; i++){
					$('#ofr,#ofr2').val(data[i].price_1);
          $('#b,#b2').val(data[i].price_2);
        }
        sum();
        sum2();

				}
			});
		});
    </script>

  <script>
  $(document).ready(function() {
    $('#datepicker').datepicker({
      format: 'dd-mm-yyyy'
    });
    <?php if($tes->id_inventori != NULL) {?>
      $('#tipe2').val(<?php echo $tes->id_inventori ?>);
      $('#tipe2').change();
      <?php if($wrn->id_invattropt != NULL) { ?>
        $('#warna2').val(<?php echo $wrn->id_invattropt ?>);
    <?php } } ?>

  });
  </script>

  <script>
    $(document).ready(function(){
      <?php if($motor->id_inventori != NULL) {?>
        $('#tipe3').val(<?php echo $motor->id_inventori ?>);
        $('#tipe3').change();
        <?php if($warna->id_invattropt != NULL) { ?>
          $('#warna3').val(<?php echo $warna->id_invattropt ?>);
      <?php } } ?>
    });
  </script>

  <script>
    $(document).ready(function(){
      <?php if($motor->id_inventori != NULL) {?>
        $('#tipe5').val(<?php echo $motor->id_inventori ?>);
        $('#tipe5').change();
        <?php if($warna->id_invattropt != NULL) { ?>
          $('#warna5').val(<?php echo $warna->id_invattropt ?>);
      <?php } } ?>
    });
  </script>

  <script>

  function konfirmasi(){
    statusakhir = $('#laststatus2, #lasthasil2').val();
    status = $('#status2, #hasil').val();
    if($('#hpupdateinq, #hpupdaterem, #hpinquiry').val()== ''){
      alert('No HP tidak boleh kosong!');
      return false;
    }
    if(statusakhir > status){
      alert('Maaf Status Customer Salah!');
      return false;
    }else{
      x = confirm('Apakah anda yakin untuk mengupdate Inquiry ini?');
      if(x) return true;
      else return false;
    }
  }

  function sum() {
        var yesno = $('#yes').val();
        var a = document.getElementById('ofr').value;
        var b = document.getElementById('b').value;
        var c = document.getElementById('a').value;
        if(yesno == 'Y'){
          result = parseInt(a) + parseInt(b);
          $('#valbbn2').val(b);
        }
        else{
          result = parseInt(a) + parseInt(c);
          $('#valbbn2').val(c);
        }
        $('#otr').val(result);
  }
  </script>

  <script type="text/javascript">

  function sum2() {
        var yesno = $('#yes2').val();
        var a = document.getElementById('ofr2').value;
        var b = document.getElementById('b2').value;
        var c = document.getElementById('a2').value;
        if(yesno == 'Y'){
          result = parseInt(a) + parseInt(b);
          $('#valbbn').val(b);
        }
        else {
          result = parseInt(a) + parseInt(c);
          $('#valbbn').val(c);
        }
        $('#otr2').val(result);
  }
  </script>

      <script>
      $(document).ready(function() {
      var table = $('#myTable').DataTable( {
          // rowReorder: {
          //     selector: 'td:nth-child(2)'
          // },
          responsive: true
      } );
  } );
      </script>

      <script type="text/javascript">
        $(document).ready(function(){
              $('ul li a').click(function(){
                $('li a').removeClass("active");
                $(this).addClass("active");
            });
        });
      </script>
      <script>
        $("#bbn2").show();
        $("#bbn1").hide();
        $("#yes").change(function(){
          if($("#yes").val() == 'N'){
          $("#bbn1").show();
          $("#bbn2").hide();
          }
          else if ($("#yes").val() == 'Y'){
            $("#bbn1").hide();
            $("#bbn2").show();
          }
          sum();
        });
        $("#bbn4").show();
        $("#bbn3").show();
        $("#yes2").change(function(){
          if($("#yes2").val() == 'N'){
          $("#bbn3").show();
          $("#bbn4").hide();
          }
          else if ($("#yes2").val() == 'Y'){
            $("#bbn3").hide();
            $("#bbn4").show();
          }
          sum2();
        });

        $("#jenis").change(function(){
          if($("#jenis").val() == 'N'){
          $("#yes2").val('N');
          $("#yes2").change();
          $("#otr2").val(0);
          $("#otr2").prop('readonly',false);
          }
          else if ($("#jenis").val() == 'Y'){
            $("#yes2").val('Y');
            $("#yes2").change();
            $("#otr2").prop('readonly',true);
          }
        });

        $("#jenis2").change(function(){
          if($("#jenis2").val() == 'N'){
          $("#yes").val('N');
          $("#yes").change();
          $("#otr").val(0);
          $("#otr").prop('readonly',false);
          }
          else if ($("#jenis2").val() == 'Y'){
            $("#yes").val('Y');
            $("#yes").change();
            $("#otr").prop('readonly',true);
          }
        });
      </script>

      <script>
          $(document).ready(function(){
          $("#box").hide();
          $("#box2").hide();
          $("#select").hide();
          $("#cust").change(function(){
           if($("#cust").val() == 'Baru'){
              //Show text box here
              $("#box").show();
              $("#box2").show();
              $("#select").hide();
              $("#cekro").prop('checked', false);
              $("#cekro").prop('disabled', false);
              $("#hidero").val(0);
           }
           else if($("#cust").val() == 'Lama'){
             //Hide text box here
             $("#select").show();
             $("#box").hide();
             $("#box2").show();
             $("#nohp").prop('required',false);
             $("#cekro").prop('checked', true);
             $("#cekro").prop('disabled', true);
             $("#hidero").val(1);
           }
           else{

             $("#select").hide();
             $("#box2").hide();
             $("#box").hide();

           }
         });
          });

          $(".listfitur").hide();
          $("#btnfitur").click(function(){
            $(".listfitur").toggle();
          });

          $(".listspek").hide();
          $("#btnspek").click(function(){
            $(".listspek").toggle();
          });

          $("#cekro").change(function(){
            if($(this).prop("checked") == true){
            $("#hidero").val(1);
            }
            else if($(this).prop("checked") == false){
            $("#hidero").val(0);
            }
          });
      </script>

      <script>
        $(document).ready(function(){
        $("#nama").change(function(){
          if(("#nama").val() == 0){
            $("#nama").hide();
          }
        });
        });
      </script>


      <script>
          $("#datacust").hide();
          $("#hasil").change(function(){
            if($('#nama-cust').val() != 'Tidak ada customer') {
           if($("#hasil").val() == '5'){
              //Show text box here
              $("#datacust").show();
              $("#nomanual3, #ktp3").prop('required',true);
           }
           else {
             $("#datacust").hide();
             $("#nomanual3, #ktp3").prop('required',false);
           }
         }
            });
      </script>

      <script>
          $("#spk2").hide();
          $("#status2").change(function(){
           if($("#status2").val() == '5'){
              //Show text box here
              $("#spk2").show();
              sum2();
              $("#nomanual2, #ktp2").prop('required',true);
           }
           else {
             $("#spk2").hide();
             $("#nomanual2, #ktp2").prop('required',false);
           }
            });

            function tampilmot(i){
              $(".mot").hide();
              $("#mot"+i).show();
            }
      </script>

      <script>
      $("#spk").hide();
      $("#kategori").change(function(){
       if($("#kategori").val() == '5'){
          //Show text box here
          $("#spk").show();
          $("#nomanual, #ktp").prop('required',true);
       }
       else {
         $("#spk").hide();
         $("#nomanual, #ktp").prop('required',false);
       }
        });
      </script>
      <script>
        function trailtelp(a){
          var id=a;
          $.ajax({
            url : "<?php echo base_url();?>C_sistem/trailtelp",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json'
          });
        }
        function trailwa(a){
          var id=a;
          $.ajax({
            url : "<?php echo base_url();?>C_sistem/trailwa",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json'
          });
        }
      </script>

      <?php if(isset($_GET['ur'])){
        if($_GET['ur']==1){
   ?>
      <script>
        alert('Data Berhasil Diubah!');
      </script>
  <?php } } ?>

</body>

</html>
