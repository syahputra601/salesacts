<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Update Inquery</h1>
  </div>
  <hr>
  <a href="<?php echo base_url(); ?>C_sistem/reminder" class="btn btn-secondary"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</a> <br><br>

                <div class="row">
                  <div class="col-md-6">
                <form class="" action="<?php echo base_url(); ?>C_sistem/inputreal" method="post">
                  <input type="hidden" id="idlogin" name="" value="<?php echo $this->session->userdata('id'); ?>">
                  <input type="hidden" name="id" value="<?php echo $r->id_rencana_det ?>">
                  <input type="hidden" name="idren" value="<?php echo $r->id_rencana ?>">
                  <label>Tanggal</label>
                  <input type="text" class="form-control" name="tgl" value="<?php echo date("d-m-Y", strtotime($r->tgl_rencana))  ?>" readonly>
                  <label>Nama Customer</label>
                  <input type="text" class="form-control" name="" value="<?php echo $r->nama ?>" readonly>
                  <input type="hidden" id="idcst" name="nama" value="<?php echo $r->id_customer ?>">
                  <label>No Telp</label>
                  <input type="text" class="form-control" name="" value="<?php echo $r->hp ?>" readonly>
                  <label>Source</label>
                  <input type="text" class="form-control" name="source1" value="<?php echo $r->nama_sumbercust ?>" readonly>
                  <input type="hidden" name="source" value="<?php echo $r->id_sumbercust ?>">
                  <label>Aktivitas</label>
                  <input type="text" class="form-control" name="" value="<?php echo $r->nama_aktiv ?>" readonly>
                  <label>Status Customer</label>
                  <select id="hasil" class="form-control" name="hasil">
                    <option value="<?php echo $src->id_hslaktiv ?>"><?php echo $src->nama_hslaktiv ?></option>
                    <?php foreach($src2 as $s){ ?>
                      <?php if($s->id_hslaktiv != $src->id_hslaktiv) {?>
                    <option value="<?php echo $s->id_hslaktiv ?>"><?php echo $s->nama_hslaktiv ?></option>
                  <?php } } ?>
                  </select>
                  <input type="hidden" id="lasthasil2" value="<?php echo $src->id_hslaktiv ?>">
                  <!-- SPK -->
                  <div class="">
                    <div id="datacust" class="col-md-10" style="background-color:gainsboro;">
                      <div class="">
                  <label><i>Manual Number</i></label>
                  <input type="text" id="nomanual3" class="form-control" name="nomanual" value="" style="text-transform:uppercase;">
                  <label><i>Nama Customer</i></label>
                  <input type="text" class="form-control" name="namacust" value="<?php echo $r->nama ?>">
                  <label><i>Tempat Lahir</i></label>
                  <input type="text" class="form-control" name="tempatlahir" value="">
                  <label><i>Tanggal Lahir</i></label>
                    <input type="date" class="form-control" name="tanggallahir">
                  <label><i>Identitas</i></label>
                  <input type="number" id="ktp3" class="form-control" name="identitas" value="">
                  <label><i>Jenis Kelamin</i></label>
                  <select class="form-control" name="jk">
                    <option value="Pria">Pria</option>
                    <option value="Wanita">Wanita</option>
                  </select>
                  <label><i>Alamat</i></label>
                  <textarea class="form-control" name="alamat" rows="2"><?php echo $r->alamat ?></textarea>
                  <label><i>Kota</i></label>
                  <input type="text" class="form-control" name="kota" value="">
                  <label><i>Kode Pos</i></label>
                  <input type="number" class="form-control" name="kodepos" value="">
                  <label><i>HP</i></label>
                  <input id="hpupdateinq" type="number" class="form-control" name="hp" value="<?php echo $r->hp ?>">
                <label><i>Payment Model</i></label>
                <select class="form-control" name="payment">
                  <option value="Credit">Credit</option>
                  <option value="Cash">Cash</option>
                </select>
                <div class="" hidden>
                <label><i>Offroad Price</i></label>
                <input id="ofr" onkeyup="sum();" type="number" class="form-control" name="offroad" value="" readonly>
                </div>
                <!-- <label><i>BBN Price</i></label> -->
                <div class="row" hidden>
                <div class="col-md-4">
                  <select id="yes" class="form-control" name="yesno">
                    <option value="Y">Yes</option>
                    <option value="N">No</option>
                  </select>
                </div>
                <div class="col-md-8" id="bbn1">
                  <input id="a" onkeyup="sum();" type="text" class="form-control" name="" value="0" readonly>
                  <input id="b" onkeyup="sum();" type="text" class="form-control" name="" value="">
                  <input id="valbbn2" onkeyup="sum();" type="text" class="form-control" name="bbn2" value="0">
                </div>
                </div>
                <label>Jenis Penjualan</label>
                  <select id="jenis2" class="form-control" name="">
                    <option value="Y">On The Road</option>
                    <option value="N">Off The Road</option>
                  </select>
                <label><i>Harga Unit</i></label>
                <input id="otr" type="text" class="form-control" name="ontheroad" value="" readonly>
                <!-- <label><i>Subsidi DP</i></label> -->
                <input type="hidden" class="form-control" name="subsididp" value="0">
                <label><i>Payment Method</i></label>
                  <select class="form-control" name="paymethod">
                    <option value="Cash">Cash</option>
                    <option value="Transfer">Transfer</option>
                    <option value="Cheque">Cheque</option>
                    <option value="Giro">Giro</option>
                  </select>
                  <label><i>Uang Muka</i></label>
                  <input type="number" name="dp" class="form-control">
                <br>
                    </div>
                  </div>
                </div>
                <!-- SPK -->

                  <label>Tipe Motor</label>
                  <select id="tipe2" class="form-control" name="tipe">
                      <option value="0"></option>
                      <?php foreach($motor as $a){ ?>
                      <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
                    <?php } ?>
                  </select>
                  <label>Warna Motor</label>
                  <select id="warna2" class="form-control" name="warna">
                    <option value=""></option>
                  </select>
                  <label>Tahun Motor</label>
                  <input type="number" class="form-control" name="tahun" value="<?php echo $r->tahun_motor ?>">
                  <label>Keterangan</label>
                  <textarea name="ket" class="form-control" rows="3" cols="80"><?php echo $r->ket_rencana ?></textarea>

                </div>
              </div><br>
              <button type="submit" onclick="return konfirmasi()" class="btn btn-danger"><i class="fas fa-fw fa-save"></i> Simpan</button>

              </form>

</div><br>
<!-- /.container-fluid -->
