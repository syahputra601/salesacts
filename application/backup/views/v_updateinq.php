<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Update Inquiry</h1>
  </div>
  <hr>
  <a href="<?php echo base_url(); ?>C_sistem/realisasi" class="btn btn-secondary"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</a> <br><br>
                  <div class="col-md-6">
                  <form class="" action="<?php echo base_url(); ?>C_sistem/updateinq" method="post">
                  <input type="hidden" id="idlogin" name="" value="<?php echo $this->session->userdata('id'); ?>">
                  <input type="hidden" class="form-control" name="idren" value="<?php echo $join->id_rencana ?>">
                  <input type="hidden" id="idcst" class="form-control" name="idcust" value="<?php echo $join->id_customer ?>">
                  <input type="hidden" class="form-control" name="sumbercust" value="<?php echo $join->id_sumbercust ?>">
                  <label>Nama Customer</label>
                  <input type="text" class="form-control" name="nama" value="<?php echo $join->nama ?>" readonly>
                  <label>No Telp</label>
                  <input type="text" class="form-control" name="tlp" value="<?php echo $join->hp ?>" readonly>
                  <label>Source</label>
                  <input type="text" class="form-control" name="source" value="<?php echo $join->nama_sumbercust ?>" readonly>
                  <label>Tipe Motor</label>
                  <select id="tipe3" class="form-control" name="tipe">
                      <?php foreach($mtr as $a){ ?>
                      <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
                    <?php } ?>
                  </select>
                  <label>Warna Motor</label>
                  <select id="warna3" class="form-control" name="warna">
                    <option value=""></option>
                  </select>
                  <label>Tahun Motor</label>
                  <input type="number" class="form-control" name="tahun" value="<?php echo $data->tahun_motor ?>">
                  <label>Status Customer</label>
                  <select id="status2" class="form-control" name="hslaktiv">
                    <option value="<?php echo $src->id_hslaktiv ?>"><?php echo $src->nama_hslaktiv ?></option>
                    <?php foreach($status as $s){ ?>
                      <?php if($s->id_hslaktiv != $src->id_hslaktiv) {?>
                    <option value="<?php echo $s->id_hslaktiv ?>"><?php echo $s->nama_hslaktiv ?></option>
                  <?php } } ?>
                  </select>
                  <input type="hidden" id="laststatus2" value="<?php echo $src->id_hslaktiv ?>">
                  <!-- SPK -->
                  <div class="">
                    <div id="spk2" class="col-md-12" style="background-color:gainsboro;">
                      <div class="">
                  <label><i>Manual Number</i></label>
                  <input type="text" id="nomanual2" class="form-control" name="nomanual" value="" style="text-transform:uppercase;">
                  <label><i>Nama Customer</i></label>
                  <input type="text" class="form-control" name="namacust" value="<?php echo $join->nama ?>">
                  <label><i>Tempat Lahir</i></label>
                  <input type="text" class="form-control" name="tempatlahir" value="">
                  <label><i>Tanggal Lahir</i></label>
                    <input type="date" id="date" class="form-control" name="tanggallahir">
                  <label><i>Identitas</i></label>
                  <input type="number" id="ktp2" class="form-control" name="identitas" value="">
                  <label><i>Jenis Kelamin</i></label>
                  <select class="form-control" name="jk">
                    <option value="Pria">Pria</option>
                    <option value="Wanita">Wanita</option>
                  </select>
                  <label><i>Alamat</i></label>
                  <textarea class="form-control" name="alamat" rows="2"><?php echo $join->alamat ?></textarea>
                  <label><i>Kota</i></label>
                  <input type="text" class="form-control" name="kota" value="">
                  <label><i>Kode Pos</i></label>
                  <input type="number" class="form-control" name="kodepos" value="">
                  <label><i>HP</i></label>
                  <input id="hpupdaterem" type="number" class="form-control" name="hp" value="<?php echo $join->hp ?>">
                  <label><i>Payment Model</i></label>
                  <select class="form-control" name="payment">
                    <option value="Credit">Credit</option>
                    <option value="Cash">Cash</option>
                  </select>
                  <div class="" hidden>
                  <label><i>Offroad Price</i></label>
                  <input id="ofr2" onkeyup="sum2();" type="number" class="form-control" name="offroad" value="" readonly>
                  </div>
                  <!-- <label><i>BBN Price</i></label> -->
                  <div class="row" hidden>
                  <div class="col-md-4">
                    <select id="yes2" class="form-control" name="yesno">
                      <option value="Y">Yes</option>
                      <option value="N">No</option>
                    </select>
                  </div>
                  <div class="col-md-8" id="bbn3">
                    <input id="a2" type="text" class="form-control" name="" value="0" readonly>
                    <input id="b2" type="text" class="form-control" name="" value="">
                    <input id="valbbn" onkeyup="sum2();" type="text" class="form-control" name="bbn2" value="0">
                  </div>
                  </div>
                  <label>Jenis Penjualan</label>
                    <select id="jenis" class="form-control" name="">
                      <option value="Y">On The Road</option>
                      <option value="N">Off The Road</option>
                    </select>
                  <label><i>Harga Unit</i></label>
                  <input id="otr2" type="text" class="form-control" name="ontheroad" value="" readonly>
                  <!-- <label><i>Subsidi DP</i></label> -->
                  <input type="hidden" class="form-control" name="subsididp" value="0">
                  <label><i>Payment Method</i></label>
                    <select class="form-control" name="paymethod">
                      <option value="Cash">Cash</option>
                      <option value="Transfer">Transfer</option>
                      <option value="Cheque">Cheque</option>
                      <option value="Giro">Giro</option>
                    </select>
                    <label><i>Uang Muka</i></label>
                    <input type="number" name="dp" class="form-control">
                  <br>
                      </div>
                    </div>
                  </div>
                  <!-- SPK -->

                  <label>Keterangan</label>
                  <input type="text" class="form-control" name="ket" value="">
                  <br>
              <button type="submit" onclick="return konfirmasi()" class="btn btn-primary"><i class="fas fa-fw fa-save"></i> Simpan</button>
              </form>
            </div>

</div><br>
<!-- /.container-fluid -->
