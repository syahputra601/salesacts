<?php
if($this->session->userdata('status') == "login"){
	redirect('C_sistem');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login System</title>
<link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logossm.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
<body>
<div class="login-form">
    <form action="<?php echo base_url(); ?>C_login/aksilogin" method="post">
        <img src="<?php echo base_url(); ?>assets/img/ssmfix.png" style="width:100%; padding-bottom:3px;">
        <div class="form-group">
						<i class="fas fa-fw fa-user" style="position:absolute; padding:12px;"></i>
            <input type="text" name="user" class="form-control" placeholder="Username" required="required" style="padding-left:33px;">
        </div>
        <div class="form-group">
						<i class="fas fa-fw fa-key" style="position:absolute; padding:12px;"></i>
            <input type="password" name="pass" class="form-control" placeholder="Password" required="required" style="padding-left:33px;">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger btn-block"><i class="fas fa-fw fa-sign-in-alt"></i> Log in</button>
        </div>
				<?php
				if($this->session->flashdata('sukses')){
			echo	'<center><p style="color:red">Username / password salah!</p></center>';
			}?>
    </form>
</div>
</body>
</html>
