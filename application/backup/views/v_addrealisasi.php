<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Realisasi</h1>
  </div>
  <hr>
  <a href="<?php echo base_url(); ?>C_sistem/realisasi" class="btn btn-secondary">Kembali</a> <br><br>


                <?php
                date_default_timezone_set('Asia/Jakarta');
                 $date = date('Y-m-d H:i:s'); ?>
                <div class="row">
                  <div class="col-md-8">
                <form class="" action="<?php echo base_url(); ?>C_sistem/inputrealisasi" method="post">
                  <input type="hidden" name="idrencana" value="<?php echo $join->id_rencana; ?>">
                  <input type="hidden" name="idcust" value="<?php echo $join->id_customer; ?>">
                  <label>No SO</label>
                  <input type="text" class="form-control" name="inv" value="<?php echo $inv ?>" readonly>
                  <label>Date Time</label>
                  <input type="text" class="form-control" name="date" value="<?php echo $date ?>" readonly>
                  <label>Jam Awal</label>
                  <input type="time" class="form-control" name="jamawal" value="" required>
                  <label>Jam Akhir</label>
                  <input type="time" class="form-control" name="jamakhir" value="" required>
                  <label>Hasil Aktivitas</label>
                  <select id="hasil" class="form-control" name="hslaktiv" required>
                    <option value="">Pilih Hasil Aktivitas</option>
                    <?php foreach($hslaktiv as $i){ ?>
                    <option value="<?php echo $i->id_hslaktiv?>"><?php echo $i->nama_hslaktiv ?></option>
                    <?php } ?>
                  </select>
              <!-- SPK -->
                  <div id="datacust" class="">
                    <div class="" style="background-color:gainsboro;">
                      <div class="col-md-10">
                  <label><i>Manual Number</i></label>
                  <input type="text" class="form-control" name="nomanual" value="">
                  <label><i>Nickname</i></label>
                  <input type="text" class="form-control" name="nick" value="<?php echo $join->nickname ?>" readonly>
                  <label><i>Nama</i></label>
                  <input type="text" class="form-control" id="nama-cust" name="nama" value="<?php echo $join->nama ?>" readonly>
                  <label><i>Tempat Lahir</i></label>
                  <input type="text" class="form-control" name="tempatlahir" value="<?php echo $join->tmp_lahir ?>">
                  <label><i>Tanggal Lahir</i></label>
                  <?php if($join->tgl_lahir != NULL) { ?>
                  <input type="text" name="tanggallahir" class="form-control" value="<?php echo date('d-m-Y', strtotime($join->tgl_lahir)); ?>" readonly>
                  <?php } else { ?>
                    <input type="text" class="form-control" id="datepicker" name="tanggallahir" readonly>
                  <?php } ?>
                  <label><i>Identitas</i></label>
                  <?php if($join->identitas != "") { ?>
                  <input type="text" name="identitas" class="form-control" value="<?php echo $join->identitas ?>" readonly>
                  <?php } else { ?>
                  <input type="text" class="form-control" name="identitas" value="KTP-">
                  <?php } ?>
                  <label><i>Jenis Kelamin</i></label>
                  <?php if($join->jenis_kelamin != "") { ?>
                  <input type="text" class="form-control" name="jk" value="<?php echo $join->jenis_kelamin ?>" readonly>
                  <?php } else { ?>
                  <select class="form-control" name="jk">
                    <option value="Pria">Pria</option>
                    <option value="Wanita">Wanita</option>
                  </select>
                  <?php } ?>
                  <label><i>Alamat</i></label>
                  <textarea class="form-control" name="alamat" rows="2"><?php echo $join->alamat ?></textarea>
                  <label><i>Kota</i></label>
                  <input type="text" class="form-control" name="kota" value="<?php echo $join->kota ?>">
                  <label><i>Kode Pos</i></label>
                  <input type="text" class="form-control" name="kodepos" value="<?php echo $join->kode_pos ?>">
                  <label><i>Telepon</i></label>
                  <input type="text" class="form-control" name="telp" value="<?php echo $join->telepon_1 ?>">
                  <label><i>HP</i></label>
                  <input type="text" class="form-control" name="hp" value="<?php echo $join->hp ?>">
                  <?php if($tes != NULL) {?>
                  <label><i>Tipe Motor</i></label>
                  <select id="tipe" class="form-control" name="tipe">
                      <option value="<?php echo $tes->id_inventori ?>"><?php echo $tes->kode_original_inv ?> | <?php echo $tes->nama ?></option>\
                      <?php foreach($motor as $a){ ?>
                        <?php if($a['id_inventori']!= $tes->id_inventori) {?>
                      <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
                    <?php } } ?>
                  </select>
                <label><i>Warna Motor</i></label>
                <select id="warna" class="form-control" name="warna">
                  <option value="<?php echo $wrn->id_invattropt ?>"><?php $wrn->val ?></option>
                  <?php foreach($warna as $i){ ?>
                  <option value="<?php echo $i->id_invattropt ?>" class="<?php echo $i->id_inventori ?>"><?php echo $i->val ?></option>
                  <?php } ?>
                </select><?php } else{ ?>
                  <select class="" name="tipe">
                    <option value="NULL">A</option>
                  </select>
                  <select class="" name="warna">
                    <option value="NULL">B</option>
                  </select>
                <?php } ?>
                <label><i>Payment Model</i></label>
                <select class="form-control" name="payment">
                  <option value="Credit">Credit</option>
                  <option value="Cash">Cash</option>
                </select>
                <label><i>Offroad Price</i></label>
                <input id="ofr" onkeyup="sum();" type="text" class="form-control" name="offroad" value="<?php echo $offroad->price_1 ?>" readonly>
                <label><i>BBN Price</i></label>
                <div class="row">
                <div class="col-md-2">
                  <select id="yes" class="form-control" name="yesno">
                    <option value="N">No</option>
                    <option value="Y">Yes</option>
                  </select>
                </div>
                <div class="col-md-10" id="bbn1">
                  <input id="a" onkeyup="sum();" type="text" class="form-control" name="bbn" value="0" readonly>
                </div>
                <div class="col-md-10" id="bbn2">
                  <input id="b" onkeyup="sum();" type="text" class="form-control" name="bbn" value="0" required>
                </div>
                </div>
                <label><i>On The Road</i></label>
                <input id="otr" type="text" class="form-control" name="ontheroad" value="" readonly>
                <label><i>Subsidi DP</i></label>
                <input type="text" class="form-control" name="subsididp" value="">

              <!-- SPK -->


                  <label><i>Payment Method</i></label>
                  <select class="form-control" name="paymethod">
                    <option value="Cash">Cash</option>
                    <option value="Transfer">Transfer</option>
                    <option value="Cheque">Cheque</option>
                    <option value="Giro">Giro</option>
                  </select>
                  <label><i>Uang Muka</i></label>
                  <input type="text" name="dp" class="form-control">
                  <br>
                </div>
                </div>
                </div>
                <?php if($join->nama == "Tidak ada customer") {?>
                  <div class="col-md-8">
                  <label>Sumber Customer</label>
                  <select class="form-control" name="sumbercust">
                    <?php foreach($sumbercust as $a){ ?>
                    <option value="<?php echo $a->id_sumbercust?>"><?php echo $a->nama_sumbercust ?></option>
                  <?php } ?>
                  </select>
                  <label>Keterangan</label>
                  <textarea name="ket" class="form-control" rows="2"></textarea>
              </div>
            </div><br>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
          </div> <?php } else { ?>
            <label>Sumber Customer</label>
            <select class="form-control" name="sumbercust">
              <?php foreach($sumbercust as $a){ ?>
              <option value="<?php echo $a->id_sumbercust?>"><?php echo $a->nama_sumbercust ?></option>
            <?php } ?>
            </select>
            <label>Keterangan</label>
            <textarea name="ket" class="form-control" rows="2"></textarea>
            </div>
            </div><br>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form> <?php } ?>

</div>
<!-- /.container-fluid -->
