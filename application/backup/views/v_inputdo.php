<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Input DO</h1>
  </div>
  <hr>
  <a href="<?php echo base_url(); ?>C_sistem/realisasi" class="btn btn-secondary"><i class="fas fa-fw fa-hand-point-left"></i> Kembali</a> <br><br>
                  <div class="col-md-6">
                  <form class="" action="<?php echo base_url(); ?>C_sistem/inputdo" method="post">
                    <label>No DO</label>
                    <input type="text" class="form-control" name="nodo" value="" required style="text-transform:uppercase;">
                    <label>Tipe Motor</label>
                    <select id="tipe5" class="form-control" name="idtipe">
                      <?php foreach($mtr as $a){ ?>
                      <option value="<?php echo $a['id_inventori'] ?>"><?php echo $a['kode_original_inv'] ?> | <?php echo $a['nama'] ?></option>
                    <?php } ?>
                    </select>
                  <label>Warna Motor</label>
                  <select id="warna5" class="form-control" name="idwarna">
                    <option></option>
                  </select>
                  <input type="hidden" class="form-control" name="idren" value="<?php echo $data->id_rencana ?>">
                  <input type="hidden" class="form-control" name="idaktiv" value="<?php echo $data->id_aktiv ?>">
                  <input type="hidden" class="form-control" name="tahun" value="<?php echo $data->tahun_motor ?>">
                  <input type="hidden" class="form-control" name="idso" value="<?php echo $data->idso ?>">
                  <br>
              <button type="submit" onclick="return confirm('Apa anda yakin untuk mengupdate DO ini?')" class="btn btn-primary"><i class="fas fa-fw fa-save"></i> Simpan</button>
              </form>
            </div>

</div><br>
<!-- /.container-fluid -->
