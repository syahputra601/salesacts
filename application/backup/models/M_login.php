<?php

class M_login extends CI_Model{

  function cek_login($user, $pass)
  {
    $db2 = $this->load->database('db2', TRUE);
    $grup = array('5', '16', '19', '22', '24', '49', '62', '65');
    $db2->where('username', $user);
    $db2->where('userpass', md5($pass));
    $db2->where('active', TRUE);
    $db2->where_in('idgrup', $grup);
    return $db2->get('erpuser');

  }

  function cek_data($user, $pass)
  {
    $db2 = $this->load->database('db2', TRUE);
    $db2->where('username', $user);
    $db2->where('userpass', md5($pass));
    return $db2->get('erpuser');
  }

  function ambilidemp($id)
  {
    $db2 = $this->load->database('db2', TRUE);
    $db2->where('id_user', $id);
    return $db2->get('employee');
  }

}
