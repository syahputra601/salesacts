<?php

class M_katalog extends CI_Model{

  function tampilkatalog($id)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->select('id_motor, nama_motor');
    $db3->where('id_category', $id);
    $a = $db3->get('motorcycle')->result_array();
    $j = 0;

    foreach($a as $i){
      $db3->select('gambar_color');
      $db3->where('id_motor',$i['id_motor']);
      $db3->where('default_color', 'Y');
      $b[$j][0] = $i['id_motor'];
      $b[$j][1] = $i['nama_motor'];
      $b[$j][2] = $db3->get('motcol')->row();
      $j++;
    }
    return $b;
  }

  function tampilgbrmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->from('motorcycle');
    $db3->join('motcol','motcol.id_motor=motorcycle.id_motor');
    $db3->join('master_color','master_color.id_master=motcol.id_icon');
    $db3->where('motorcycle.id_motor', $idmotor);
    return $db3->get();
  }

  function tampildatamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motorcycle');
  }

  function tampilwarnamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->select('id_icon');
    $db3->where('id_motor', $idmotor);
    $a = $db3->get('motcol')->result();

    foreach($a as $i){
      $db3->where('id_master', $i->id_icon);
      $b[] = $db3->get('master_color')->result();
    }
    return $b;
  }

  function tampilhargamotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motfitur');
  }

  function tampilfiturmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('motfitur_list');
  }

  function tampilbrosurmotor($idmotor)
  {
    $db3 = $this->load->database('db3', TRUE);
    $db3->where('id_motor', $idmotor);
    return $db3->get('fiturmotor');
  }

}
