<?php

class M_report extends CI_Model{

  function tampildo()
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

  function filter_do($date)
  {
    $this->db->from('tb_rencana');
    $this->db->join('tb_rencana_det','tb_rencana_det.id_rencana=tb_rencana.id_rencana');
    $this->db->join('tb_customer','tb_customer.id_customer=tb_rencana.id_customer');
    $this->db->join('tb_sumbercust','tb_sumbercust.id_sumbercust=tb_rencana.id_sumbercust');
    $this->db->join('tb_hslaktivitas','tb_hslaktivitas.id_hslaktiv=tb_rencana_det.id_hslaktiv');
    $this->db->join('tb_aktivitas','tb_aktivitas.id_aktiv=tb_rencana_det.id_aktiv');
    $this->db->where('tb_rencana.id_login', $this->session->userdata('id'));
    $this->db->where('tb_rencana_det.status', 1);
    $this->db->where('tb_rencana_det.id_hslaktiv', 7);
    $this->db->like('tb_rencana_det.tgl_rencana', $date);
    $this->db->order_by('tb_rencana_det.tgl_rencana', 'DESC');
    return $this->db->get();
  }

}
