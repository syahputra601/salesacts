<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_katalog extends CI_Controller {
	function __construct(){
    parent:: __construct();
		$this->load->model('M_katalog');
  }

  function katmotor()
	{
		$data['content_page']= 'v_katmotor';
		$this->load->view('index', $data);
	}

	function katalog($kat)
	{
		if($kat == 'sport'){
			$id = 10201;
		}elseif($kat == 'trail'){
			$id = 10202;
		}elseif($kat = 'moped'){
			$id = 10203;
		}

		$data['katalog']= $this->M_katalog->tampilkatalog($id);
		$data['content_page']= 'v_katalog';
    $this->load->view('index', $data);
	}

  function specmotor($idmotor)
  {
    $data['gbrmotor']= $this->M_katalog->tampilgbrmotor($idmotor)->result_array();
    $data['datamotor']= $this->M_katalog->tampildatamotor($idmotor)->row();
		$data['warnamotor']= $this->M_katalog->tampilwarnamotor($idmotor);
		$data['hargamotor']= $this->M_katalog->tampilhargamotor($idmotor)->row();
		$data['fiturmotor']= $this->M_katalog->tampilfiturmotor($idmotor)->result_array();
		$data['brosurmotor']= $this->M_katalog->tampilbrosurmotor($idmotor)->result();
    $data['content_page']= 'v_specmotor';
    $this->load->view('index', $data);
  }

}
